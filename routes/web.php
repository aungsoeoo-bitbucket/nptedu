<?php
include_once 'web_builder.php';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::pattern('slug', '[a-z0-9- _]+');

Route::group(['prefix' => 'admin', 'namespace'=>'Admin'], function () {

    # Error pages should be shown without requiring login
    Route::get('404', function () {
        return view('admin/404');
    });
    Route::get('500', function () {
        return view('admin/500');
    });
    # Lock screen
    Route::get('{id}/lockscreen', 'UsersController@lockscreen')->name('lockscreen');
    Route::post('{id}/lockscreen', 'UsersController@postLockscreen')->name('lockscreen');

    # All basic routes defined here
    Route::get('login', 'AuthController@getSignin')->name('login');
    Route::get('signin', 'AuthController@getSignin')->name('signin');
    Route::post('signin', 'AuthController@postSignin')->name('postSignin');
    Route::post('signup', 'AuthController@postSignup')->name('admin.signup');
    Route::post('forgot-password', 'AuthController@postForgotPassword')->name('forgot-password');
    
    Route::get('login2', function () {
        return view('admin/login2');
    });


    # Forgot Password Confirmation
    Route::get('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm')->name('forgot-password-confirm');
    Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm');

    # Logout
    Route::get('logout', 'AuthController@getLogout')->name('logout');

    # Account Activation
    Route::get('activate/{userId}/{activationCode}', 'AuthController@getActivate')->name('activate');
});


Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'], function () {
# currently used routes
     # Activity log
    Route::get('activity-log', 'JoshController@activityLogData')->name('activity_log.data');
     # Dashboard / Index
    Route::get('/', 'JoshController@showHome')->name('dashboard');
});



Route::group(['prefix' => 'admin','namespace'=>'Admin', 'middleware' => 'admin', 'as' => 'admin.'], function () {


    # Role Management
    Route::group(['prefix' => 'roles'], function () {
        Route::get('{role}/delete', 'RolesController@destroy')->name('roles.delete');
        Route::get('{role}/confirm-delete', 'RolesController@getModalDelete')->name('roles.confirm-delete');
        Route::get('{role}/restore', 'RolesController@getRestore')->name('roles.restore');
    });
    Route::resource('roles', 'RolesController');

    # Students Management
    Route::group([ 'prefix' => 'users'], function () {
        Route::get('data', 'UsersController@data')->name('users.data');
        Route::get('{user}/delete', 'UsersController@destroy')->name('users.delete');
        Route::get('{user}/confirm-delete', 'UsersController@getModalDelete')->name('users.confirm-delete');
        Route::get('{user}/restore', 'UsersController@getRestore')->name('restore.user');
//        Route::post('{user}/passwordreset', 'UsersController@passwordreset')->name('passwordreset');
        Route::post('passwordreset', 'UsersController@passwordreset')->name('passwordreset');

       //import and export excel
        Route::get('export', 'ExcelController@userExport')->name('users.export');
        Route::post('import', 'ExcelController@import')->name('import');

    });
    Route::resource('users', 'UsersController');

    Route::get('deleted_users',['before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'])->name('deleted_users');


    # Group Management
    Route::group(['prefix' => 'groups'], function () {
        Route::get('{group}/delete', 'GroupsController@destroy')->name('groups.delete');
        Route::get('{group}/confirm-delete', 'GroupsController@getModalDelete')->name('groups.confirm-delete');
        Route::get('{group}/restore', 'GroupsController@getRestore')->name('groups.restore');
    });
    Route::resource('groups', 'GroupsController');

    /*routes for blog*/
    Route::group(['prefix' => 'blog'], function () {
        Route::get('{blog}/delete', 'BlogController@destroy')->name('blog.delete');
        Route::get('{blog}/confirm-delete', 'BlogController@getModalDelete')->name('blog.confirm-delete');
        Route::get('{blog}/restore', 'BlogController@restore')->name('blog.restore');
        Route::post('{blog}/storecomment', 'BlogController@storeComment')->name('storeComment');
    });
    Route::resource('blog', 'BlogController');

    /*routes for blog category*/
    Route::group(['prefix' => 'blogcategory'], function () {
        Route::get('{blogCategory}/delete', 'BlogCategoryController@destroy')->name('blogcategory.delete');
        Route::get('{blogCategory}/confirm-delete', 'BlogCategoryController@getModalDelete')->name('blogcategory.confirm-delete');
        Route::get('{blogCategory}/restore', 'BlogCategoryController@getRestore')->name('blogcategory.restore');
    });
    Route::resource('blogcategory', 'BlogCategoryController');

     # Class Management
    Route::group(['prefix' => 'classes'], function () {
        Route::get('{class}/delete', 'ClassController@destroy')->name('classes.delete');
        Route::get('{class}/confirm-delete', 'ClassController@getModalDelete')->name('classes.confirm-delete');
        Route::get('{class}/restore', 'ClassController@getRestore')->name('classes.restore');
    });
    Route::resource('classes', 'ClassController');

     # Acadamic Year Management
    Route::group(['prefix' => 'acadamic_years'], function () {
        Route::get('{acadamic_years}/delete', 'AcadamicYearController@destroy')->name('acadamic_years.delete');
        Route::get('{acadamic_years}/confirm-delete', 'AcadamicYearController@getModalDelete')->name('acadamic_years.confirm-delete');
    });
    Route::resource('acadamic_years', 'AcadamicYearController');

    # Year Management
    Route::group(['prefix' => 'years'], function () {
        Route::get('{year}/delete', 'YearController@destroy')->name('year.delete');
        Route::get('{year}/confirm-delete', 'YearController@getModalDelete')->name('year.confirm-delete');
        Route::get('{year}/restore', 'YearController@getRestore')->name('year.restore');
    });
    Route::resource('years', 'YearController');


    # Quarter Management
    Route::group(['prefix' => 'quarter'], function () {
        Route::get('{quarter}/delete', 'QuarterController@destroy')->name('quarter.delete');
        Route::get('{quarter}/confirm-delete', 'QuarterController@getModalDelete')->name('quarter.confirm-delete');
        Route::get('{class}/restore', 'QuarterController@getRestore')->name('quarter.restore');
    });
    Route::resource('quarter', 'QuarterController');

    # Courses
    Route::group(['prefix' => 'courses'], function () {
        Route::get('{course}/delete', 'CourseController@destroy')->name('course.delete');
        Route::get('{course}/confirm-delete', 'CourseController@getModalDelete')->name('course.confirm-delete');
        Route::get('{course}/restore', 'CourseController@getRestore')->name('course.restore');
    });
    Route::resource('courses', 'CourseController');



     # Course Management
    Route::group(['prefix' => 'course-management'], function () {
        Route::get('{id}/delete', 'CourseManagementController@destroy')->name('course-management.delete');
        Route::get('{id}/confirm-delete', 'CourseManagementController@getModalDelete')->name('course-management.confirm-delete');
        Route::get('{course-management}/restore', 'CourseManagementController@getRestore')->name('course-management.restore');
    });
    Route::resource('course-management', 'CourseManagementController');
   



     # Professor Management
    Route::group(['prefix' => 'professor'], function () {
        Route::get('{professor}/delete', 'ProfessorController@destroy')->name('professor.delete');
        Route::get('{professor}/confirm-delete', 'ProfessorController@getModalDelete')->name('professor.confirm-delete');
        Route::get('{professor}/restore', 'ProfessorController@getRestore')->name('professor.restore');
    });
    Route::resource('professor', 'ProfessorController');


     # free download files Management
    Route::group(['prefix' => 'freedownload'], function () {
        Route::get('{freedownload}/delete', 'FreeDownloadController@destroy')->name('freedownload.delete');
        Route::get('{freedownload}/confirm-delete', 'FreeDownloadController@getModalDelete')->name('freedownload.confirm-delete');
        Route::get('{freedownload}/restore', 'FreeDownloadController@getRestore')->name('freedownload.restore');
    });
    Route::resource('freedownload', 'FreeDownloadController');


     # about Management
    Route::group(['prefix' => 'about'], function () {
        Route::get('{about}/delete', 'AboutController@destroy')->name('about.delete');
        Route::get('{about}/confirm-delete', 'AboutController@getModalDelete')->name('about.confirm-delete');
        Route::get('{about}/restore', 'AboutController@getRestore')->name('about.restore');
    });
    Route::resource('about', 'AboutController');

     # Exam Management
    Route::group(['prefix' => 'exam'], function () {
        Route::get('{exam}/delete', 'ExamController@destroy')->name('exam.delete');
        Route::get('{exam}/confirm-delete', 'ExamController@getModalDelete')->name('exam.confirm-delete');
        Route::get('{exam}/restore', 'ExamController@getRestore')->name('exam.restore');
    });
    Route::resource('exam', 'ExamController');

    # ExamResult Management
    Route::group(['prefix' => 'exam-result'], function () {
        Route::get('{id}/delete', 'ExamResultController@destroy')->name('exam-result.delete');
        Route::get('{id}/confirm-delete', 'ExamResultController@getModalDelete')->name('result.confirm-delete');
        Route::get('{exam-result}/restore', 'ExamResultController@getRestore')->name('exam-result.restore');
        Route::get('data', 'ExamResultController@data')->name('result.data');
        Route::get('export', 'ExcelController@resultExport')->name('results.export');
    });
    Route::resource('exam-result', 'ExamResultController');

      # Contact Management
    Route::group(['prefix' => 'contacts'], function () {
        Route::get('{contacts}/delete', 'ContactController@destroy')->name('contacts.delete');
        Route::get('{contacts}/confirm-delete', 'ContactController@getModalDelete')->name('contacts.confirm-delete');
        Route::get('{contacts}/restore', 'ContactController@getRestore')->name('contacts.restore');
    });
    Route::resource('contacts', 'ContactController');

    Route::group(['prefix' => 'feedback'], function () {
        Route::get('{feedback}/delete', 'FeedbackController@destroy')->name('feedback.delete');
        Route::get('{feedback}/confirm-delete', 'FeedbackController@getModalDelete')->name('feedback.confirm-delete');
        Route::get('{contacts}/restore', 'FeedbackController@getRestore')->name('feedback.restore');
    });

    Route::resource('feedback', 'FeedbackController');

    Route::group(['prefix' => 'thesis'], function () {
        Route::get('{thesis}/delete', 'ThesisController@destroy')->name('thesis.delete');
        Route::get('{thesis}/confirm-delete', 'ThesisController@getModalDelete')->name('thesis.confirm-delete');
        Route::get('{contacts}/restore', 'FeedbackController@getRestore')->name('feedback.restore');
    });

    Route::resource('thesis', 'ThesisController');

        Route::group(['prefix' => 'thesis_status'], function () {
        Route::get('{thesis_status}/delete', 'ThesisStatusController@destroy')->name('thesis_status.delete');
        Route::get('{thesis_status}/confirm-delete', 'ThesisStatusController@getModalDelete')->name('thesis_status.confirm-delete');
        Route::get('{contacts}/restore', 'ThesisStatusController@getRestore')->name('feedback.restore');
    });

    Route::resource('thesis_status','ThesisStatusController');

     

    Route::group(['prefix' => 'batch_viva'], function () {
        Route::get('{batch_viva}/delete', 'BatchVivaController@destroy')->name('batch_viva.delete');
        Route::get('{batch_viva}/confirm-delete', 'BatchVivaController@getModalDelete')->name('batch_viva.confirm-delete');
        Route::get('{contacts}/restore', 'FeedbackController@getRestore')->name('feedback.restore');
    });

    Route::resource('batch_viva','BatchVivaController'); 
    // batch_viva


});



# Remaining pages will be called from below controller method
# in real world scenario, you may be required to define all routes manually

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('{name?}', 'JoshController@showView');
});


#---------- FrontEndController ------------------------------------------------
Route::get('login', 'FrontEndController@getLogin')->name('login');
Route::post('login', 'FrontEndController@postLogin')->name('login');
Route::get('register', 'FrontEndController@getRegister')->name('register');
Route::post('register/student','RegisterController@registerStudent')->name('register.student');
Route::get('activate/{userId}/{activationCode}','FrontEndController@getActivate')->name('activate');
Route::get('forgot-password','FrontEndController@getForgotPassword')->name('forgot-password');
Route::post('forgot-password', 'FrontEndController@postForgotPassword');

# Forgot Password Confirmation
Route::post('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@postForgotPasswordConfirm');
Route::get('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@getForgotPasswordConfirm')->name('forgot-password-confirm');

Route::post('passwordreset', 'FrontEndController@passwordreset')->name('passwordreset');
# My account display and update details
Route::group(['middleware' => 'user'], function () {
    Route::put('my-account', 'FrontEndController@update');
    Route::get('my-account', 'FrontEndController@myAccount')->name('my-account');
});
Route::get('logout', 'FrontEndController@getLogout')->name('logout');
# contact form
Route::get('contact','FrontEndController@getContact')->name('contact');
Route::post('contact', 'FrontEndController@postContact')->name('post.contact');

Route::get('aboutus','FrontEndController@about')->name('about');

#frontend views
Route::get('/','FrontEndController@frontendHome')->name('home');

Route::get('news','BlogController@index')->name('blog');
Route::get('news/{slug}/tag', 'BlogController@getBlogTag');
Route::get('news-item/{slug?}', 'BlogController@getBlog');
Route::post('blogitem/{blog}/comment', 'BlogController@storeComment');


Route::get('{name?}', 'FrontEndController@showFrontEndView');
# End of frontend views



