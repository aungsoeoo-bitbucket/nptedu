<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseManagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_managements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quarter_id');
            $table->integer('course_id');
            $table->integer('login_id');
            $table->string('title');
            $table->string('description');
            $table->string('document')->nullable();
            $table->string('audio')->nullable();
            $table->string('video')->nullable();
            $table->date('publish_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_managements');
    }
}
