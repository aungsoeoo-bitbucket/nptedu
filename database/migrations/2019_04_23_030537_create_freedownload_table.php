<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreedownloadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freedownload', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en');
            $table->string('title_mm')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_mm')->nullable();
            $table->string('file')->nullable();
            $table->string('youtube_url')->nullable();
            $table->boolean('active')->default(0);
            $table->integer('login_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freedownload');
    }
}
