<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			// add gender,dob,pic,subject,class_of_year,roll_no,group_id,designation,department,address,phone,status
			
			$table->string('gender')->nullable();
			$table->date('dob')->nullable();
			$table->string('pic')->nullable();
			$table->string('subject')->nullable();
			$table->string('class_of_year')->nullable();
			$table->string('roll_no')->nullable();
			$table->string('roll_no_2')->nullable();
			$table->integer('group_id')->nullable();
			$table->text('designation')->nullable();
			$table->text('department')->nullable();
			$table->string('ministry_company')->nullable();
			$table->string('address')->nullable();
			$table->string('phone')->nullable();
			$table->boolean('status')->nullable();
			$table->string('country')->nullable();
			$table->string('facebook')->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			// delete above columns
			$table->dropColumn(array("gender","dob","pic","subject","class_of_year","roll_no","group_id","designation","department","address","phone","status","country"));
		});
	}

}
