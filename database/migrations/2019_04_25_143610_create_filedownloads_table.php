<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiledownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filedownloads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('c_id')->comment('foreign kye of course management table id');
            $table->integer('user_id')->comment('foreign kye of course management table id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filedownloads');
    }
}
