<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('class_id')->comment('foreign key of class table');
            $table->integer('academic_year_id')->comment('foreign key of Academic year table'); 
            $table->integer('year_id')->comment('foreign key of year table');
            $table->integer('quarter_id')->comment('foreign key of quarter table');
            $table->string('course_name');
            $table->string('course_code');
            $table->string('course_photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
