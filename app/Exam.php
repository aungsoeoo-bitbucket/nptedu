<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table = 'exams';

	protected $fillable = ['year_id','quarter_id','title'];
	protected $guarded = ['id'];

	public function year()
    {
        return $this->hasOne('App\Year','id','year_id');
    }


    public function quarter()
    {
        return $this->hasOne('App\Quarter','id','quarter_id');
    }

}
