<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';

	protected $fillable = ['brief_en','brief_mm','description_en','description_mm','images'];
	protected $guarded = ['id'];
}
