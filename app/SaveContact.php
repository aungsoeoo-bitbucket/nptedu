<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveContact extends Model
{

    protected $table = 'contacts';
    protected $guarded  = ['id'];
    protected $fillable  = ['name','email','message'];


}
