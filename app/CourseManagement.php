<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseManagement extends Model
{
    
    protected $table = 'course_managements';

	protected $fillable = ['quarter_id','course_id','login_id','title','description','document','audio','video','publish_date'];
	protected $guarded = ['id'];

	public function course()
    {
        return $this->hasOne('App\Course','id','course_id');
    }

    public function user()
    {
        return $this->hasOne('App\User','id','login_id');
    }

    public function filedownload()
    {
        return $this->hasMany('App\FileDownload','c_id');
    }



}
