<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $guarded  = ['id'];
    protected $fillable  = ['group_name'];
    protected $searchableColumns = ['group_name'];


    public function students()
    {
    	 return $this->hasMany('App\User','group_id');
    }
}
