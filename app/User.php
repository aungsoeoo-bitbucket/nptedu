<?php 
namespace App;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentTaggable\Taggable;


class User extends EloquentUser {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	protected $table = 'users';

	/**
	 * The attributes to be fillable from the model.
	 *
	 * A dirty hack to allow fields to be fillable by calling empty fillable array
	 *
	 * @var array
	 */
    use Taggable;

	protected $fillable = [];
	// "username",
	// 						"email",
	// 						"password",
	// 						"gender",
	// 						"subject",
	// 						"class_of_year",
	// 						"roll_no",
	// 						"dob",
	// 						"designation",
	// 						"department", 
	// 						"group_id",
	// 						"role_id",
	// 						"phone",
	// 						"address",
	// 						"status",
	protected $guarded = ['id'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	* To allow soft deletes
	*/
	// use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = ['full_name'];
    public function getFullNameAttribute()
    {
        return str_limit($this->first_name . ' ' . $this->last_name, 30);
    }
    public function country() {
        return $this->belongsTo( Country::class );
    }

    public function group() {
        return $this->belongsTo( Group::class );
    }

    public function result()
    {
    	 return $this->hasMany('App\ExamResult','user_id');
    }

}
