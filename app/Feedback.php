<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedBack extends Model
{

    protected $table = 'feedbacks';
    protected $guarded  = ['id'];
    protected $fillable  = ['name','email','message','photo'];


}
