<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThesisStatus extends Model
{
    protected $table = 'thesis_statuses';
     protected $guarded  = ['id'];
	protected $fillable = ['status_name','order_no'];
}
