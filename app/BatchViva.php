<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchViva extends Model
{
    protected $table = 'batch_vivas';
    protected $fillable = ['std_id','thisis_status_id','pass_date','complete_date'];

    protected $guarded = ['id'];

   
    public function status()
    {
        return $this->hasOne('App\ThesisStatus','id','thisis_status_id');
    }

    public function student()
    {
        return $this->hasOne('App\User','id','std_id');
    }

    public function thesisGp()
    {
        return $this->hasMany('App\BatchGP','batch_id');
    }

}
