<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thesis extends Model
{
    protected $table = 'thesis';

	protected $fillable = ['user_id','group_id','attend_date'];
	protected $guarded = ['id'];

    public function group()
    {
        return $this->hasOne('App\Group','id','group_id');
    }

     public function thesisGp()
    {
    	return $this->hasMany('App\ThesisGroup','thesis_id');
    }
}
