<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
     // use Eloquence;
    protected $table = 'professors';
    protected $guarded  = ['id'];
    protected $fillable  = ['name','gender','position','bio','phone','email','facebook','twitter','linkedin','photo','active'];
    protected $searchableColumns = ['name','position','email'];

}
