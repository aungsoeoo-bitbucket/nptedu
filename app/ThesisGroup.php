<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThesisGroup extends Model
{
    protected $table = 'thesis_groups';

	protected $fillable = ['thesis_id','user_id'];
	protected $guarded = ['id'];


	public function student()
    {
        return $this->hasOne('App\User','id','user_id');
    }

}
