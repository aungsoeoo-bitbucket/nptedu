<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quarter extends Model
{
    // use Eloquence;
    protected $table = 'quarters';
    protected $guarded  = ['id'];
    protected $fillable  = ['year_id','quarter_name'];
    protected $searchableColumns = ['quarter_name'];

    public function year()
    {
        return $this->hasOne('App\Year','id','year_id');
    }

    public function course()
    {
    	return $this->hasMany('App\Course','quarter_id');
    }
}
