<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

	protected $fillable = ['class_id','academic_year_id','year_id','quarter_id','course_name','course_code','course_photo'];
	protected $guarded = ['id'];


    public function class()
    {
        return $this->hasOne('App\Classes','id','class_id');
    }

     public function adademic()
    {
        return $this->hasOne('App\AcadamicYear','id','academic_year_id');
    }

    public function year()
    {
        return $this->hasOne('App\Year','id','year_id');
    }

    public function quarter()
    {
        return $this->hasOne('App\Quarter','id','quarter_id');
    }


    public function coursemanagement()
    {
        return $this->hasMany('App\CourseManagement','course_id');
    }


	public function fetchCourses($value='')
	{
		$courses = new Course();

		$courses = $courses->leftJoin('classes','classes.id', '=', 'courses.class_id')
                        ->leftJoin('acadamic_years','acadamic_years.id', '=', 'courses.academic_year_id')     
						->leftJoin('year','year.id', '=', 'courses.year_id')
            			->leftJoin('quarters','quarters.id', '=', 'courses.quarter_id')
                       	->select(
                            'courses.id as id',
                            'courses.course_code as course_code',
                            'courses.course_name as course_name',
                            'acadamic_years.acadamic_year as acadamic_year',
                            'courses.created_at as created_at',
                            'classes.class_name as class_name',
                            'year.year as year',
                            'quarters.quarter_name as quarter_name'
                       ); 
        $courses = $courses->get();
        
        return $courses;
	}


	public function detail($id)
	{
		$course = new Course();

		$course = $course->leftJoin('classes','classes.id', '=', 'courses.class_id')
						->leftJoin('year','year.id', '=', 'courses.year_id')
            			->leftJoin('quarters','quarters.id', '=', 'courses.quarter_id')
                       	->select(
                            'courses.id as id',
                            'courses.course_name as course_name',
                            'courses.course_photo as course_photo',
                            'courses.created_at as created_at',
                            'classes.class_name as class_name',
                            'year.year as year',
                            'quarters.quarter_name as quarter_name'
                       ); 
        $course = $course->where('courses.id',$id)->get();
        
        return $course;
	}

}
