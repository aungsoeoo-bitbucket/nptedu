<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Classes extends Model
{
    // use Eloquence;


    protected $table = 'classes';
    protected $guarded  = ['id'];
    protected $fillable  = ['class_name'];
    protected $searchableColumns = ['class_name'];

    public function courses()
    {
    	 return $this->hasMany('App\Course','class_id');
    }

}
