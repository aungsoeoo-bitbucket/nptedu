<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $table = 'year';
    protected $guarded  = ['id'];
    protected $fillable  = ['year'];
    protected $searchableColumns = ['year'];

    public function course()
    {
    	return $this->hasMany('App\Course','year_id');
    }

      public function quarter()
    {
    	return $this->hasMany('App\Quarter','year_id');
    }
}
