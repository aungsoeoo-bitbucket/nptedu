<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileDownload extends Model
{
    protected $table = 'filedownloads';
    protected $guarded  = ['id'];
    protected $fillable  = ['c_id','user_id'];

    public function coursemanagement()
    {
        return $this->belongsTo('App\CourseManagement','c_id');
    }


    public function user()
    {
        return $this->hasMany('App\User','user_id');
    }

}
