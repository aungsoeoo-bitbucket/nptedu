<?php

  
namespace App\Exports;
  
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
  
class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {	
        $usersQuery = User::query();

        $acadamic_year_id = (!empty($_GET["acadamic_year_id"])) ? ($_GET["acadamic_year_id"]) : ('');
        $class_of_year = (!empty($_GET["clear_of_year"])) ? ($_GET["clear_of_year"]) : ('');
        $gender = (!empty($_GET["gender"])) ? ($_GET["gender"]) : ('');

        if($acadamic_year_id){
            $usersQuery = $usersQuery->where('acadamic_year_id',$acadamic_year_id);
        }

        if($class_of_year){
            $usersQuery = $usersQuery->where('class_of_year',$class_of_year);
        }

         if($gender){
            $usersQuery = $usersQuery->where('gender',$gender);
        }

        // dd($acadamic_year_id,$class_of_year,$gender);
        $users = $usersQuery->where('id','!=',1)->select('username','email','gender','dob','designation','department','ministry_company','phone','address')->get();

        return $users;
    }

    public function headings(): array
    {
        return [
			"Name",
			"Email",
            "Gender",
			"Date of Birth",
			"Designation",
			"Department",
			"Ministry/Company",
			"Phone",
			"Address",
        ];
    }
}
