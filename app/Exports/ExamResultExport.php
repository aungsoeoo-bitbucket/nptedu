<?php

namespace App\Exports;

use App\ExamResult;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExamResultExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {	
    	$query = new ExamResult();
        $results =$query->leftJoin('users','users.id', '=', 'exam_results.user_id')
                        ->leftJoin('quarters','quarters.id', '=', 'exam_results.quarter_id')
                        ->leftJoin('courses','courses.id', '=', 'exam_results.course_id')
                        ->select(
                        	'users.roll_no as  roll_no',
                            'users.username as  username',
                            'quarters.quarter_name as  quarter_name',
                            'courses.course_name as  course_name',
                            'exam_results.grade as  grade'
                       )->get();

        // return ExamResult::all();
        return $results;               
    }

    public function headings(): array
    {
        return [
            'Roll No',
            'Name ',
            'Quarter',
            'Course',
            'Result'
        ];
    }
}
