<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'roll_no'       => $row[0],
            'username'      => $row[1], 
            'dob'           => $row[2],
            'job'           => $row[3],
            'designation'   => $row[4], 
            'department'    => $row[5],
            'ministry_company' => $row[6],
            'phone'         => $row[7], 
            'email'         => $row[8],

        ]);
    }
}
