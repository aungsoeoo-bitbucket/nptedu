<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamResult extends Model
{
    protected $table = 'exam_results';

	protected $fillable = ['batch_id','year_id','user_id','quarter_id','course_id','grade'];
	protected $guarded = ['id'];
    protected $searchableColumns = ['grade'];


	public function student()
    {
        return $this->hasOne('App\User','id','user_id');
    }

}
