<?php

namespace App\Http\Controllers;

use Activation;
use App\Http\Requests;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\UserRequest;  
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\FrontendRequest;
use App\User;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use File;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Reminder;
use Validator;
use Sentinel;
use URL;
use View;
use stdClass;
use App\Mail\Contact;
use App\Mail\ForgotPassword ;
use App\Mail\Restore;

class RegisterController extends Controller
{	
	    /*
     * $user_activation set to false makes the user activation via user registered email
     * and set to true makes user activated while creation
     */
    private $user_activation = false;

    public function registerStudent(RegisterUserRequest $request)
    {   
    	$data = new stdClass();

        $request['pic']='';
        $request['class_of_year']='1';
        // //upload image
        // if ($file = $request->file('pic_file')) {
        //     $extension = $file->extension()?: 'png';
        //     $destinationPath = public_path() . '/uploads/users/';
        //     $safeName =  $request->file('pic_file')->getClientOriginalName();
        //     $file->move($destinationPath, $safeName);
        //     $request['pic'] = $safeName;
        // }

        if($request->get('pic_file')!=""){
            
            $file_data = $request->input('pic_file');
            $image = $request->input('pic_file');  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = "pic_file".time().".jpg";
            $path = public_path().'/uploads/users/'. $imageName;
            $success = file_put_contents($path, base64_decode($image));
            $request['pic'] = $imageName;
         }
         else if ($file = $request->file('pic_file')) {
                $extension = $file->extension()?: 'png';
                $destinationPath = public_path() . '/uploads/users/';
                $safeName =  $request->file('pic_file')->getClientOriginalName();
                $file->move($destinationPath, $safeName);
                $request['pic'] = $safeName;
        }else{                
               $request['pic']='';
         }

        //check whether use should be activated by default or not
        $activate = $request->get('activate') ? true : true;

        try {
            // Register the user
            $user = Sentinel::register($request->except('_token', 'password_confirm', 'role', 'activate','pic_file','deletephoto'), $activate);

            //add user to 'User' role
            $role = Sentinel::findRoleById($request->get('role'));
            if ($role) {
                $role->users()->attach($user);
            }
            //check for activation and send activation mail if not activated by default
            // if (!$request->get('activate')) {
            //     // Data to be used on the email view
            //     $data->user_name =$user->first_name .' '. $user->last_name;
            //     $data->activationUrl = URL::route('activate', [$user->id, Activation::create($user)->code]);

            //     // Send the activation code through email
            //     Mail::to($user->email)
            //         ->send(new Restore($data));
            // }

             // login user automatically

            Sentinel::login($user, false);
            //Activity log for new account
            activity($user->username_en)
                ->performedOn($user)
                ->causedBy($user)
                ->log('New Account created');
            // Redirect to the home page with success menu
            return Redirect::route("my-account")->with('success', trans('auth/message.signup.success'));

        } catch (Exception $e) {
            $this->messageBag->add('email', trans('auth/message.account_already_exists'));
        }

        // Ooops.. something went wrong
           return Redirect::back()->withInput()->withErrors($this->messageBag);

         
}

  public function registerStudentBK(RegisterUserRequest $request)
    {
        $data = new stdClass();
        $activate = $this->user_activation; //make it false if you don't want to activate user automatically it is declared above as global variable
         // dd($request->all);
        try {
            // Register the user
            // $user = Sentinel::register($request->only(['username','email', 'password', 'gender']), $activate);
             $user = Sentinel::register($request->all(), $activate);

            //add user to 'User' group
            $role = Sentinel::findRoleByName('User');
            $role->users()->attach($user);
            //if you set $activate=false above then user will receive an activation mail
            if (!$activate) {
                // Data to be used on the email view
                info(Activation::create($user)->code);
                $data->user_name =$user->first_name .' '. $user->last_name;
                $data->activationUrl = URL::route('activate', [$user->id, Activation::create($user)->code]);
                // Send the activation code through email
                Mail::to($user->email)
                    ->send(new Restore($data));
                //Redirect to login page
                return redirect('login')->with('success', trans('auth/message.signup.success'));
            }
            // login user automatically
            Sentinel::login($user, false);
            //Activity log for new account
            activity($user->full_name)
                ->performedOn($user)
                ->causedBy($user)
                ->log('New Account created');
            // Redirect to the home page with success menu
            return Redirect::route("my-account")->with('success', trans('auth/message.signup.success'));

        } catch (UserExistsException $e) {
            $this->messageBag->add('email', trans('auth/message.account_already_exists'));
        }

        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }
}
