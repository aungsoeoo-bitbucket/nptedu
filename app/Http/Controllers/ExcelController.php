<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Sentinel;
use URL;
use View;
use App\ExamResult;
use App\Exports\ExamStdResultExport;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function exportStdResult() 
    {
        return Excel::download(new ExamStdResultExport, 'exam_result.xlsx');
    } 
}
