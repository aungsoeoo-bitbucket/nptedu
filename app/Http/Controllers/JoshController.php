<?php namespace App\Http\Controllers;


use App\Blog;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Sentinel;
use Analytics;
use View;
use Illuminate\Http\Request;
// use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\DataTables;
use Charts;
use App\Datatable;
use App\User;
use Illuminate\Support\Facades\DB;
use Spatie\Analytics\Period;
use Illuminate\Support\Carbon;
use File;
use App\Traits\ActivityTraits;
use App\Activity;


class JoshController extends Controller {
    use ActivityTraits;

    protected $countries = array(
        ""   => "Select Country",
        "MM" => "Myanmar",
    );
    /**
     * Message bag.
     *
     * @var Illuminate\Support\MessageBag
     */
    protected $messageBag = null;

    /**
     * Initializer.
     *
     */
    public function __construct()
    {
        $this->messageBag = new MessageBag;

    }

    /**
     * Crop Demo
     */
    public function crop_demo()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $targ_w = $targ_h = 150;
            $jpeg_quality = 99;

            $src = base_path().'/public/assets/img/cropping-image.jpg';

            $img_r = imagecreatefromjpeg($src);

            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

            imagecopyresampled($dst_r,$img_r,0,0,intval($_POST['x']),intval($_POST['y']), $targ_w,$targ_h, intval($_POST['w']),intval($_POST['h']));

            header('Content-type: image/jpeg');
            imagejpeg($dst_r,null,$jpeg_quality);

            exit;
        }
    }

//    public function showHome()
//    {
//        if(Sentinel::check())
//            return view('admin.index');
//        else
//            return redirect('admin/signin')->with('error', 'You must be logged in!');
//    }

    public function showView($name=null)
    {

        if(View::exists('admin/'.$name))
        {
            if(Sentinel::check())
                return view('admin.'.$name);
            else
                return redirect('admin/signin')->with('error', 'You must be logged in!');
        }
        else
        {
            abort('404');
        }
    }

    public function activityLogData(Request $request)
    {   
        $activities  = new Activity();

        $keyword  = $request->keyword;

        if($keyword!=''){
            $activities = $activities->where('log_name','like','%'.$keyword.'%');
        }
        $activities  = $activities->orderby('created_at','desc')->paginate('10');
        return view('admin.activity_log', compact('activities'));
    }



    public function showHome()
    {
        $storagePath = storage_path().'/app/analytics/';
        if (File::exists($storagePath . 'service-account-credentials.json')) {
            //Last week visitors statistics
            $month_visits = Analytics::fetchTotalVisitorsAndPageViews(Period::days(7))->groupBy(function (array $visitorStatistics) {
                return $visitorStatistics['date']->format('Y-m-d');
            })->map(function ($visitorStatistics, $yearMonth) {
                list($year, $month ,$day) = explode('-', $yearMonth);
                return ['date' => "{$year}-{$month}-{$day}", 'visitors' => $visitorStatistics->sum('visitors'), 'pageViews' => $visitorStatistics->sum('pageViews')];
            })->values();

            //yearly visitors statistics
            $year_visits = Analytics::fetchTotalVisitorsAndPageViews(Period::days(365))->groupBy(function (array $visitorStatistics) {
                return $visitorStatistics['date']->format('Y-m');
            })->map(function ($visitorStatistics, $yearMonth) {
                list($year, $month ) = explode('-', $yearMonth);
                return ['date' => "{$year}-{$month}", 'visitors' => $visitorStatistics->sum('visitors'), 'pageViews' => $visitorStatistics->sum('pageViews')];
            })->values();

            // total page visitors and views
            $visitorsData = Analytics::performQuery(Period::days(7), 'ga:visitors,ga:pageviews', ['dimensions' => 'ga:date']);
            $visitorsData = collect($visitorsData['rows'] ?? [])->map(function (array $dateRow) {
                return [

                    'visitors' => (int) $dateRow[1],
                    'pageViews' => (int) $dateRow[2],
                ];
            });
            $visitors =0;
            $pageVisits =0;
            foreach ($visitorsData as $val)
            {
                $visitors += $val['visitors'];
                $pageVisits += $val['pageViews'];

            }
            $analytics_error = 0;
        }else{
            $month_visits = 0;
            $year_visits = 0;
            $visitors =0;
            $pageVisits =0;
            $analytics_error = 1;
        }


        //total users
        $user_count =User::count();
        //total Blogs
        $blog_count =Blog::count();
        $blogs = Blog::orderBy('id','desc')->take(5)->get()->load('category','author');
        $users = User::orderBy('id', 'desc')->take(6)->get();

        $chart_data = User::select(DB::raw( "COUNT(*) as count_row"))
            ->orderBy("created_at")
            ->groupBy(DB::raw("month(created_at)"))
            ->get();
        $db_chart =  Charts::database(User::all(), 'area', 'morris')
            ->elementLabel("Users")
            ->dimensions(0, 250)
            ->responsive(true)
            ->groupByMonth( 2017, true);


        $countries = DB::table('users')
            ->leftJoin('countries', 'countries.sortname', '=', 'users.country')
            ->select('countries.name')
            ->get();
        $geo = Charts::database($countries, 'geo', 'google')
            ->dimensions(0,250)
            ->responsive(true)

            ->groupBy('name');

        $roles = DB::table('role_users')
            ->join('users','users.id','=','role_users.user_id')->wherenull('deleted_at')
            ->leftJoin('roles', 'role_users.role_id', '=', 'roles.id')
            ->select('roles.name')
            ->get();
        $user_roles = Charts::database($roles, 'pie', 'google')
            ->dimensions(0, 200)
            ->responsive(true)
            ->groupBy('name');
        $line_chart =  Charts::database(User::all(), 'donut', 'morris')
            ->elementLabel("Users")
            ->dimensions(0, 150)
            ->responsive(true)
            ->groupByMonth( 2017, true);

        if(Sentinel::check())
            return view('admin.index',[ 'analytics_error'=>$analytics_error,'chart_data'=>$chart_data, 'blog_count'=>$blog_count,'user_count'=>$user_count,'users'=>$users,'db_chart'=>$db_chart,'geo'=>$geo,'user_roles'=>$user_roles,'blogs'=>$blogs,'visitors'=>$visitors,'pageVisits'=>$pageVisits,'line_chart'=>$line_chart,'month_visits'=>$month_visits,'year_visits'=>$year_visits] );
        else
            return redirect('admin/signin')->with('error', 'You must be logged in!');
    }

}