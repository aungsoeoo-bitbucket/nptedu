<?php

namespace App\Http\Controllers;

use Activation;
use App\Http\Requests;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\UserRequest;  
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\FrontendRequest;
use App\User;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use File;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Reminder;
use Validator;
use Sentinel;
use URL;
use View;
use stdClass;
use App\Mail\Contact;
use App\Mail\ForgotPassword ;
use App\Mail\Restore;
use App\Professor;
use App\Blog;
use App\BlogCategory;
use App\BlogComment;
use App\FreeDownload;
use App\Course;
use App\Quarter;
use App\Group;
use App\Year;
use App\ExamResult;
use App\CourseManagement;
use App\SaveContact;
use App\AcadamicYear;
use App\Feedback;
use App\Thesis;
use App\BatchGP;
use App\ThesisStatus;
use App\BatchViva;



class FrontEndController extends JoshController
{

    protected $maxLoginAttempts = 10; // Amount of bad attempts user can make
    protected $lockoutTime = 300; // Time for which user is going to be blocked in seconds
    /*
     * $user_activation set to false makes the user activation via user registered email
     * and set to true makes user activated while creation
     */
    private $user_activation = false;

    /**
     * Account sign in.
     *
     * @return View
     */
    public function getLogin()
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        // Is the user logged in?
        if (Sentinel::check()=='false') {
            return Redirect::route('my-account');
        }else{
            // Show the login page
            return view('auth.login',compact('batchs'));    
        }
        
    }

    public function clearThrottle(Request $request) {
        $this->clearLoginAttempts($request);
        // Forward elsewhere or display a view
    }

    /**
     * Account sign in form processing.
     *
     * @return Redirect
     */
    public function postLogin(Request $request)
    {

        try {
            // Try to log the user in
            if ($user=  Sentinel::authenticate($request->only('email', 'password'), $request->get('remember-me', 0))) {
                //Activity log for login
                activity($user->username_en)
                    ->performedOn($user)
                    ->causedBy($user)
                    ->log('LoggedIn');

                return Redirect::route("my-account")->with('success', trans('auth/message.login.success'));
            } else {
                return redirect('login')->with('error', 'Email or password is incorrect.');
                //return Redirect::back()->withInput()->withErrors($validator);
            }

        } catch (UserNotFoundException $e) {
            $this->messageBag->add('email', trans('auth/message.account_not_found'));
        } catch (NotActivatedException $e) {
            $this->messageBag->add('email', trans('auth/message.account_not_activated'));
        } catch (UserSuspendedException $e) {
            $this->messageBag->add('email', trans('auth/message.account_suspended'));
        } catch (UserBannedException $e) {
            $this->messageBag->add('email', trans('auth/message.account_banned'));
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $this->messageBag->add('email', trans('auth/message.account_suspended', compact('delay')));
        }

        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }

    /**
     * get user details and display
     */
    public function myAccount(Request $request)
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $user = Sentinel::getUser();
        $countries = $this->countries;
        $quarters = Quarter::all();
        $courses = Course::with('class','year','quarter')->get();

        $quarter_id = $request->quarter_id;
        $course_id = $request->course_id;
        $grade = $request->grade;
        $query = new ExamResult();
        $query = $query->leftJoin('users','users.id', '=', 'exam_results.user_id')
                        ->leftJoin('quarters','quarters.id', '=', 'exam_results.quarter_id')
                        ->leftJoin('courses','courses.id', '=', 'exam_results.course_id')
                        ->select(
                            'exam_results.id as  id',
                            'users.username as  username',
                            'users.roll_no as  roll_no',
                            'quarters.quarter_name as  quarter_name',
                            'courses.course_name as  course_name',
                            'courses.course_code as  course_code',
                            'exam_results.grade as  grade'
                       ); 

        if($quarter_id!=''){
            $query =$query->where('exam_results.quarter_id',$quarter_id);
        }

        if((isset($request->course_id) && $course_id!='' )){
            $query =$query->where('exam_results.course_id',$course_id);
        }

        if($grade!=''){
            $query =$query->where('exam_results.grade',$grade);
        }

        $results = $query->where('user_id',$user->id)->get();

        $acadamicyears  = AcadamicYear::all();
        $class_of_years  = Year::all();
        $groups  = Group::all();

        return view('frontend.user_profile',compact('user','courses','quarters','results','acadamicyears','class_of_years','groups','batchs'));
    }

    /**
     * update user details and display
     * @param Request $request
     * @param User $user
     * @return Return Redirect
     */
    public function update(User $user, FrontendRequest $request)
    {
        $user = Sentinel::getUser();
        //update values
        $user->update($request->except('password','pic','password_confirm'));

        if ($password = $request->get('password')) {
            $user->password = Hash::make($password);
        }
        // is new image uploaded?
        if($request->get('pic')!=""){
            
            $file_data = $request->input('pic');
            $image = $request->input('pic');  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = "pic_file".time().".jpg";
            $path = public_path().'/uploads/users/'. $imageName;
            //delete old pic if exists

            if (File::exists($path . $user->pic)) {
                File::delete($path . $user->pic);
            }

            $success = file_put_contents($path, base64_decode($image));
            $user->pic = $imageName;
         }
         else if ($file = $request->file('pic')) {
            $extension = $file->extension()?: 'png';
            $folderName = '/uploads/users/';
            $destinationPath = public_path() . $folderName;
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);

            //delete old pic if exists
            if (File::exists(public_path() . $folderName . $user->pic)) {
                File::delete(public_path() . $folderName . $user->pic);
            }
            //save new file path into db
            $user->pic = $safeName;

        }

        // Was the user updated?
        if ($user->save()) {
            // Prepare the success message
            $success = trans('users/message.success.update');
            //Activity log for update account
            activity($user->full_name)
                ->performedOn($user)
                ->causedBy($user)
                ->log('User Updated successfully');
            // Redirect to the user page
            return Redirect::route('my-account')->with('success', $success);
        }

        // Prepare the error message
        $error = trans('users/message.error.update');


        // Redirect to the user page
        return Redirect::route('my-account')->withInput()->with('error', $error);


    }

    /**
     * Account Register.
     *
     * @return View
     */
    public function getRegister()
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        // Show the page
        return view('auth.qrcoderegister',compact('batchs'));
    }

    /**
     * Account sign up form processing.
     *
     * @return Redirect
     */
    public function postRegister(Request $request)
    {   
       
        $data = new stdClass();
        $activate = $this->user_activation; //make it false if you don't want to activate user automatically it is declared above as global variable
         // dd($request->all);
        try {
            // Register the user
            $user = Sentinel::register($request->only(['username','email', 'password', 'gender']), $activate);

            //add user to 'User' group
            $role = Sentinel::findRoleByName('User');
            $role->users()->attach($user);
            //if you set $activate=false above then user will receive an activation mail
            if (!$activate) {
                // Data to be used on the email view
                info(Activation::create($user)->code);
                $data->user_name =$request->username;
                $data->activationUrl = URL::route('activate', [$user->id, Activation::create($user)->code]);
                // Send the activation code through email
                Mail::to($user->email)
                    ->send(new Restore($data));
                //Redirect to login page
                return redirect('login')->with('success', trans('auth/message.signup.success'));
            }
            // login user automatically
            Sentinel::login($user, false);
            //Activity log for new account
            activity($user->username_en)
                ->performedOn($user)
                ->causedBy($user)
                ->log('New Account created');
            // Redirect to the home page with success menu
            return Redirect::route("my-account")->with('success', trans('auth/message.signup.success'));

        } catch (UserExistsException $e) {
            $this->messageBag->add('email', trans('auth/message.account_already_exists'));
        }

        // Ooops.. something went wrong
        return Redirect::back()->withInput()->withErrors($this->messageBag);
    }

    /**
     * User account activation page.
     *
     * @param number $userId
     * @param string $activationCode
     *
     */
    public function getActivate($userId, $activationCode)
    {
        // Is the user logged in?
        if (Sentinel::check()) {
            return Redirect::route('my-account');
        }

        $user = Sentinel::findById($userId);

        if (Activation::complete($user, $activationCode)) {
            // Activation was successfull
            return Redirect::route('login')->with('success', trans('auth/message.activate.success'));
        } else {
            // Activation not found or not completed.
            $error = trans('auth/message.activate.error');
            return Redirect::route('login')->with('error', $error);
        }
    }

    /**
     * Forgot password page.
     *
     * @return View
     */
    public function getForgotPassword()
    {
        // Show the page
        return view('auth.forgotpwd');

    }

    /**
     * Forgot password form processing page.
     * @param Request $request
     * @return Redirect
     */
    public function postForgotPassword(Request $request)
    {
        $data = new stdClass();
        try {
            // Get the user password recovery code
            $user = Sentinel::findByCredentials(['email' => $request->email]);
            if (!$user) {
                return Redirect::route('forgot-password')->with('error', trans('auth/message.account_email_not_found'));
            }

            $activation = Activation::completed($user);
            if (!$activation) {
                return Redirect::route('forgot-password')->with('error', trans('auth/message.account_not_activated'));
            }

            $reminder = Reminder::exists($user) ?: Reminder::create($user);
            // Data to be used on the email view

            $data->user_name =$user->first_name .' '. $user->last_name;
            $data->forgotPasswordUrl = URL::route('forgot-password-confirm', [$user->id, $reminder->code]);
            // Send the activation code through email
            Mail::to($user->email)
                ->send(new ForgotPassword($data));

        } catch (UserNotFoundException $e) {
            // Even though the email was not found, we will pretend
            // we have sent the password reset code through email,
            // this is a security measure against hackers.
        }

        //  Redirect to the forgot password
        return back()->with('success', trans('auth/message.forgot-password.success'));
    }

    /**
     * Forgot Password Confirmation page.
     *
     * @param  string $passwordResetCode
     * @return View
     */
    public function getForgotPasswordConfirm(Request $request, $userId, $passwordResetCode = null)
    {
        if (!$user = Sentinel::findById($userId)) {
            // Redirect to the forgot password page
            return Redirect::route('forgot-password')->with('error', trans('auth/message.account_not_found'));
        }

        if($reminder = Reminder::exists($user))
        {
            if($passwordResetCode == $reminder->code)
            {
                return view('forgotpwd-confirm', compact(['userId', 'passwordResetCode']));
            }
            else{
                return 'code does not match';
            }
        }
        else
        {
            return 'does not exists';
        }

    }

    /**
     * Forgot Password Confirmation form processing page.
     *
     * @param  string $passwordResetCode
     * @return Redirect
     */
    public function postForgotPasswordConfirm(PasswordResetRequest $request, $userId, $passwordResetCode = null)
    {

        $user = Sentinel::findById($userId);
        if (!$reminder = Reminder::complete($user, $passwordResetCode, $request->get('password'))) {
            // Ooops.. something went wrong
            return Redirect::route('login')->with('error', trans('auth/message.forgot-password-confirm.error'));
        }

        // Password successfully reseted
        return Redirect::route('login')->with('success', trans('auth/message.forgot-password-confirm.success'));
    }


    public function getContact()
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $user = Sentinel::getUser();
        return view('frontend.contact',compact('user','batchs'));
    }
    /**
     * Contact form processing.
     * @param Request $request
     * @return Redirect
     */
    public function postContact(Request $request)
    {
        $data = new stdClass();

        // Data to be used on the email view
        $data->contact_name = $request->get('name');
        $data->contact_email = $request->get('contact_email');
        $data->contact_msg = $request->get('message');

        $savContact = new SaveContact([
                    'name' => $data->contact_name,
                    'email' => $data->contact_email,
                    'message' => $data->contact_msg,
                ]); 
        $res = $savContact->save();

        // Send the activation code through email
        Mail::to('nptmba@gmail.com')
            ->send(new Contact($data));

        //Redirect to contact page
        return redirect('contact')->with('success', trans('auth/message.contact.success'));
    }

    public function showFrontEndView($name=null)
    {
        if(View::exists($name))
        {
            return view($name);
        }
        else
        {
            abort('404');
        }
    }


    /**
     * Logout page.
     *
     * @return Redirect
     */
    public function getLogout()
    {
        if (Sentinel::check()) {
            //Activity log
            $user = Sentinel::getuser();
            activity($user->full_name)
                ->performedOn($user)
                ->causedBy($user)
                ->log('LoggedOut');
            // Log the user out
            Sentinel::logout();
        }
        // Redirect to the users page
        return redirect('login')->with('success', 'You have successfully logged out!');
    }

    /**
     * get Professors page.
     *
     * @return view
     */
    public function frontendHome()
    {   

        $batchs = AcadamicYear::orderBy("created_at","asc")->get();

        $professors = Professor::where('active',1)->orderBy('id','asc')->limit(4)->get();
        $latest_news = Blog::where('blog_category_id',1)->latest()->limit(3)->get();
        //get no of active professors , students , year , quarters
        $no_of_professors = Professor::where('active',1)->get()->count();
        $no_of_students = User::where('id','!=',1)->get()->count();
        $no_of_year = Year::get()->count();
        $no_of_quarter = Quarter::get()->count();

        return view('frontend.index',compact('professors','latest_news','no_of_professors','no_of_students',
            'no_of_year','no_of_quarter','batchs'));
    }



    /**
     * get Professors page.
     *
     * @return view
     */
    public function getProfessors()
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $professors = Professor::where('active',1)->get();
        return view('frontend.professors',compact('professors','batchs'));
    }


    public function freedownload()
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $files = FreeDownload::where('active',1)->orderBy("created_at","desc")->paginate(10);
        return view('frontend.freedownload',compact('files','batchs'));
    }

    public function courses(Request $request)
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();

        if(Sentinel::check()){

            $batch = $request->batch;

            $years = Year::with(['quarter.course']);

            if($batch!=''){
                $years = Year::with(['quarter.course' => function($query) use ($batch) {
                              $query->where('academic_year_id', $batch);
                         }])->get();
            }

            if($batch==''){
                $years = Year::with(['quarter.course'])->get();
            }

            
            return view('frontend.courses',compact('years','batchs'));
        }else{
            return redirect('login')->with('error', 'You must be logged in!');
        }
    }

    public function courseDetail(Request $request,$id)
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $course = Course::with(['coursemanagement','quarter','year'])->where('id',$id)->get();
        return view('frontend.course-detail',compact('course','batchs'));
    }

    public function mbaStudents(Request $request)
    {   

        if(Sentinel::check()){
            $searchString = $request->keyword;
            $year_id = $request->year_id;
            $group_id = $request->group_id;
            $batch = $request->batch;

            if(Sentinel::getUser()->acadamic_year_id!=''){
                 $batch = Sentinel::getUser()->acadamic_year_id;
            }

            if($request->batch!=''){
                $batch = $request->batch;
            }

            $years = Year::all();
            $quarters = Quarter::all();
            $group_select = Group::all();
            $batchs = AcadamicYear::orderBy("created_at","asc")->get();

            if(($searchString=='') && ($batch=='') && ($year_id=='')  && ($group_id=='') ){
                $groups = Group::with('students');

            }

            
            if( ($batch!='') && ($group_id=='')){
                $groups = Group::with(['students' => function($query) use ($batch) {
                              $query->where('acadamic_year_id', $batch);
                         }]);
            }


            // if($year_id!=''){
            //     $groups = Group::with(['students' => function($query) use ($year_id) {
            //                   $query->where('class_of_year', $year_id);
            //              }]);
            // }

            if(($batch!='') && ($group_id!='')){
                $groups = Group::with(['students' => function($query) use ($batch,$group_id) {
                              $query->where('acadamic_year_id', $batch);
                              $query->where('group_id', $group_id);
                         }]);
            }



            if(($searchString=='') && ($batch=='') && ($group_id!='')){
                $groups = Group::with(['students' => function($query) use ($group_id) {
                              $query->where('group_id', $group_id);
                         }]);
            }


            if($searchString!=''){
                $groups = Group::with(['students' => function($query) use ($searchString) {
                              $query->where('username','like','%'. $searchString.'%');
                              $query->orwhere('roll_no','like','%'.$searchString.'%');
                         }]);
            }


            // dd($groups->toSql());

            
            
            $groups = $groups->get();
            
            // dd($groups);
            $query = $request->all();

            return view('frontend.mba_students',compact('groups','years','quarters','group_select','query','batchs'));
        }else{
            return redirect('login')->with('error', 'You must be logged in!');
        }
        
    }

    public function passwordreset( Request $request)
    {
        $id = $request->id;
        $user = Sentinel::findUserById($id);
        $password = $request->get('password');
        $user->password = Hash::make($password);
        $user->save();
    }

    public function userDetail($id)
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $query =new User();
        $data =$query->leftJoin('year','year.id', '=', 'users.class_of_year')
                        ->leftJoin('acadamic_years','acadamic_years.id', '=', 'users.acadamic_year_id')
                        ->select(
                            'users.*',
                            'year.year',
                            'acadamic_years.acadamic_year'
                       )->where('users.id',$id)->get();
                        
        return view('frontend.user_detail',compact('data','batchs'));
    }

    public function eventCalender()
    {
        $post = Blog::getDatesForCalender();
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();

        // dd($post);
        return view('frontend.event', compact('post','batchs'));
    }

    public function eventDetail(Request $request,$id)
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $latests = Blog::where('blog_category_id',2)->latest()->limit(5)->get();
        $event = Blog::where('id', $id)->first();
        if ($event) {
            $event->increment('views');
        } else {
            abort('404');
        }
        // Show the page
        return view('frontend.event_item', compact('event','latests','batchs'));
    }

    // get courses by quarter
    public function getCoursesByQuarter(Request $request)
    {   

        $quarter_id = $request->quarter_id;
        if(isset($quarter_id) && $quarter_id!=''){
            $courses = Course::where('quarter_id','=',$quarter_id)->get();
            echo "<option value=''>Select Course</option>";

            foreach ($courses as $c) {
                echo "<option value='".$c->id."' {{ old('course_id') }} >".$c->course_name."</option>";
            }
            
            
        }
    }


     public function qrcodeRegister(UserRequest $request)
    {
        $data = new stdClass();
        //upload image
        if ($file = $request->file('pic_file')) {
            $extension = $file->extension()?: 'png';
            $destinationPath = public_path() . '/uploads/users/';
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
            $request['pic'] = $safeName;
        }
        //check whether use should be activated by default or not
        $activate = $request->get('activate') ? true : false;

        try {
            // Register the user
            $user = Sentinel::register($request->except('_token', 'password_confirm', 'role', 'activate', 'pic_file'), $activate);

            //add user to 'User' role
            $role = Sentinel::findRoleById($request->get('role'));
            if ($role) {
                $role->users()->attach($user);
            }
            //check for activation and send activation mail if not activated by default
            if (!$request->get('activate')) {
                // Data to be used on the email view
                $data->user_name =$user->first_name .' '. $user->last_name;
                $data->activationUrl = URL::route('activate', [$user->id, Activation::create($user)->code]);

                // Send the activation code through email
                Mail::to($user->email)
                    ->send(new Restore($data));
                 //Redirect to login page
                return redirect('login')->with('success', trans('auth/message.signup.success'));
            }
            // Activity log for New user create
            activity($user->full_name)
                ->performedOn($user)
                ->causedBy($user)
                ->log('New User Created by '.Sentinel::getUser()->full_name);
            // Redirect to the home page with success menu
            return Redirect::route('admin.users.index')->with('success', trans('users/message.success.create'));

        } catch (LoginRequiredException $e) {
            $error = trans('admin/users/message.user_login_required');
        } catch (PasswordRequiredException $e) {
            $error = trans('admin/users/message.user_password_required');
        } catch (UserExistsException $e) {
            $error = trans('admin/users/message.user_exists');
        }

        // Redirect to the user creation page
        return Redirect::back()->withInput()->with('error', $error);
    }

    public function about()
    {
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        return view('frontend.about',compact('batchs'));
    }

    public function feedback()
    {   
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $user = Sentinel::getUser();
        return view('frontend.feedback',compact('user','batchs'));
    }
    /**
     * Contact form processing.
     * @param Request $request
     * @return Redirect
     */
    public function feedbackPost(Request $request)
    {   
        $data = new stdClass();

        // Data to be used on the email view
        $data->contact_name = $request->get('name');
        $data->contact_email = $request->get('contact_email');
        $data->contact_msg = $request->get('message');

        if ($file = $request->file('photo')) {
            $extension = $file->extension()?: 'png';
            $folderName = '/uploads/feedbacks/';
            $destinationPath = public_path() . $folderName;
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
            //save new file path into db
            $data->photo = $safeName;

        }else{
            $data->photo = "";
        }

        $savFeedback = new Feedback([
                    'name' => $data->contact_name,
                    'email' => $data->contact_email,
                    'message' => $data->contact_msg,
                    'photo' => $data->photo
                ]); 
        $res = $savFeedback->save();

        // Send the activation code through email
        // Mail::to('nptmba@gmail.com')
        //     ->send(new Contact($data));

        //Redirect to contact page
        return redirect('feedback')->with('success', 'Thank you for your feedback.');
    }

    public function thiesis(Request $request)
    {   
        $searchString = $request->keyword;

        $batchs = AcadamicYear::orderBy("created_at","asc")->get();

        $thesisgroups =Thesis::selectRaw("thesis.id,  GROUP_CONCAT(users.username) AS 'username', GROUP_CONCAT(users.username_en) AS 'username_en',GROUP_CONCAT(users.roll_no) AS 'roll_no',GROUP_CONCAT(users.email) AS 'email',GROUP_CONCAT(users.phone) AS 'phone',GROUP_CONCAT(users.pic) AS 'pic',groups.group_name AS 'group_name', thesis. attend_date AS 'attend_date'");
        $thesisgroups = $thesisgroups->leftJoin('thesis_groups','thesis.id','=','thesis_groups.thesis_id')
                                     ->leftJoin('groups','groups.id','=','thesis.group_id')
                                     ->leftJoin('users','users.id','=','thesis_groups.user_id');

        if($searchString!=''){
            // $thesisgroups =Thesis::with(['thesisGp.student'=> function($query) use ($searchString) {
            //                   $query->where('username','like','%'. $searchString.'%');
            //                   $query->where('username_en','like','%'. $searchString.'%');
            //                   $query->orwhere('roll_no','like','%'.$searchString.'%');
            //                   $query->orwhere('roll_no_2','like','%'.$searchString.'%');
            //              }])->paginate(1);

            $thesisgroups = $thesisgroups->where('users.username','like','%'. $searchString.'%')
                                         ->orwhere('users.username_en','like','%'. $searchString.'%')
                                         ->orwhere('users.roll_no','like','%'.$searchString.'%')
                                         ->orwhere('users.roll_no_2','like','%'.$searchString.'%');
        }

        $thesisgroups = $thesisgroups->groupby('thesis.id')->paginate(1);

        $query = $request->all();


        return view('frontend.thesis',compact('batchs','thesisgroups','query'));
    }

    public function spssDownload(){
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        return view('frontend.software-download',compact('batchs'));
    }

     public function userstatusDetail($id)
    {
        // dd($id);
        $batchs = AcadamicYear::orderBy("created_at","asc")->get();
        $query =new User();
        $data =$query->leftJoin('year','year.id', '=', 'users.class_of_year')
                        ->leftJoin('acadamic_years','acadamic_years.id', '=', 'users.acadamic_year_id')
                        ->leftJoin('groups','groups.id', '=', 'users.group_id')
                        ->select(
                            'users.*',
                            'year.year',
                            'acadamic_years.acadamic_year',
                            'groups.group_name'
                       )->where('users.id',$id)->get();
                        
        $statuses = BatchViva::where('std_id',$id)->get();
        // dd($statuses);
        return view('frontend.user_status_detail',compact('data','batchs','statuses'));
    }


    public function statusStudents(Request $request)
    {
        if(Sentinel::check()){
            $searchString = $request->keyword;
            $year_id = $request->year_id;
            $status_id = $request->status_id;
            $batch = $request->batch;

            $status_select = ThesisStatus::orderBy('order_no','asc')->get();

            $batchs = AcadamicYear::orderBy("created_at","asc")->get();


            $batchvivas = new BatchViva();

            $batchvivas = $batchvivas->leftJoin('users','users.id', '=', 'batch_vivas.std_id')
                        ->leftJoin('thesis_statuses','thesis_statuses.id', '=', 'batch_vivas.thisis_status_id')
                        ->select(
                            'users.*',
                            'users.id AS user_id',
                            'batch_vivas.*',
                            'thesis_statuses.status_name'
                       );

            if ($status_id != null) {
                $batchvivas = $batchvivas->where('thesis_statuses.id',$status_id);
            }

            if ($searchString != '') {
                $batchvivas = $batchvivas->where('users.username_en','like','%'.$searchString.'%')->orwhere('users.username','like','%'.$searchString.'%')->orwhere('users.roll_no','like','%'.$searchString.'%')->orwhere('users.roll_no_2','like','%'.$searchString.'%');
            } 

            $total = $batchvivas->count();

            $batchvivas = $batchvivas->paginate(20);
            // dd($batchvivas);π

            // if(($searchString=='') && ($batch=='') && ($year_id=='')  && ($group_id=='') ){
            //     $groups = Group::with('students');

            // }

            
            // if( ($batch!='') && ($group_id=='')){
            //     $groups = Group::with(['students' => function($query) use ($batch) {
            //                   $query->where('acadamic_year_id', $batch);
            //              }]);
            // }


            // if($year_id!=''){
            //     $groups = Group::with(['students' => function($query) use ($year_id) {
            //                   $query->where('class_of_year', $year_id);
            //              }]);
            // }

            // if(($batch!='') && ($group_id!='')){
            //     $groups = BatchGP::with(['student' => function($query) use ($batch,$group_id) {
            //                   $query->where('acadamic_year_id', $batch);
            //                   $query->where('group_id', $group_id);
            //              }]);
            // }



            // if(($searchString=='') && ($batch=='') && ($group_id!='')){
            //     $groups = Group::with(['students' => function($query) use ($group_id) {
            //                   $query->where('group_id', $group_id);
            //              }]);
            // }


            // if($searchString!=''){
            //     $groups = Group::with(['students' => function($query) use ($searchString) {
            //                   $query->where('username','like','%'. $searchString.'%');
            //                   $query->orwhere('roll_no','like','%'.$searchString.'%');
            //              }]);
            // }


            $query = $request->all();

            return view('frontend.status_student',compact('batchvivas','status_select','batchs','total'));
        }else{
            return redirect('login')->with('error', 'You must be logged in!');
        }
    }

}
