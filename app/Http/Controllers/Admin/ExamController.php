<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Year;
use App\Quarter;
use App\Exam;
use App\Http\Requests\ExamRequest;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams =Exam::with(['year','quarter'])->get();
        // Show the page
        return view('admin.exam.index', compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
    	$years = Year::all();
    	$quarters = Quarter::all();
        return view('admin.exam.create',compact('years','quarters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExamRequest $request)
    {   
        
      	$data=[
            'year_id' => $request->year_id,
            'quarter_id' => $request->quarter_id,
            'title' => $request->title,       
        ];

        $res=Exam::create($data);

        if ($res) {
            return redirect('admin/exam')->with('success', 'Exam create successfully.');
        } else {
            return Redirect::route('admin/exam')->withInput()->with('error', 'Error in creating exam.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
    	$years = Year::all();
    	$quarters = Quarter::all();
        $exam = Exam::find($id);
        return view('admin.exam.edit', compact('exam','years','quarters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Quarter $class
     * @return Response
     */

    public function update(ExamRequest $request,$id)
    {   

        $exam=Exam::find($id);
        $exam->year_id = $request->get('year_id');
        $exam->quarter_id = $request->get('quarter_id');
        $exam->title = $request->get('title');

        $result = $exam->save();
        
        if ($result) {
            return redirect('admin/exam')->with('success', "Update Success");
        } else {
             return Redirect::route('admin/exam')->withInput()->with('error', "Update Error");
        }
    }



    /**
     * Remove Quarter.
     *
     * @param Quarter $quarter
     * @return Response
     */
    public function getModalDelete(Exam $exam)
    {   

        $model = 'exam';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.exam.delete', ['id' => $exam->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('exam/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        if ($exam->delete()) {
            return redirect('admin/exam')->with('success', trans('exam/message.success.delete'));
        } else {
            return Redirect::route('admin/exam')->withInput()->with('error', trans('exam/message.error.delete'));
        }
    }
}
