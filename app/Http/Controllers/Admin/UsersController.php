<?php 

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\JoshController;
use App\Http\Requests\UserRequest;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use File;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Yajra\DataTables\DataTables;
use Validator;
Use App\Mail\Restore;
use stdClass;
Use App\Group;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Year;
use App\AcadamicYear;


class UsersController extends JoshController
{
    /**
     * Show a list of all the users.
     *
     * @return View
     */

    public function index()
    {
        $users = User::all();

        $acadamicyears = AcadamicYear::all();
        $years = Year::all();
        // Show the page
        return view('admin.users.index', compact('users','acadamicyears','years'));
    }

    /*
     * Pass data through ajax call
     */
    /**
     * @return mixed
     */
    public function data()
    {
        $users = User::where('id','!=',1);

        // $usersQuery = Users::query();
        $acadamic_year_id = (!empty($_GET["acadamic_year_id"])) ? ($_GET["acadamic_year_id"]) : ('');
        $class_of_year = (!empty($_GET["class_of_year"])) ? ($_GET["class_of_year"]) : ('');
        $gender = (!empty($_GET["gender"])) ? ($_GET["gender"]) : ('');

        if($acadamic_year_id){
            $users = $users->where('acadamic_year_id',$acadamic_year_id);
        }

        if($class_of_year){
            $users = $users->where('class_of_year',$class_of_year);
        }

         if($gender){
            $users = $users->where('gender',$gender);
        }


        $users = $users->get(['id','roll_no', 'username', 'email','created_at']);

        return DataTables::of($users)
            ->editColumn('created_at',function(User $user) {
                return $user->created_at->diffForHumans();
            })
            ->addColumn('status',function($user){

                if($activation = Activation::completed($user)){

                    return 'Activated';} else
                    return 'Pending';

            })
            ->addColumn('actions',function($user) {

                if($user->result()->count()>0){
                   $actions = '<div class="dropdown"><button class="btn btn-primary glyphicon glyphicon-cog dropdown-toggle" type="button" data-toggle="dropdown">&nbsp;<span class="caret"></span></button><ul class="dropdown-menu"><li><a href='.route('admin.users.show', $user->id).'><span class="glyphicon glyphicon-info-sign"></span>Detail</a></li><li>
                        <a href='. route('admin.users.edit', $user->id) .'>
                <span class="glyphicon glyphicon-edit"></span>Edit</a></li><li>
                    <a href="#" data-toggle="modal" data-target="#users_exists" data-name="'. $user->username .'" class="users_exists">
                                                       <span class="glyphicon glyphicon-warning-sign"></span>Delete
                                                    </a></li></ul></div>';
                }else{
                    $actions = '<div class="dropdown"><button class="btn btn-primary glyphicon glyphicon-cog dropdown-toggle" type="button" data-toggle="dropdown">&nbsp;<span class="caret"></span></button><ul class="dropdown-menu"><li><a href='.route('admin.users.show', $user->id).'><span class="glyphicon glyphicon-info-sign"></span>Detail</a></li><li>
                        <a href='. route('admin.users.edit', $user->id) .'>
                
                <span class="glyphicon glyphicon-edit"></span>Edit</a></li><li>
                    <a href='. route('admin.users.confirm-delete', $user->id) .' data-toggle="modal" data-target="#delete_confirm"><span class="glyphicon glyphicon-trash"></span>Delete</a></li></ul></div>';
                }

            
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Create new user
     *
     * @return View
     */
    public function create()
    {

        // Get all the available roles
        $roles = Sentinel::getRoleRepository()->all();
        $groups =Group::all();

        // Get this user groups
        $user = new User;
        $getlastid = $user->all();
        // dd($getlastid);
        // Show the page
        $acadamicyears = AcadamicYear::all();
        $class_of_years = Year::all();
        return view('admin.users.create', compact('roles', 'groups','class_of_years','acadamicyears'));
    }

    /**
     * User create form processing.
     *
     * @return Redirect
     */
    public function store(UserRequest $request)
    {
        $data = new stdClass();
        //upload image
        // if ($file = $request->file('pic_file')) {
        //     $extension = $file->extension()?: 'png';
        //     $destinationPath = public_path() . '/uploads/users/';
        //     $safeName = str_random(10) . '.' . $extension;
        //     $file->move($destinationPath, $safeName);
        //     $request['pic'] = $safeName;
        // }

        if($request->get('pic_file')!=""){
            
            $file_data = $request->input('pic_file');
            $image = $request->input('pic_file');  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = "pic_file".time().".jpg";
            $path = public_path().'/uploads/users/'. $imageName;
            $success = file_put_contents($path, base64_decode($image));
            $request['pic'] = $imageName;
         }
         else if ($file = $request->file('pic_file')) {
                $extension = $file->extension()?: 'png';
                $destinationPath = public_path() . '/uploads/users/';
                $safeName =  $request->file('pic_file')->getClientOriginalName();
                $file->move($destinationPath, $safeName);
                $request['pic'] = $safeName;
        }else{                
               $request['pic']='';
         }

        //check whether use should be activated by default or not
        $activate = $request->get('activate') ? true : false;

        try {
            // Register the user
            $user = Sentinel::register($request->except('_token', 'password_confirm', 'role', 'activate', 'pic_file'), $activate);

            //add user to 'User' role
            $role = Sentinel::findRoleById($request->get('role'));
            if ($role) {
                $role->users()->attach($user);
            }
            //check for activation and send activation mail if not activated by default
            if (!$request->get('activate')) {
                // Data to be used on the email view
                $data->user_name =$user->first_name .' '. $user->last_name;
                $data->activationUrl = URL::route('activate', [$user->id, Activation::create($user)->code]);

                // Send the activation code through email
                Mail::to($user->email)
                    ->send(new Restore($data));
            }
            // Activity log for New user create
            activity(Sentinel::getUser()->username)
                ->performedOn($user)
                ->causedBy($user)
                ->log('New User Created by '.Sentinel::getUser()->username);
            // Redirect to the home page with success menu
            return Redirect::route('admin.users.index')->with('success', trans('users/message.success.create'));

        } catch (LoginRequiredException $e) {
            $error = trans('admin/users/message.user_login_required');
        } catch (PasswordRequiredException $e) {
            $error = trans('admin/users/message.user_password_required');
        } catch (UserExistsException $e) {
            $error = trans('admin/users/message.user_exists');
        }

        // Redirect to the user creation page
        return Redirect::back()->withInput()->with('error', $error);
    }

    /**
     * User update.
     *
     * @param  int $id
     * @return View
     */
    public function edit(User $user)
    {

        // Get this user groups
        $userRoles = $user->getRoles()->pluck('username', 'id')->all();
        // Get a list of all the available groups
        $roles = Sentinel::getRoleRepository()->all();
        $groups =Group::all();
        $status = Activation::completed($user);

        // Show the page
        $acadamicyears = AcadamicYear::all();
        $class_of_years = Year::all();
        return view('admin.users.edit', compact('user','groups', 'roles', 'userRoles', 'status','acadamicyears','class_of_years'));
    }

    /**
     * User update form processing page.
     *
     * @param  User $user
     * @param UserRequest $request
     * @return Redirect
     */
    public function update(User $user, UserRequest $request)
    {
        $data = new stdClass();

        try {
            $user->update($request->except('pic','password','password_confirm','groups','activate'));

            if ( !empty($request->password)) {
                $user->password = Hash::make($request->password);
            }

            // is new image uploaded?
            // if ($file = $request->file('pic')) {
            //     $extension = $file->extension()?: 'png';
            //     $destinationPath = public_path() . '/uploads/users/';
            //     $safeName = str_random(10) . '.' . $extension;
            //     $file->move($destinationPath, $safeName);
            //     //delete old pic if exists
            //     if (File::exists($destinationPath . $user->pic)) {
            //         File::delete($destinationPath . $user->pic);
            //     }
            //     //save new file path into db
            //     $user->pic = $safeName;
            // }

             if($request->get('pic')!=""){
            
                $file_data = $request->input('pic');
                $image = $request->input('pic');  // your base64 encoded
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = "pic_file".time().".jpg";
                $path = public_path().'/uploads/users/'. $imageName;
                //delete old pic if exists

                if (File::exists($path . $user->pic)) {
                    File::delete($path . $user->pic);
                }

                $success = file_put_contents($path, base64_decode($image));
                $user->pic = $imageName;
             }
             else if ($file = $request->file('pic')) {
                    $extension = $file->extension()?: 'png';
                    $destinationPath = public_path() . '/uploads/users/';
                    $safeName =  $request->file('pic')->getClientOriginalName();
                     //delete old pic if exists
                    if (File::exists($destinationPath . $user->pic)) {
                        File::delete($destinationPath . $user->pic);
                    }
                    $file->move($destinationPath, $safeName);
                    $user->pic = $safeName;
            }else{                
                   $user->pic=$user->pic;
             }

            //save record
            $user->save();

            // Get the current user groups
            $userRoles = $user->roles()->pluck('id')->all();

            // Get the selected groups

            $selectedRoles = $request->get('groups');

            // Groups comparison between the groups the user currently
            // have and the groups the user wish to have.
            $rolesToAdd = array_diff($selectedRoles, $userRoles);
            $rolesToRemove = array_diff($userRoles, $selectedRoles);

            // Assign the user to groups

            foreach ($rolesToAdd as $roleId) {
                $role = Sentinel::findRoleById($roleId);
                $role->users()->attach($user);
            }

            // Remove the user from groups
            foreach ($rolesToRemove as $roleId) {
                $role = Sentinel::findRoleById($roleId);
                $role->users()->detach($user);
            }

            // Activate / De-activate user

            $status = $activation = Activation::completed($user);

            if ($request->get('activate') != $status) {
                if ($request->get('activate')) {
                    $activation = Activation::exists($user);
                    if ($activation) {
                        Activation::complete($user, $activation->code);
                    }
                } else {
                    //remove existing activation record
                    Activation::remove($user);
                    //add new record
                    Activation::create($user);
                    //send activation mail
                    $data->user_name =$user->first_name .' '. $user->last_name;
                    $data->activationUrl = URL::route('activate', [$user->id, Activation::exists($user)->code]);
                    // Send the activation code through email
                    Mail::to($user->email)
                        ->send(new Restore($data));

                }
            }

            // Was the user updated?
            if ($user->save()) {
                // Prepare the success message
                $success = trans('users/message.success.update');
               //Activity log for user update
                activity(Sentinel::getUser()->username)
                    ->performedOn($user)
                    ->causedBy($user)
                    ->log('User Updated by '. Sentinel::getUser()->username);
                // Redirect to the user page
                return Redirect::route('admin.users.index', $user)->with('success', $success);
            }

            // Prepare the error message
            $error = trans('users/message.error.update');
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = trans('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }

        // Redirect to the user page
        return Redirect::route('admin.users.index', $user)->withInput()->with('error', $error);
    }

    /**
     * Show a list of all the deleted users.
     *
     * @return View
     */
    public function getDeletedUsers()
    {
        // Grab deleted users
        $users = User::onlyTrashed()->get();

        // Show the page
        return view('admin.deleted_users', compact('users'));
    }


    /**
     * Delete Confirm
     *
     * @param   int $id
     * @return  View
     */
    public function getModalDelete($id)
    {
        $model = 'users';
        $confirm_route = $error = null;
        try {
            // Get user information
            $user = Sentinel::findById($id);

            // Check if we are not trying to delete ourselves
            if ($user->id === Sentinel::getUser()->id) {
                // Prepare the error message
                $error = trans('users/message.error.delete');

                return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
            }
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = trans('users/message.user_not_found', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
        $confirm_route = route('admin.users.delete', ['id' => $user->id]);
        return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    /**
     * Delete the given user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy($id)
    {
        try {
            // Get user information
            $user = Sentinel::findById($id);
            // Check if we are not trying to delete ourselves
            if ($user->id === Sentinel::getUser()->id) {
                // Prepare the error message
                $error = trans('admin/users/message.error.delete');
                // Redirect to the user management page
                return Redirect::route('admin.users.index')->with('error', $error);
            }

           
            //delete old pic if exists
            $folderName = '/uploads/users/';
            if (File::exists(public_path() . $folderName . $user->pic)) {
                File::delete(public_path() . $folderName . $user->pic);
            }
            // Delete the user
            //to allow soft deleted, we are performing query on users model instead of Sentinel model
            User::destroy($id);
            Activation::where('user_id',$user->id)->delete();
            // Prepare the success message
            $success = trans('users/message.success.delete');
            //Activity log for user delete
            activity(Sentinel::getUser()->username)
                ->performedOn($user)
                ->causedBy($user)
                ->log('User deleted by '.Sentinel::getUser()->username);
            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = trans('admin/users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }
    }

    /**
     * Restore a deleted user.
     *
     * @param  int $id
     * @return Redirect
     */
    public function getRestore($id)
    {
        $data = new stdClass();
        try {
            // Get user information
            $user = User::withTrashed()->find($id);
            info($user);
            // Restore the user
            $user->restore();
            // create activation record for user and send mail with activation link
            $data->user_name = $user->first_name .' '. $user->last_name;
            $data->activationUrl = URL::route('activate', [$user->id, Activation::create($user)->code]);
            // Send the activation code through email
            Mail::to($user->email)
                ->send(new Restore($data));
            // Prepare the success message
            $success = trans('users/message.success.restored');
            activity($user->full_name)
                ->performedOn($user)
                ->causedBy($user)
                ->log('User restored by '.Sentinel::getUser()->full_name);
            // Redirect to the user management page
            return Redirect::route('admin.deleted_users')->with('success', $success);
        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = trans('users/message.user_not_found', compact('id'));

            // Redirect to the user management page
            return Redirect::route('admin.deleted_users')->with('error', $error);
        }
    }

    /**
     * Display specified user profile.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            // Get the user information
            $user = Sentinel::findUserById($id);

            $userModel = new User();

            $group = $user->leftJoin('groups','groups.id', '=', 'users.group_id')
                            ->select(
                                'groups.group_name as group_name'
                           ); 
            $group = $group->where('users.id',$id)->get();

            $user->group = $group[0]->group_name;

            $acadamicyear = $user->leftJoin('acadamic_years','acadamic_years.id', '=', 'users.acadamic_year_id')
                            ->select(
                                'acadamic_years.acadamic_year as acadamic_year'
                           ); 
            $acadamicyear = $acadamicyear->where('users.id',$id)->get();

            $user->acadamicyear = $acadamicyear[0]->acadamic_year;

            $class_of_year = $user->leftJoin('year','year.id', '=', 'users.class_of_year')
                            ->select(
                                'year.year as year'
                           ); 
            $class_of_year = $class_of_year->where('users.id',$id)->get();

            $user->class_of_year = $class_of_year[0]->year;



        } catch (UserNotFoundException $e) {
            // Prepare the error message
            $error = trans('users/message.user_not_found', compact('id'));
            // Redirect to the user management page
            return Redirect::route('admin.users.index')->with('error', $error);
        }
        // Show the page
        return view('admin.users.show', compact('user'));

    }

    public function passwordreset( Request $request)
    {
        $id = $request->id;
        $user = Sentinel::findUserById($id);
        $password = $request->get('password');
        $user->password = Hash::make($password);
        $user->save();
    }

    public function lockscreen($id){

        if (Sentinel::check()) {
            $user = Sentinel::findUserById($id);
            return view('admin.lockscreen',compact('user'));
        }
        return view('admin.login');
    }

    public function postLockscreen(Request $request){
        $password = Sentinel::getUser()->password;
        if(Hash::check($request->password,$password)){
            return 'success';
        } else{
            return 'error';
        }
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {   
        return Excel::download(new UsersExport, 'users.xlsx');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import() 
    {
        Excel::import(new UsersImport,request()->file('file'));
           
        return back();
    }
}
