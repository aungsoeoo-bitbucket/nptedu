<?php

namespace App\Http\Controllers\Admin;

use App\ThesisStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ActivityTraits;
use Sentinel;

class ThesisStatusController extends Controller
{
    use ActivityTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $thesis_statuses = ThesisStatus::orderBy('order_no','asc')->get();

        return view('admin.thesis_status.index', compact('thesis_statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.thesis_status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $statuses = new ThesisStatus($request->all());

        if ($statuses->save()) {

            $settingParms = $statuses->toArray();
            $changes = Sentinel::getUser()->username . ' added new class name as  '.'"'.$statuses->status_name.'"';
            $this->logCreatedActivity($statuses,$changes,$settingParms);


            return redirect('admin/thesis_status')->with('success', trans('class/message.success.create'));
        } else {
            return Redirect::route('admin/thesis_status')->withInput()->with('error', trans('class/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ThesisStatus  $thesisStatus
     * @return \Illuminate\Http\Response
     */
    public function show(ThesisStatus $thesisStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ThesisStatus  $thesisStatus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thesis_status = ThesisStatus::find($id);
        return view('admin.thesis_status.edit', compact('thesis_status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ThesisStatus  $thesisStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);
        // dd($request->all());
        $data = ThesisStatus::find($id);

        $res = $data->update([
            'status_name'=>$request->status_name
        ]);

        if ($res) {
            return redirect('admin/thesis_status')->with('success', 'Create successfully.');
        } else {
            return Redirect::route('admin/thesis_status')->withInput()->with('error', 'Error in creating Thesis satatus.');
        }
    }



    public function getModalDelete(ThesisStatus $status,$id)
    {   
        // dd($id);
        $model = 'thesis_status';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.thesis_status.delete', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('thesis/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ThesisStatus  $thesisStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(ThesisStatus $thesisStatus)
    {
        if ($thesisStatus->delete()) {


            return redirect('admin/thesis_status')->with('success', trans('thesis/message.success.delete'));
        } else {
            return Redirect::route('admin/thesis_status')->withInput()->with('error', trans('thesis/message.error.delete'));
        }
    }
}
