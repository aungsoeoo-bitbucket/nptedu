<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FreeDownload;
use App\Http\Requests\FreeDownloadRequest;
use App\Traits\ActivityTraits;
use Sentinel;

class FreeDownloadController extends Controller
{
    use ActivityTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files =FreeDownload::with('user')->orderBy('created_at','desc')->get();
        // Show the page
        return view('admin.freedownload.index', compact('files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('admin.freedownload.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FreeDownloadRequest $request)
    {   

        $destinationPath = public_path() . '/uploads/freedownload/';
        $attach_file='';
        if($request->file('file')){
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $attach_file = $name;
            $file->move($destinationPath, $attach_file);
        }

        
      $data=[
            'title_en' => $request->title_en,
            'title_mm' => ($request->title_mm)? $request->title_mm : '',
            'description_en' => $request->description_en,
            'description_mm' => ($request->description_mm)? $request->description_mm : '',
            'file' => $attach_file,
            'youtube_url' => $request->youtube_url, 
            'active' => $request->active,
            'login_id' => Sentinel::getuser()->id,          
        ];

        $res=FreeDownload::create($data);

        $settingParms = $res->toArray();
        $changes = Sentinel::getUser()->username . ' added new file in freedownload page  name as  '.'"'.$res->title_en.'"';
        $this->logCreatedActivity($res,$changes,$settingParms);

        if ($res) {
            return redirect('admin/freedownload')->with('success', trans('freedownload/message.success.create'));
        } else {
            return Redirect::route('admin/freedownload')->withInput()->with('error', trans('freedownload/message.error.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $file = FreeDownload::find($id);
        return view('admin.freedownload.edit', compact('file'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Quarter $class
     * @return Response
     */

    public function update(FreeDownloadRequest $request,$id)
    {   

        $freedownload=FreeDownload::find($id);
        $beforeUpdateValues = $freedownload->toArray();

        $freedownload->title_en = $request->get('title_en');
        $freedownload->description_en = $request->get('description_en');
        $freedownload->active = $request->get('active');
        $freedownload->youtube_url = $request->get('youtube_url');
        $freedownload->login_id = Sentinel::getuser()->id;

        $destinationPath = public_path() . '/uploads/freedownload/';
        if($request->file('file')){
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $attach_file = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $attach_file);
            $freedownload->file = $attach_file;
             
        }
        
        $result = $freedownload->save();

        $afterUpdateValues = $freedownload->getChanges();
        $this->logUpdatedActivity($freedownload,$beforeUpdateValues,$afterUpdateValues);

        
        if ($result) {
            return redirect('admin/freedownload')->with('success', trans('freedownload/message.success.update'));
        } else {
             return Redirect::route('admin/freedownload')->withInput()->with('error', trans('freedownload/message.error.update'));
        }
    }



    /**
     * Remove Quarter.
     *
     * @param Quarter $quarter
     * @return Response
     */
    public function getModalDelete(FreeDownload $freedownload)
    {   

        $model = 'freedownload';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.freedownload.delete', ['id' => $freedownload->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('freedownload/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FreeDownload $freedownload)
    {
        if ($freedownload->delete()) {
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$freedownload->title_en.'"'.' from FreeDownload';
            $this->logDeletedActivity($freedownload,$changeLog);

            return redirect('admin/freedownload')->with('success', trans('freedownload/message.success.delete'));
        } else {
            return Redirect::route('admin/freedownload')->withInput()->with('error', trans('freedownload/message.error.delete'));
        }
    }
}
