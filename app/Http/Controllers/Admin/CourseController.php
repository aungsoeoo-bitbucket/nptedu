<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes;
use App\Year;
use App\Quarter;
use App\Course;
use App\AcadamicYear;
use App\Http\Requests\CourseRequest;
use App\Traits\ActivityTraits;
use Sentinel;

class CourseController extends Controller
{
    use ActivityTraits;
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courseModel =new Course();
        $courses = $courseModel->fetchCourses();
        // Show the page
        return view('admin.course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $classes = Classes::all();
        $academic_years   = AcadamicYear::all();
        $years   = Year::all();
        $quarters = Quarter::all();
        return view('admin.course.create', compact('classes','years','quarters','academic_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {   
        $course_photo = "";
        //upload image
        if ($file = $request->file('course_photo')) {
            $extension = $file->extension()?: 'png';
            $destinationPath = public_path() . '/uploads/course/';
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
            $course_photo = $safeName;
        }

        $course = new Course([
            'class_id'=> $request->class_id,
            'academic_year_id'=> $request->academic_year_id,
            'year_id'=> $request->year_id,
            'quarter_id' => $request->quarter_id,
            'course_name' => $request->course_name,
            'course_code' => $request->course_code,
            'course_photo' => $course_photo
        ]);


        if ($course->save()) {

            $settingParms = $course->toArray();
            $changes = Sentinel::getUser()->username . ' added new course name as  '.'"'.$course->course_name.'"';
            $this->logCreatedActivity($course,$changes,$settingParms);

            return redirect('admin/courses')->with('success', trans('course/message.success.create'));
        } else {
            return Redirect::route('admin/courses')->withInput()->with('error', trans('course/message.error.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $courseModel = new Course();
        $course = $courseModel->detail($id);
        return view('admin.course.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $course,$id)
    {   

        $classes = Classes::all();
        $academic_years   = AcadamicYear::all();
        $years = Year::all();
        $quarters = Quarter::all();
        $course = Course::find($id);
        return view('admin.course.edit', compact('classes','years','quarters','course','academic_years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Course $course
     * @return Response
     */

    public function update(CourseRequest $request,$id)
    {   

        $course=Course::find($id);
        $beforeUpdateValues = $course->toArray();

        $course->class_id = $request->get('class_id');
        $course->academic_year_id = $request->get('academic_year_id');
        $course->year_id = $request->get('year_id');
        $course->quarter_id = $request->get('quarter_id');
        $course->course_name = $request->get('course_name');
        $course->course_code = $request->get('course_code');

        //upload image
        if ($file = $request->file('course_photo')) {
            $extension = $file->extension()?: 'png';
            $destinationPath = public_path() . '/uploads/course/';
            $saveName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $saveName);
            $course->course_photo = $saveName;  
        }
        
        $result = $course->save();

        
        if ($result) {

            $afterUpdateValues = $course->getChanges();
            $this->logUpdatedActivity($course,$beforeUpdateValues,$afterUpdateValues);

            return redirect('admin/courses')->with('success', trans('course/message.success.update'));
        } else {
             return Redirect::route('admin/courses')->withInput()->with('error', trans('course/message.error.update'));
        }
    }



    /**
     * Remove course.
     *
     * @param course $course
     * @return Response
     */
    public function getModalDelete(Course $course)
    {   

        $model = 'course';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.course.delete', ['id' => $course->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('course/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        if ($course->delete()) {
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$course->course_name.'"'.' from Course';
            $this->logDeletedActivity($course,$changeLog);
            return redirect('admin/courses')->with('success', trans('course/message.success.delete'));
        } else {
            return Redirect::route('admin/courses')->withInput()->with('error', trans('course/message.error.delete'));
        }
    }


}
