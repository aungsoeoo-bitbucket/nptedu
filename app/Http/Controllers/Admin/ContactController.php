<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SaveContact;
use Redirect;
use App\Traits\ActivityTraits;
use Sentinel;

class ContactController extends Controller
{   
    use ActivityTraits;
    public function index()
    {   
        $contacts = SaveContact::paginate(10);
        // Show the page
        return view('admin.contact.index', compact('contacts'));
    }

    public function show($id)
    {
        $contact = SaveContact::findorfail($id);
        // Show the page
        return view('admin.contact.show', compact('contact'));
    }


    /* Remove SaveContact.
     *
     * @param SaveContact $class
     * @return Response
     */
    public function getModalDelete(Request $request,$id)
    {   
        $model = 'contacts';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.contacts.delete', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('contacts/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  SaveContact $SaveContact
     * @return Response
     */
    public function destroy(Request $request,$id)
    {	
    	$contact = SaveContact::findorfail($id);
        $changeLog='';
        $user = Sentinel::getUser();

        
        if ($contact->delete()) {
            $this->logDeletedActivity($contact,$changeLog);

            return redirect('admin/contacts')->with('success', trans('contacts/message.success.delete'));
        } else {
            return Redirect::route('admin.contacts.index')->withInput()->with('error', trans('contacts/message.error.delete'));
        }
    }

}
