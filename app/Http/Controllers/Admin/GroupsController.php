<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Group;
use Redirect;
use App\Http\Requests\GroupRequest;


class GroupsController extends Controller
{
    /**
     * Show a list of all the groups.
     *
     * @return View
     */
    public function index()
    {
        // Grab all the groups
        $groups = Group::all();

        // Show the page
        return view('admin.groups.index', compact('groups'));
    }

    /**
     * Group create.
     *
     * @return View
     */
    public function create()
    {
        // Show the page
        return view ('admin.groups.create');
    }

    /**
     * Group create form processing.
     *
     * @return Redirect
     */
    public function store(GroupRequest $request)
    {   
        $group = new Group($request->all());

        if ($group->save()) {
             // Redirect to the new group page
            return Redirect::route('admin.groups.index')->with('success', trans('groups/message.success.create'));
        } else {
            // Redirect to the group create page
        return Redirect::route('admin.groups.create')->withInput()->with('error', trans('groups/message.error.create'));

        }
    }


    /**
     * Group update.
     *
     * @param  int $id
     * @return View
     */
    public function edit($id)
    {
         $group = Group::find($id);
        // Show the page
        return view('admin.groups.edit', compact('group'));
    }

    /**
     * Group update form processing page.
     *
     * @param  int $id
     * @return Redirect
     */
    public function update(GroupRequest $request,$id)
    {     
        $group = Group::findOrfail($id);
        if ($group->update($request->all())) {
            return redirect('admin/groups')->with('success', trans('groups/message.success.update'));
        } else {
             return Redirect::route('admin/groups')->withInput()->with('error', trans('groups/message.error.update'));
        }
    }

    /**
     * Delete confirmation for the given group.
     *
     * @param  int $id
     * @return View
     */
    public function getModalDelete(Group $group)
    {   

        $model = 'groups';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.groups.delete', ['id' => $group->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('groups/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    
    }

    /**
     * Delete the given group.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy(Group $group)
    {
        if ($group->delete()) {
            return redirect('admin/groups')->with('success', trans('groups/message.success.delete'));
        } else {
            return Redirect::route('admin/groups')->withInput()->with('error', trans('groups/message.error.delete'));
        }
    }

}
