<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feedback;
use Redirect;
use App\Traits\ActivityTraits;
use Sentinel;

class FeedbackController extends Controller
{   
    use ActivityTraits;

    public function index()
    {
        $feedbacks = Feedback::orderBy('created_at','desc')->paginate(10);
        // Show the page
        return view('admin.feedback.index', compact('feedbacks'));
    }

    public function show($id)
    {
        $feedback = Feedback::findorfail($id);
        // Show the page
        return view('admin.feedback.show', compact('feedback'));
    }


    /* Remove Feedback.
     *
     * @param Feedback $class
     * @return Response
     */
    public function getModalDelete(Request $request,$id)
    {   
        $model = 'feedbacks';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.feedback.delete', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('feedbacks/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Feedback $Feedback
     * @return Response
     */
    public function destroy(Request $request,$id)
    {	
    	$feedback = Feedback::findorfail($id);
        if ($feedback->delete()) {
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$feedback->name.'"'.' from Feedback';
            $this->logDeletedActivity($feedback,$changeLog);

            return redirect('admin/feedback')->with('success', trans('feedbacks/message.success.delete'));
        } else {
            return Redirect::route('admin.feedback.index')->withInput()->with('error', trans('feedbacks/message.error.delete'));
        }
    }

}
