<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CourseManagement;
use App\Http\Requests\CourseManagementRequest;
use App\Quarter;
use App\Course;
use App\FileDownload ;
use DB;
use Redirect;
use App\Traits\ActivityTraits;
use Sentinel;

class CourseManagementController extends Controller
{
     use ActivityTraits;
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = CourseManagement::with(['course','filedownload','user'])->orderBy('publish_date','desc')->get();
        // dd($courses);
        // Show the page
        return view('admin.course-management.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $quarters = Quarter::all();

        return view('admin.course-management.create', compact('quarters'));
    }

    // get courses by quarter
    public function getCoursesByQuarter(Request $request)
    {   

        $quarter_id = $request->quarter_id;
        if(isset($quarter_id) && $quarter_id!=''){
            $courses = Course::where('quarter_id','=',$quarter_id)->get();
            echo "<option value=''>Select Course</option>";

            foreach ($courses as $c) {
                echo "<option value='".$c->id."' {{ old('course_id') }} >".$c->course_name."</option>";
            }
            
            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseManagementRequest $request)
    {   
        $destinationPath = public_path() . '/uploads/files/';
        $document="";
        if($request->file('document')){
            $file = $request->file('document');
            $extension = $file->getClientOriginalExtension();
            $document = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $document);
        }

        $audio="";
        if($request->file('audio')){
            $file = $request->file('audio');
            $extension = $file->getClientOriginalExtension();
            $audio = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $audio);
        }

        $video="";
        if($request->file('video')){
            $file = $request->file('video');
            $extension = $file->getClientOriginalExtension();
            $video = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $video);
        }

      $data=[
            'quarter_id' => $request->quarter_id,
            'course_id' => $request->course_id,
            'title' => $request->title,
            'description' => $request->description,
            'publish_date' =>  date('Y-m-d', strtotime($request->publish_date)),
            'document' => $document,
            'audio' => $audio,
            'video' => $video,
            'login_id' => Sentinel::getuser()->id,    
          
        ];

        $res=CourseManagement::create($data);

        if ($res) {
            $settingParms = $res->toArray();
            $changes = Sentinel::getUser()->username . ' added new data in Course Management name as  '.'"'.$res->title.'"';
            $this->logCreatedActivity($res,$changes,$settingParms);

            return redirect('admin/course-management')->with('success', trans('course-management/message.success.create'));
        } else {
            return Redirect::route('admin/course-management')->withInput()->with('error', trans('course-management/message.error.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $course = new CourseManagement();

        $course = $course->leftJoin('quarters','quarters.id', '=', 'course_managements.quarter_id')
                        ->leftJoin('courses','courses.id', '=', 'course_managements.course_id')
                        ->leftJoin('year','year.id', '=', 'courses.year_id')
                        ->leftJoin('classes','classes.id', '=', 'courses.class_id')
                        ->select(
                            'course_managements.id as id',
                            'course_managements.title as title',
                            'course_managements.description as description',
                            'course_managements.publish_date as publish_date',
                            'course_managements.document as document',
                            'course_managements.audio as audio',
                            'course_managements.video as video',
                            'courses.course_name as course_name',
                            'quarters.quarter_name as quarter_name',
                            'year.year as year',
                            'classes.class_name as class_name'
                       ); 

        $course = $course->where('course_managements.id','=',$id)->get();


        $downloadfiles = DB::table('filedownloads')
                    ->selectRaw('count(c_id) as download_count, username')
                    ->leftJoin('course_managements','course_managements.id', '=', 'filedownloads.c_id')
                    ->leftJoin('users','users.id', '=', 'filedownloads.user_id')
                    ->where('filedownloads.c_id',$id)
                     ->groupBy('user_id')
                     ->paginate(10);

        return view('admin.course-management.show', compact('course','downloadfiles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {   

        $quarters = Quarter::all();
        $courses =  Course::all();
        $management = CourseManagement::find($id);
        return view('admin.course-management.edit', compact('courses','quarters','management'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Quarter $class
     * @return Response
     */

    public function update(CourseManagementRequest $request,$id)
    {   

         $management=CourseManagement::find($id);
         $beforeUpdateValues = $management->toArray();

         $management->quarter_id = $request->get('quarter_id');
         $management->course_id = $request->get('course_id');
         $management->title = $request->get('title');
         $management->description = $request->get('description');
         $management->publish_date = $request->get('publish_date');
         $management->login_id = Sentinel::getuser()->id;

        //upload image
        $destinationPath = public_path() . '/uploads/files/';

        if($request->file('document')){
            $file = $request->file('document');
            $extension = $file->getClientOriginalExtension();
            $document = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $document);
            $management->document = $document;
        }


        if($request->file('audio')){
            $file = $request->file('audio');
            $extension = $file->getClientOriginalExtension();
            $audio = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $audio);
            $management->audio = $audio;
        }

        if($request->file('video')){
            $file = $request->file('video');
            $extension = $file->getClientOriginalExtension();
            $video = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $video);
            $management->video = $video;
        }

        $result = $management->save();

        $afterUpdateValues = $management->getChanges();
        $this->logUpdatedActivity($management,$beforeUpdateValues,$afterUpdateValues);


        if ($result) {
            return redirect('admin/course-management')->with('success', trans('course-management/message.success.update'));
        } else {
             return Redirect::route('admin/course-management')->withInput()->with('error', trans('course-management/message.error.update'));
        }
    }



    /**
     * Remove Quarter.
     *
     * @param Quarter $quarter
     * @return Response
     */
    public function getModalDelete($id)
    {   
        $data = CourseManagement::find($id);
        $model = 'course-management';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.course-management.delete', ['id' => $data->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('course-management/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $course = CourseManagement::findorfail($id);
        if ($course->delete()) {
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$course->title.'"'.' from Course';
            $this->logDeletedActivity($course,$changeLog);

            return redirect('admin/course-management')->with('success', trans('course-management/message.success.delete'));
        } else {
            return Redirect::route('admin.course-management.index')->withInput()->with('error', trans('course-management/message.error.delete'));
        }
    }
}
