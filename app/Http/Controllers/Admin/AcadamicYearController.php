<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AcadamicYear;
use Redirect;
use App\Http\Requests\AcadamicYearRequest;
use App\Traits\ActivityTraits;
use Sentinel;


class AcadamicYearController extends Controller
{   
    use ActivityTraits;
    /**
     * Show a list of all the AcadamicYear.
     *
     * @return View
     */
    public function index()
    {	
        // Grab all the AcadamicYear
        $acadamicyears = AcadamicYear::all();

        // Show the page
        return view('admin.acadamic_year.index', compact('acadamicyears'));
    }

    /**
     * Year create.
     *
     * @return View
     */
    public function create()
    {
        // Show the page
        return view ('admin.acadamic_year.create');
    }

    /**
     * Year create form processing.
     *
     * @return Redirect
     */
    public function store(AcadamicYearRequest $request)
    {   
        $year = new AcadamicYear($request->all());

        if ($year->save()) {

            $settingParms = $year->toArray();
            $changes = Sentinel::getUser()->username . ' added new acadamic batch name as  '.'"'.$year->acadamic_year.'"';
            $this->logCreatedActivity($year,$changes,$settingParms);

             // Redirect to the new Year page
            return Redirect::route('admin.acadamic_years.index')->with('success', trans('acadamicyear/message.success.create'));
        } else {
            // Redirect to the Year create page
        return Redirect::route('admin.acadamic_years.create')->withInput()->with('error', trans('acadamicyear/message.error.create'));

        }
    }


    /**
     * Year update.
     *
     * @param  int $id
     * @return View
     */
    public function edit($id)
    {
         $year = AcadamicYear::find($id);
        // Show the page
        return view('admin.acadamic_year.edit', compact('year'));
    }

    /**
     * Year update form processing page.
     *
     * @param  int $id
     * @return Redirect
     */
    public function update(AcadamicYearRequest $request,$id)
    {     
        $Year = AcadamicYear::findOrfail($id);
        $beforeUpdateValues = $Year->toArray();

        if ($Year->update($request->all())) {

            $afterUpdateValues = $Year->getChanges();
            $this->logUpdatedActivity($Year,$beforeUpdateValues,$afterUpdateValues);

            return redirect('admin/acadamic_years')->with('success', trans('acadamicyear/message.success.update'));
        } else {
             return Redirect::route('admin/acadamic_year')->withInput()->with('error', trans('acadamicyear/message.error.update'));
        }
    }

    /**
     * Delete confirmation for the given Year.
     *
     * @param  int $id
     * @return View
     */
    public function getModalDelete($id)
    {   
        $acadamic_year= AcadamicYear::find($id);
        $id = $acadamic_year->id;
        $model = 'acadamicyear';

        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.acadamic_years.delete', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (Exception $e) {

            $error = trans('acadamicyear/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    
    }

    /**
     * Delete the given year.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy($id)
    {   

        $acadamicyear=AcadamicYear::findOrfail($id);
        if ($acadamicyear->delete()) {
            
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$acadamicyear->acadamic_year.'"'.' from Acadamic Batch';
            $this->logDeletedActivity($acadamicyear,$changeLog);

            return redirect('admin/acadamic_years')->with('success', trans('acadamicyear/message.success.delete'));
        } else {
            return redirect('admin/acadamic_years')->withInput()->with('error', trans('acadamicyear/message.error.delete'));
        }
    }
}
