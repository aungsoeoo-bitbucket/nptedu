<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Professor;
use App\Http\Requests\ProfessorRequest;
use App\Traits\ActivityTraits;
use Sentinel;

class ProfessorController extends Controller
{   
    use ActivityTraits;
    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professors = Professor::orderBy('created_at','desc')->get();
        // Show the page
        return view('admin.professor.index', compact('professors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('admin.professor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfessorRequest $request)
    {   
        $photo = "";
        //upload image
        if ($file = $request->file('photo')) {
            $extension = $file->extension()?: 'png';
            $destinationPath = public_path() . '/uploads/professor/';
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
            $photo = $safeName;
        }

        $professor = new Professor([
            'name'=> $request->name,
            'gender'=> $request->gender,
            'position' => $request->position,
            'bio' => $request->bio,
            'phone'=> $request->phone,
            'email' => $request->email,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'linkedin' => $request->linkedin,
            'photo' => $photo,
            'active' => $request->active,
        ]);

        if ($professor->save()) {
            $settingParms = $professor->toArray();
            $changes = Sentinel::getUser()->username . ' added new professor name as  '.'"'.$professor->name.'"';
            $this->logCreatedActivity($professor,$changes,$settingParms);


            return redirect('admin/professor')->with('success', trans('professor/message.success.create'));
        } else {
            return Redirect::route('admin/professor')->withInput()->with('error', trans('professor/message.error.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $professor =Professor::findorfail($id);
        return view('admin.professor.show', compact('professor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $professor,$id)
    {   
        $professor = Professor::find($id);
        return view('admin.professor.edit', compact('professor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Quarter $class
     * @return Response
     */

    public function update(ProfessorRequest $request,$id)
    {   

         $professor=Professor::find($id);
        
         $beforeUpdateValues = $professor->toArray();

         $professor->name = $request->get('name');
         $professor->gender = $request->get('gender');
         $professor->position = $request->get('position');
         $professor->bio = $request->get('bio');
         $professor->phone = $request->get('phone');
         $professor->email = $request->get('email');
         $professor->facebook = $request->get('facebook');
         $professor->twitter = $request->get('twitter');
         $professor->linkedin = $request->get('linkedin');
         $professor->active = $request->get('active');

        //upload image
        if ($file = $request->file('photo')) {
            $extension = $file->extension()?: 'png';
            $destinationPath = public_path() . '/uploads/professor/';
            $saveName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $saveName);
            $professor->photo = $saveName;  
        }


        
        $result = $professor->save();
        $afterUpdateValues = $professor->getChanges();

        $this->logUpdatedActivity($professor,$beforeUpdateValues,$afterUpdateValues);

        
        if ($result) {
            return redirect('admin/professor')->with('success', trans('professor/message.success.update'));
        } else {
             return Redirect::route('admin/professor')->withInput()->with('error', trans('professor/message.error.update'));
        }
    }



    /**
     * Remove Professor.
     *
     * @param Professor $professor
     * @return Response
     */
    public function getModalDelete(Professor $professor)
    {   

        $model = 'professor';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.professor.delete', ['id' => $professor->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('professor/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Professor $professor)
    {
        if ($professor->delete()) {
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$professor->name.'"'.' from professor';
            $this->logDeletedActivity($professor,$changeLog);
            return redirect('admin/professor')->with('success', trans('professor/message.success.delete'));
        } else {
            return Redirect::route('admin/professor')->withInput()->with('error', trans('professor/message.error.delete'));
        }
    }
}
