<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\About;
use App\Http\Requests\AboutRequest;


class AboutController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts =About::all();
        // Show the page
        return view('admin.about.index', compact('abouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('admin.about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $image ='';
        if($input=$request['images'])
        {
        	$destinationPath = public_path() . '/uploads/about/';
            foreach($input as $file)
            {
            	$extension = $file->getClientOriginalExtension();
            	$name = str_random(10) . '.' . $extension;
            	$file->move($destinationPath, $name);
              	$images = $name;
            }
        }

        $data = array(
                'brief_en' => $request->brief_en,
                'description_en' => $request->description_en,
                'images'=> $image
        );

        $res=About::create($data); 
              

        if ($res) {
            return redirect('admin/about')->with('success', trans('about/message.success.create'));
        } else {
            return Redirect::route('admin/about')->withInput()->with('error', trans('about/message.error.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {   
        $about = About::find($id);
        return view('admin.about.edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Quarter $class
     * @return Response
     */

    public function update(Request $request,$id)
    {   

        $about=About::find($id);
        $about->brief_en = $request->get('brief_en');
        $about->description_en = $request->get('description_en');
      
        if($input=$request['images'])
        {
        	$destinationPath = public_path() . '/uploads/about/';
            foreach($input as $file)
            {
            	$extension = $file->getClientOriginalExtension();
            	$name = str_random(10) . '.' . $extension;
            	$file->move($destinationPath, $name);
              	$image = $name;
            }
        }
        $data = array(
            'brief_en' => $request->brief_en,
            'description_en' => $request->description_en,
            'images'=> $image
        );

        $result = About::where('id', $request['id'])->update($data);

        
        if ($result) {
            return redirect('admin/about')->with('success', trans('about/message.success.update'));
        } else {
             return Redirect::route('admin/about')->withInput()->with('error', trans('about/message.error.update'));
        }
    }



    /**
     * Remove Quarter.
     *
     * @param Quarter $quarter
     * @return Response
     */
    public function getModalDelete(About $about)
    {   

        $model = 'about';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.about.delete', ['id' => $about->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('about/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        if ($about->delete()) {
            return redirect('admin/about')->with('success', trans('about/message.success.delete'));
        } else {
            return Redirect::route('admin/about')->withInput()->with('error', trans('about/message.error.delete'));
        }
    }
}
