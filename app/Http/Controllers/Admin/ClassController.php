<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes;
use App\Http\Requests\ClassRequest;
use App\Traits\ActivityTraits;
use Sentinel;

class ClassController extends Controller
{   
    use ActivityTraits;
     /**
     * Show a list of all class.
     *
     * @return View
     */

    public function index()
    {
        $classes = Classes::all();
        // Show the page
        return view('admin.class.index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.class.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ClassRequest $request)
    {
        $class = new Classes($request->all());

        if ($class->save()) {

            $settingParms = $class->toArray();
            $changes = Sentinel::getUser()->username . ' added new class name as  '.'"'.$class->class_name.'"';
            $this->logCreatedActivity($class,$changes,$settingParms);


            return redirect('admin/classes')->with('success', trans('class/message.success.create'));
        } else {
            return Redirect::route('admin/classes')->withInput()->with('error', trans('class/message.error.create'));
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  Class $blog
     * @return view
     */
    public function show(Classes $class)
    {
        $class = Classes::find($class->id);

        return view('admin.class.show', compact('class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Classes $Classes
     * @return view
     */
    public function edit(Classes $classes,$id)
    {   
        $class = Classes::find($id);
        return view('admin.class.edit', compact('class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Class $class
     * @return Response
     */

    public function update(ClassRequest $request, Classes $class)
    {   
        $beforeUpdateValues = $class->toArray();
        if ($class->update($request->all())) {
            $afterUpdateValues = $class->getChanges();
            $this->logUpdatedActivity($class,$beforeUpdateValues,$afterUpdateValues);
            return redirect('admin/classes')->with('success', trans('class/message.success.update'));
        } else {
             return Redirect::route('admin/classes')->withInput()->with('error', trans('class/message.error.update'));
        }
    }



    /**
     * Remove Class.
     *
     * @param Class $class
     * @return Response
     */
    public function getModalDelete(Classes $class)
    {   

        $model = 'class';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.classes.delete', ['id' => $class->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('class/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Class $class
     * @return Response
     */
    public function destroy(Classes $class)
    {
        if ($class->delete()) {
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$class->class_name.'"'.' from Class';
            $this->logDeletedActivity($class,$changeLog);
            return redirect('admin/classes')->with('success', trans('class/message.success.delete'));
        } else {
            return Redirect::route('admin/classes')->withInput()->with('error', trans('class/message.error.delete'));
        }
    }

}
