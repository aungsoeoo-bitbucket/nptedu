<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Thesis;
use App\ThesisGroup;
use App\Group;
use App\User;

class ThesisController extends Controller
{
    
    public function index()
    {
        $data =Thesis::with('thesisGp.student')->get();
        // Show the page
        return view('admin.thesis.index', compact('data'));
    }


    public function create()
    {   
    	$groups = Group::all();
    	$students = User::where('id','!=',1)->get();
        return view('admin.thesis.create',compact('groups','students'));
    }


    public function dataAjax(Request $request)
    {
    	$term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $users = User::where('roll_no','like',$term)
        			   ->orwhere('username','like','%'.$term.'%')
        			   ->orwhere('username_en','like','%'.$term.'%')
        			   ->get();

        $formatted_users = [];

        foreach ($users as $user) {
            $formatted_users[] = ['id' => $user->id, 'text' => $user->roll_no .'  -  '. $user->username_en    ];
        }

        return \Response::json($formatted_users);
    }


    public function store(Request $request)
    {	

    	$data=[
	            'group_id' => $request->group_id,
	            'attend_date' => date('Y-m-d', strtotime($request->attend_date)),       
	        ];

	    $data=Thesis::create($data);

	    $id = $data->id;


    	$user_arr = $request->input('user_id');

    	foreach ($user_arr as $key => $value) {

    		$data=[
    			'thesis_id' => $id,
	            'user_id' => $value,    
	        ];

	        $res=ThesisGroup::create($data);
    	}

    	

        if ($res) {
            return redirect('admin/thesis')->with('success', 'Thesis group create successfully.');
        } else {
            return Redirect::route('admin/thesis')->withInput()->with('error', 'Error in creating Thesis group.');
        }
    }

    public function edit($id)
    {   
    	$groups = Group::all();
    	$students = User::where('id','!=',1)->get();
        $thesis = Thesis::with('thesisGp.student')->find($id);
        return view('admin.thesis.edit', compact('groups','thesis','students'));
    }

    public function update(Request $request,$id)
    {	
    	$data = Thesis::find($id);

    	$data->group_id  = $request->group_id;
    	$data->attend_date  = date('Y-m-d', strtotime($request->attend_date));
    	$result = $data->save();

		$deleteold = ThesisGroup::where('thesis_id',$id)->delete();


    	$user_arr = $request->input('user_id');

    	foreach ($user_arr as $key => $value) {

    		$data=[
    			'thesis_id' => $id,
	            'user_id' => $value,    
	        ];

	        $res=ThesisGroup::create($data);
    	}

    	

        if ($res) {
            return redirect('admin/thesis')->with('success', 'Thesis group create successfully.');
        } else {
            return Redirect::route('admin/thesis')->withInput()->with('error', 'Error in creating Thesis group.');
        }
    }

    public function getModalDelete(Thesis $thesis)
    {   

        $model = 'thesis';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.thesis.delete', ['id' => $thesis->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('thesis/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thesis $thesis)
    {
        if ($thesis->delete()) {

        	$deleteold = ThesisGroup::where('thesis_id',$thesis->id)->delete();

            return redirect('admin/thesis')->with('success', trans('thesis/message.success.delete'));
        } else {
            return Redirect::route('admin/thesis')->withInput()->with('error', trans('thesis/message.error.delete'));
        }
    }

}
