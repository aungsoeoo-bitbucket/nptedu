<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Year;
use Redirect;
use App\Http\Requests\YearRequest;
use App\Traits\ActivityTraits;
use Sentinel;

class YearController extends Controller
{   
    use ActivityTraits;
    /**
     * Show a list of all the Years.
     *
     * @return View
     */
    public function index()
    {	
        // Grab all the Years
        $years = Year::all();

        // Show the page
        return view('admin.class_of_year.index', compact('years'));
    }

    /**
     * Year create.
     *
     * @return View
     */
    public function create()
    {
        // Show the page
        return view ('admin.class_of_year.create');
    }

    /**
     * Year create form processing.
     *
     * @return Redirect
     */
    public function store(YearRequest $request)
    {   
        $year = new Year($request->all());

        if ($year->save()) {
            $settingParms = $year->toArray();
            $changes = Sentinel::getUser()->username . ' added new class of  year name as  '.'"'.$year->year.'"';
            $this->logCreatedActivity($year,$changes,$settingParms);

             // Redirect to the new Year page
            return Redirect::route('admin.years.index')->with('success', trans('year/message.success.create'));
        } else {
            // Redirect to the Year create page
        return Redirect::route('admin.years.create')->withInput()->with('error', trans('year/message.error.create'));

        }
    }


    /**
     * Year update.
     *
     * @param  int $id
     * @return View
     */
    public function edit($id)
    {
         $year = Year::find($id);
        // Show the page
        return view('admin.class_of_year.edit', compact('year'));
    }

    /**
     * Year update form processing page.
     *
     * @param  int $id
     * @return Redirect
     */
    public function update(YearRequest $request,$id)
    {     
        $Year = Year::findOrfail($id);

        $beforeUpdateValues = $Year->toArray();

        if ($Year->update($request->all())) {

            $afterUpdateValues = $Year->getChanges();
            $this->logUpdatedActivity($Year,$beforeUpdateValues,$afterUpdateValues);

            return redirect('admin/years')->with('success', trans('year/message.success.update'));
        } else {
             return Redirect::route('admin/years')->withInput()->with('error', trans('years/message.error.update'));
        }
    }

    /**
     * Delete confirmation for the given Year.
     *
     * @param  int $id
     * @return View
     */
    public function getModalDelete(Year $year)
    {   

        $model = 'year';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.year.delete', ['id' => $year->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (Exception $e) {

            $error = trans('years/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    
    }

    /**
     * Delete the given year.
     *
     * @param  int $id
     * @return Redirect
     */
    public function destroy(Year $year)
    {
        if ($year->delete()) {
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$year->year.'"'.' from Class of Year';
            $this->logDeletedActivity($year,$changeLog);


            return redirect('admin/years')->with('success', trans('year/message.success.delete'));
        } else {
            return Redirect::route('admin/years')->withInput()->with('error', trans('years/message.error.delete'));
        }
    }
}
