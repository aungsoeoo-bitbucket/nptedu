<?php

namespace App\Http\Controllers\Admin;

use App\BatchViva;
use App\ThesisStatus;
use App\BatchGP;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BatchVivaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $batchVivas = BatchViva::with('student')->get();
        // dd($batchVivas);
        return view('admin.batch_viva.index', compact('batchVivas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = ThesisStatus::orderBy('order_no','asc')->get();
        $students = User::where('id','!=',1)->get();
        return view('admin.batch_viva.create',compact('statuses','students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_arr = $request->input('user_id');

        foreach ($user_arr as $key => $value) {

            $data=[
                'thisis_status_id' => $request->status_id,
                'pass_date'=> $request->pass_date != '' ? date('Y-m-d', strtotime($request->pass_date)): NULL,
                'complete_date'=> $request->complete_date != '' ? date('Y-m-d', strtotime($request->complete_date)):NULL,
                'std_id' => $value,    
            ];

            $res=BatchViva::create($data);
        }

        

        if ($res) {
            return redirect('admin/batch_viva')->with('success', 'Create successfully.');
        } else {
            return Redirect::route('admin/batch_viva')->withInput()->with('error', 'Error in creating');
        }

        // $data=[
        //         'thisis_status_id' => $request->status_id,
        //         'pass_date'=>date('Y-m-d', strtotime($request->pass_date)),
        //         'complete_date'=>date('Y-m-d', strtotime($request->complete_date)),      
        //     ];

        // $data=BatchViva::create($data);

        // $id = $data->id;


        // $user_arr = $request->input('user_id');

        // foreach ($user_arr as $key => $value) {

        //     $data=[
        //         'batch_id' => $id,
        //         'user_id' => $value,    
        //     ];

        //     $res=BatchGP::create($data);
        // }

        

        // if ($res) {
        //     return redirect('admin/batch_viva')->with('success', 'Thesis group create successfully.');
        // } else {
        //     return Redirect::route('admin/batch_viva')->withInput()->with('error', 'Error in creating Thesis group.');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BatchViva  $batchViva
     * @return \Illuminate\Http\Response
     */
    public function show(BatchViva $batchViva)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BatchViva  $batchViva
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $statuses = ThesisStatus::orderBy('order_no','asc')->get();
        $students = User::where('id','!=',1)->get();
        $batchVivas = BatchViva::with('student')->find($id);
        return view('admin.batch_viva.edit', compact('statuses','batchVivas','students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BatchViva  $batchViva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $user_arr = $request->input('user_id');

        foreach ($user_arr as $key => $value) {

            $data=[
                'thisis_status_id' => $request->status_id,
                'pass_date'=>date('Y-m-d', strtotime($request->pass_date)),
                'complete_date'=>date('Y-m-d', strtotime($request->complete_date)),
                'std_id' => $value,    
            ];

            $res = BatchViva::find($id);
            $res = $res->update($data);

            // $res=BatchViva::create($data);
        }

        // $data = BatchViva::find($id);

        // $data->thisis_status_id  = $request->status_id;
        // $data->pass_date  = date('Y-m-d', strtotime($request->pass_date));
        // $data->complete_date  = date('Y-m-d', strtotime($request->complete_date));
        // $result = $data->save();

        // $deleteold = BatchGP::where('batch_id',$id)->delete();


        // $user_arr = $request->input('user_id');

        // foreach ($user_arr as $key => $value) {

        //     $data=[
        //         'batch_id' => $id,
        //         'user_id' => $value,    
        //     ];

        //     $res=BatchGP::create($data);
        // }

        

        if ($res) {
            return redirect('admin/batch_viva')->with('success', 'Create successfully.');
        } else {
            return Redirect::route('admin/batch_viva')->withInput()->with('error', 'Error in creating');
        }
    }

    public function getModalDelete(BatchViva $batch_viva)
    {   
        $model = 'thesis_status';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.batch_viva.delete', ['id' => $batch_viva->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('thesis/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BatchViva  $batchViva
     * @return \Illuminate\Http\Response
     */
    public function destroy(BatchViva $batchViva)
    {
        // if ($batchViva->delete()) {

        //     $deleteold = BatchGP::where('batch_id',$batchViva->id)->delete();

        //     return redirect('admin/batch_viva')->with('success', trans('thesis/message.success.delete'));
        // } else {
        //     return Redirect::route('admin/batch_viva')->withInput()->with('error', trans('thesis/message.error.delete'));
        // }

        if ($batchViva->delete()) {
            return redirect('admin/batch_viva')->with('success', trans('thesis/message.success.delete'));
        }else{
            return Redirect::route('admin/batch_viva')->withInput()->with('error', trans('thesis/message.error.delete'));
        }
    }
}
