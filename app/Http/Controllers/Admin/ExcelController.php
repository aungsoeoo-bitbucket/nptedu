<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Exports\UsersExport;
use App\Imports\UsersImport;

use App\ExamResult;
use App\Exports\ExamResultExport;

use Maatwebsite\Excel\Facades\Excel;


class ExcelController extends Controller
{
    
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function userExport() 
    {
        $res =  Excel::download(new UsersExport, 'users.xlsx');
        return $res;
    }

    public function resultExport()
    {
        $res =  Excel::download(new ExamResultExport, 'results.xlsx');
        return $res;
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import() 
    {
        $res = Excel::import(new UsersImport,request()->file('file'));

        return back();
    }    
}
