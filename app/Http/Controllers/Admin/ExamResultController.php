<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use App\Quarter;
use App\Exam;
use App\User;
use App\Course;
use App\ExamResult;
use App\Http\Requests\ExamResultRequest;
use App\Year;
use App\AcadamicYear;
use DB;
use App\Traits\ActivityTraits;
use Sentinel;

class ExamResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $batch_id = $request->batch_id;
        $keyword = $request->keyword;
        $quarter_id = $request->quarter_id;
        $course_id = $request->course_id;
        $grade = $request->grade;

        $batchs = AcadamicYear::all();
        $quarters = Quarter::all();
        $courses = Course::all();
        $users = User::select('id','username','roll_no')->where('id','!=',1)->get();

        $query = new ExamResult();
        $query = $query->leftJoin('users','users.id', '=', 'exam_results.user_id')
                        ->leftJoin('quarters','quarters.id', '=', 'exam_results.quarter_id')
                        ->leftJoin('courses','courses.id', '=', 'exam_results.course_id')
                        ->select(
                            'exam_results.id as  id',
                            'users.username as  username',
                            'users.roll_no as  roll_no',
                            'quarters.quarter_name as  quarter_name',
                            'courses.course_code as  course_code',
                            'exam_results.grade as  grade'
                       ); 

        if($batch_id!=''){
            $query =$query->where('exam_results.batch_id',$batch_id);
        }

        if($keyword!=''){
            // $results =ExamResult::with(['exam','student'=> function($query) use ($keyword){
            //             $query->where('username','like','%'. $keyword.'%');
            //      }])->orderBy('user_id')->paginate(10);
           $query = $query->where('users.username','like','%'.$keyword.'%')->orWhere('users.roll_no','like','%'.$keyword.'%');
        }
        if($quarter_id!=''){
            $query =$query->where('exam_results.quarter_id',$quarter_id);
        }

        if((isset($request->course_id) && $course_id!='' )){
            $query =$query->where('exam_results.course_id',$course_id);
        }

        if($grade!=''){
            $query =$query->where('exam_results.grade',$grade);
        }


        $results =$query->orderBy('exam_results.created_at','desc')->paginate(10);

        // dd($results);

        return view('admin.exam_result.index', compact('results','quarters','courses','users','batchs'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $batchs = AcadamicYear::all(); 
        $years = Year::all(); 
    	$quarters = Quarter::all();		
    	$students = User::where('id','!=',1)->get();
        return view('admin.exam_result.create',compact('quarters','students','years','batchs'));
    }


    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */


    public function dataAjax(Request $request)
    {
        $data = [];

        $data = DB::table("users")
                    ->select("id","username_en","roll_no")
                    ->where('id','!=',1);         
                    

        $search = $request->q;
        $batch_id = $request->batch;


        if($batch_id!=''){
            $data = $data->where(function ($query) use ($batch_id) {
                $query->where('acadamic_year_id', '=', $batch_id);
            });
        }


        if($search!=''){

            $data = $data->where(function ($query) use ($search) {
                $query->where('username_en','LIKE',"%$search%");
                $query->orwhere('roll_no','LIKE',"%$search%");
            });
        }

        $data= $data->get();


        return response()->json($data);
    }

     // get courses by quarter
    public function getQuarterByYear(Request $request)
    {   

        $year_id = $request->year_id;
        if(isset($year_id) && $year_id!=''){
            $quarters = Quarter::where('year_id','=',$year_id)->get();
            echo "<option value=''>Select Quarter</option>";

            foreach ($quarters as $q) {
                echo "<option value='".$q->id."' {{ old('quarter_id') }} >".$q->quarter_name."</option>";
            }
            
            
        }
    }

    public function getCoursesByQuarter(Request $request)
    {
    	// $query = new Course();
    	// $query = $query->leftJoin('years','years.id', '=', 'courses.year_id')
					// 	->leftJoin('quarters','quarters.id', '=', 'courses.quarter_id')
     //                   	->select(
     //                        'courses.id as  course_id',
     //                        'courses.course_code as course_code',
     //                        'courses.course_name as course_name'
     //                   ); 
        $courses = Course::where('year_id',$request->year_id)->where('quarter_id',$request->quarter_id)->get();

        return response()->json([
        		'data'=>$courses,
        		'error'=>false
        	]);


    }

    public function getCoursesByExamEdit(Request $request)
    {
        $query = new ExamResult();
        $query = $query->leftJoin('courses','courses.id', '=', 'exam_results.course_id')
                        ->select(
                            'courses.id as  course_id',
                            'courses.course_code as course_code',
                            'exam_results.grade as grade'
                       ); 
        $courses = $query->where('exam_results.id',$request->id)->get();


        return response()->json([
                'data'=>$courses,
                'error'=>false
            ]);


    }

    

    public function getExamResult(Request $request)
    {
        $query = new ExamResult();
        $query = $query->leftJoin('courses','courses.id', '=', 'exam_results.course_id')
                        ->select(
                            'courses.id as  course_id',
                            'courses.course_code as course_code',
                            'exam_results.grade as grade'
                       ); 
        $results = $query->where('exam_results.id',$request->id)->get();;

        return response()->json([
                'data'=>$results,
                'error'=>false
            ]);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExamResultRequest $request)
    {   
        $batch_id = $request->batch_id;
        $year_id = $request->year_id; 
        $quarter_id = $request->quarter_id;
        $student_id = $request->user_id;

        $course_id_arr = $request->course_id;
        $course_id = implode (",", $course_id_arr);

        $grade_arr = $request->grade;
        $grade = implode (",", $grade_arr);
        
        


        foreach ($course_id_arr as $key => $val) {
            $data = [
                'batch_id'      =>$batch_id,
                'year_id'       =>$year_id,
                'user_id'       =>$student_id,
                'quarter_id'    =>$quarter_id,
                'course_id'     =>$val,
                'grade'         =>$grade_arr[$key],
                'login_id'       =>Sentinel::getUser()->id
            ];

            // array_push($data, $arr);
            // dd($data);
            $exam_result = new ExamResult($data);
            // dd($exam_result);
            $res = $exam_result->save();
        }

        if ($res) {
            return redirect('admin/exam-result')->with('success', 'Exam  Result create successfully.');
        } else {
            return Redirect::route('admin/exam-result')->withInput()->with('error', 'Error in creating result.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $batchs = AcadamicYear::all();
        $years = Year::all();
    	$quarters = Quarter::all();		
    	$students = User::where('id','!=',1)->get();
    
        $exam_result = ExamResult::with('student')->find($id);
        return view('admin.exam_result.edit', compact('exam_result','quarters','students','years','batchs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Quarter $class
     * @return Response
     */

    public function update(ExamResultRequest $request,$id)
    {   

        $course_id = $request->course_id;
        $grade = $request->grade;


        $examresult=ExamResult::find($id);
        $examresult->batch_id = $request->get('batch_id');
        $examresult->year_id = $request->get('year_id');
        $examresult->user_id = $request->get('user_id');
        $examresult->quarter_id = $request->get('quarter_id');
        $examresult->course_id = $course_id[0];
        $examresult->grade = $grade[0];
        $examresult->login_id =  Sentinel::getUser()->id;

        $result = $examresult->save();

        if ($result) {
            return redirect('admin/exam-result')->with('success', "Update Success");
        } else {
             return Redirect::route('admin/exam-result')->withInput()->with('error', "Update Error");
        }
    }



    /**
     * Remove Quarter.
     *
     * @param Quarter $quarter
     * @return Response
     */
    public function getModalDelete(Request $request,$id)
    {   
        $model = 'exam-result';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.exam-result.delete', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('exam-result/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {   
        $examresult = ExamResult::find($id);
        if ($examresult->delete()) {
            return redirect('admin/exam-result')->with('success', "Delete successfully.");
        } else {
            return Redirect::route('admin.exam-result.index')->withInput()->with('error', "Error in delete.");
        }
    }
}
