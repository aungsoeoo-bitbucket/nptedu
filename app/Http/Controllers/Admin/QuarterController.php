<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quarter;
use App\Year;
use App\Http\Requests\QuarterRequest;
use App\Traits\ActivityTraits;
use Sentinel;

class QuarterController extends Controller
{
      use ActivityTraits;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quarters = Quarter::with('year')->get();
        // Show the page
        return view('admin.quarter.index', compact('quarters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $years = Year::all();
        return view('admin.quarter.create',compact('years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuarterRequest $request)
    {   
        $quarter = new Quarter($request->all());

        if ($quarter->save()) {

            $settingParms = $quarter->toArray();
            $changes = Sentinel::getUser()->username . ' added new quarter name as  '.'"'.$quarter->quarter_name.'"';
            $this->logCreatedActivity($quarter,$changes,$settingParms);

            return redirect('admin/quarter')->with('success', trans('quarter/message.success.create'));
        } else {
            return Redirect::route('admin/quarter')->withInput()->with('error', trans('quarter/message.error.create'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $quarter,$id)
    {   
        $years = Year::all();
        $quarter = Quarter::find($id);
        return view('admin.quarter.edit', compact('quarter','years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Quarter $class
     * @return Response
     */

    public function update(QuarterRequest $request, Quarter $quarter)
    {   
        $beforeUpdateValues = $quarter->toArray();
        if ($quarter->update($request->all())) {

            $afterUpdateValues = $quarter->getChanges();
            $this->logUpdatedActivity($quarter,$beforeUpdateValues,$afterUpdateValues);

            return redirect('admin/quarter')->with('success', trans('quarter/message.success.update'));
        } else {
             return Redirect::route('admin/quarter')->withInput()->with('error', trans('quarter/message.error.update'));
        }
    }



    /**
     * Remove Quarter.
     *
     * @param Quarter $quarter
     * @return Response
     */
    public function getModalDelete(Quarter $quarter)
    {   

        $model = 'quarter';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('admin.quarter.delete', ['id' => $quarter->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('quarter/message.error.destroy', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quarter $quarter)
    {
        if ($quarter->delete()) {
            $changeLog = Sentinel::getUser()->username . ' delete '.'"'.$quarter->quarter_name.'"'.' from Quarter';
            $this->logDeletedActivity($quarter,$changeLog);

            return redirect('admin/quarter')->with('success', trans('quarter/message.success.delete'));
        } else {
            return Redirect::route('admin/quarter')->withInput()->with('error', trans('quarter/message.error.delete'));
        }
    }
}
