<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourseManagement;
use Response;
use File;
use App\FileDownload;
use App\FreeDownload;
use Sentinel;
use Storage;

class FilesController extends Controller
{
    public function downfun($id)
    {	

        $user = Sentinel::getUser();
        
        $freedownload= FreeDownload::where('id',$id)->get()->first();

        $file= public_path(). "/uploads/freedownload/$freedownload->file";

        $headers = array(
                  'Content-Type: .doc,.docx,.ppt,.pptx,.pdf, application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, audio/mp3, video/mp4',
                );

        if ($freedownload->file=="")
        {
            // echo "not exists";
            return redirect()->back()->withErrors(['File not exist!']);
        }
        else{

            if($user!=''){
                $save_count  = new FileDownload([
                    'c_id'    => $id,
                    'user_id' => $user->id
                ]);

                $res = $save_count->save();
            }
            

           

           return Response::download($file,$freedownload->file, $headers);
        }
    }
    public function viewfile($doc)
    {   

        $course= CourseManagement::where('document',$doc)->get();

        $filename = $course[0]->document;

        $path = public_path(). "/uploads/files/".$filename ;

        if ($course[0]->document=="")
        {
            // echo "not exists";
            return redirect()->back()->withErrors(['File not exist!']);
        }
        else{

              return Response::make(file_get_contents($path), 200, [
                'Content-Type: .doc,.docx,.ppt,.pptx,.pdf, application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]);
        }
    }

    public function getAudio($audio)
    {
        $data= CourseManagement::where('audio',$audio)->get();
        $audio = $data[0]->audio;


        $user = Sentinel::getUser();
        
        $file= public_path(). "/uploads/files/$audio";
        $headers = array(
                  'Content-Type: audio/mp3',
                );
        if ($audio=="")
        {
            // echo "not exists";
            return redirect()->back()->withErrors(['File not exist!']);
        }
        else{
            $save_count  = new FileDownload([
                'c_id'    => $data[0]->id,
                'user_id' => $user->id
            ]);

            $res = $save_count->save();

           return Response::download($file,$audio, $headers);
        }


    }
    public function getVideo($video)
    {
        $data= CourseManagement::where('video',$video)->get();

        $video = $data[0]->video;


        $user = Sentinel::getUser();
        
        $file= public_path(). "/uploads/files/$video";
        $headers = array(
                  'Content-Type: video/mp4',
                );
        if ($video=="")
        {
            // echo "not exists";
            return redirect()->back()->withErrors(['File not exist!']);
        }
        else{
            $save_count  = new FileDownload([
                'c_id'    => $data[0]->id,
                'user_id' => $user->id
            ]);

            $res = $save_count->save();

           return Response::download($file,$video, $headers);
        }
    }


}
