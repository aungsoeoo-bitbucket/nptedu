<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GroupRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'group_name' => 'required|unique:groups,group_name'
            ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'group_name' => 'required|unique:groups,group_name'
                ];
            }
            default:
                break;
        }

    }

}
