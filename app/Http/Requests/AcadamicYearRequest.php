<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class AcadamicYearRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'acadamic_year' => 'required|unique:acadamic_years,acadamic_year',

                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'acadamic_year' => 'required' 
                ];
            }
            default:
                break;
        }


    }



}
