<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class CourseManagementRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'quarter_id' => 'required',
                    'course_id' => 'required',
                    'title' => 'required',
                    'description' => 'required',
                    'publish_date' => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'quarter_id' => 'required',
                    'course_id' => 'required',
                    'title' => 'required',
                    'description' => 'required',
                    'publish_date' => 'required',
                ];
            }
            default:
                break;
        }


    }



}
