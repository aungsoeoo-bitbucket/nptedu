<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class CategoryRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'class_id' => 'required',
                    'quarter_id' => 'required',
                    'category_name' => 'required',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                $category_id = $this->route()->class;
                return [
                    'class_id' => 'required',
                    'quarter_id' => 'required',
                    'category_name' => 'required|min:3,' . $category_id
                ];
            }
            default:
                break;
        }


    }



}
