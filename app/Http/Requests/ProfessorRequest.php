<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;


class ProfessorRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return [
                    'name' => 'required',
                    'gender'=>'required',
                    'position' => 'required',
                    // 'phone' => 'required',
                    // 'email' => 'unique:professors,email',
                ];
            }
            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required',
                    'gender'=>'required',
                    'position' => 'required',
                    // 'phone' => 'required',
                    // 'email' => 'email|unique:professors,email',
                ];
            }
            default:
                break;
        }


    }

}
