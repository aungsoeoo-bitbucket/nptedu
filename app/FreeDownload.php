<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeDownload extends Model
{
    protected $table = 'freedownload';

	protected $fillable = ['title_en','title_mm','description_en','description_mm','file','youtube_url','active','login_id'];
	protected $guarded = ['id'];


	public function user()
	{
		return $this->hasOne('App\User','id','login_id');
	}
}
