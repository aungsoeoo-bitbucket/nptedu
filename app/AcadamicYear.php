<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcadamicYear extends Model
{
    protected $table = 'acadamic_years';
    protected $guarded  = ['id'];
    protected $fillable  = ['acadamic_year'];
    protected $searchableColumns = ['acadamic_year'];

    public function courses()
    {
    	 return $this->hasMany('App\Course','academic_year_id');
    }
}
