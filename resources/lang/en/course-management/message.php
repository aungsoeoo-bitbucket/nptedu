<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'course_exists'              => 'Course already exists!',
    'course_not_found'           => 'Course [:id] does not exist.',

    'success' => array(
        'create'    => 'Course was successfully created.',
        'update'    => 'Course was successfully updated.',
        'delete'    => 'Course was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the course. Please try again.',
        'update'    => 'There was an issue updating the course. Please try again.',
        'delete'    => 'There was an issue deleting the course. Please try again.',
    ),

);
