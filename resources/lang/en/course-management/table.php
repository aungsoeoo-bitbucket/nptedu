<?php
/**
* Language file for blog category table headings
*
*/

return array(

    'title'         => 'Title',
    'description'       => 'Description',
    'created_at' => 'Entry Time',
    'actions'	 => 'Actions',
    'view-course-management' => 'View Course Management',
    'update-course-management' => 'update Course Management',
    'delete-course-management' => 'delete Course Management'

);
