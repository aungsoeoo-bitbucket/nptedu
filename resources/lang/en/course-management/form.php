<?php
/**
* Language file for class management form text
*
*/
return array(

    'title'	=> 'Title',
    'description'	=> 'Description',
    
    'document'	=> 'Document',
    'audio'	=> 'Audio',
    'video'	=> 'Video',
    'publish_date' => "Publish Date",

    'update-course' => 'update Course',
    'courseexists' => 'Course already exists',
    'deletecourse' => 'Delete Course',

);
