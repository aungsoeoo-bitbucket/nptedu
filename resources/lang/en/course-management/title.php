<?php
/**
* Language file for blog section titles
*
*/

return array(

	'title'			=> 'Course Management',
	'create' => 'Create New Course',
	'edit' => 'Edit Course',
	'management' => 'Manage Course',
	'add-Course' => 'Add New Course',
	'course' => 'Course',
	'courses' => 'Course',
	'course-managementlist' => 'Course Management List',
	'course-managementdetail' => 'Course Management Details',


);
