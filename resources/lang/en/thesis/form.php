<?php
/**
 * Language file for form fields for user account management
 *
 */

return array(

    'title' => 'title here...',
    'description' => 'Place some text here',
    'title_label' => 'Title',
    'desc_label' => 'Description',
    'select-file' => 'Select file',
    'change' => 'Change',
    'cancel' => 'Cancel',
    'update' => 'Update',
    'publish' => 'Publish',
    'discard' => 'Discard',
    'name' => 'Status Name'

);
