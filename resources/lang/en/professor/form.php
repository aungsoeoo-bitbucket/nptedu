<?php
/**
* Language file for class management form text
*
*/
return array(
	'name'                      => 'Professor Name',
    'gender'                    => 'Gender',
    'position'                  => 'Position',
    'bio'                       => 'Bio',
    'phone'                     => 'Phone',
    'email'                     => 'Email',
    'facebook'                  => 'Facebook',
    'twitter'                   => 'Twitter',
    'linkedin'                  => 'Linkedin',
    'phone-placeholder'         => '09 1234 56789',
    'email-placeholder'         => 'example.gmail.com',
    'facebook-placeholder'      => 'https://www.facebook.com/yourfacebokID',
    'twitter-placeholder'       => 'https://twitter.com/yourtwitterID',
    'linkedin-placeholder'      => 'https://www.linkedin.com/in/yourlinkeinID/',
    'photo'		                => 'Photo',
    'active'                    => 'Active',
    'update-professor'          => 'update professor',
    'professorexists'           => 'Professor already exists',
    'deleteprofessor'           => 'Delete professor',

);
