<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'professor already exists!',
    'professor_not_found'           => 'professor [:id] does not exist.',

    'success' => array(
        'create'    => 'Professor was successfully created.',
        'update'    => 'Professor was successfully updated.',
        'delete'    => 'Professor was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the professor. Please try again.',
        'update'    => 'There was an issue updating the professor. Please try again.',
        'delete'    => 'There was an issue deleting the professor. Please try again.',
    ),

);
