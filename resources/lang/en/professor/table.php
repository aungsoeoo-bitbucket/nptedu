<?php
/**
* Language file for blog category table headings
*
*/

return array(
    'name'       => 'Professor Name',
    'position'   => 'Position',
    'bio'        => 'Bio',
    'email'   => 'Email',
    'facebook'   => 'Facebook',
    'twitter'    => 'Twitter',
    'linkedin'   => 'Linkedin',
    'photo'		 => 'Photo',
    'created_at' => 'Created at',
    'actions'	 => 'Actions',
    'view-professor' => 'View Professor',
    'update-professor' => 'update Professor',
    'delete-professor' => 'delete Professor'

);
