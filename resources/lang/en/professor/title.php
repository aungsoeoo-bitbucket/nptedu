<?php
/**
* Language file for blog section titles
*
*/

return array(

	'title'			=> 'Professor',
	'create' => 'Create New Professor',
	'edit' => 'Edit Professor',
	'management' => 'Manage Professor',
	'add-professor' => 'Add New Professor',
	'professor' => 'Professor',
	'categories' => 'Professors',
	'professorlist' => 'Professor List',
	'professordetail' => 'Professor Details',


);
