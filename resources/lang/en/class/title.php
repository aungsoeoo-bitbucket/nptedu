<?php
/**
* Language file for blog section titles
*
*/

return array(

	'title'			=> 'Class',
	'create' => 'Create New Class',
	'edit' => 'Edit Class',
	'management' => 'Manage Class',
	'add-Class' => 'Add New Class',
	'class' => 'Class',
	'classs' => 'Classs',
	'classlist' => 'Class List',
	'classdetail' => 'Class Details',


);
