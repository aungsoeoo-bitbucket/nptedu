<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'class_exists'              => 'Class already exists!',
    'class_not_found'           => 'Class [:id] does not exist.',
    'class_has_course'           => 'Class  have courses!',

    'success' => array(
        'create'    => 'Class was successfully created.',
        'update'    => 'Class was successfully updated.',
        'delete'    => 'Class was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the class. Please try again.',
        'update'    => 'There was an issue updating the class. Please try again.',
        'delete'    => 'There was an issue deleting the class. Please try again.',
    ),

);
