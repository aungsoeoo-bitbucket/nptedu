<?php
/**
* Language file for class management form text
*
*/
return array(

    'name'			=> 'Class Name',
    'update-class' => 'update class',
    'classexists' => 'Class already exists',
    'deleteclass' => 'Delete class',

);
