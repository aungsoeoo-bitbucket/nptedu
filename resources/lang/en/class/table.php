<?php
/**
* Language file for blog category table headings
*
*/

return array(

    'id'         => 'Id',
    'title'       => 'Class Name',
    'created_at' => 'Created at',
    'actions'	 => 'Actions',
    'view-class' => 'View class',
    'update-class' => 'update class',
    'delete-class' => 'delete class'

);
