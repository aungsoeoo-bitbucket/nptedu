<?php
/**
* Language file for user section titles
*
*/

return array(

	'user_profile'			=> 'User Profile',
	'user_name'			    => 'Name',
	'email'					=> 'E-mail',
	'phone'					=> 'Phone Number',
	'address'				=> 'Address',
	'class_of_year'			=> 'Class of year',
	'status'				=> 'Status',
	'created_at'			=> 'Created At',
    'select_image'			=> 'Select Image',
    'gender'				=> 'Gender / Sex',
    'dob'					=> 'Birth Date',
    'country'				=> 'Country',
    'state'					=> 'State',
    'facebook'				=> 'Facebook',
    'subject'				=> 'Subject',
    'roll_no'				=> 'Roll Number',
    'group'					=> 'Group',
    'designation'			=> 'Designation',
    'department'			=> 'Department',

);
