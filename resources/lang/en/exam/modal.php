<?php

/**
* Language file for blog delete modal
*
*/
return array(

    'body'			=> 'Are you sure to delete this exam? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',
    'title'         => 'Delete File',

);
