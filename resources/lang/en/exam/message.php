<?php
/**
* Language file for File error/success messages
*
*/

return array(

    'exam_exists'              => 'Exam already exists!',
    'exam_not_found'           => 'Exam [:id] does not exist.',

    'success' => array(
        'create'    => 'Exam was successfully created.',
        'update'    => 'Exam was successfully updated.',
        'delete'    => 'Exam was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the exam. Please try again.',
        'update'    => 'There was an issue updating the exam. Please try again.',
        'delete'    => 'There was an issue deleting the exam. Please try again.',
    ),

);
