<?php
/**
* Language file for file section titles
*
*/

return array(

	'title'			=> 'Free Download Files',
	'create' => 'Add New File',
	'edit' => 'Edit File',
	'management' => 'Manage files',
	'add-file' => 'Add New file',
	'file' => 'File',
	'files' => 'Files',
	'filelist' => 'File List',
	'filedetail' => 'File Details',
	'update-file' => 'Update File',


);
