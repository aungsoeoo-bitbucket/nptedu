<?php
/**
* Language file for file section titles
*
*/

return array(

	'title'			=> 'About',
	'create' => 'Add New ',
	'edit' => 'Edit About Page',
	'management' => 'Manage about page',
	'add-about' => 'Add About Page',
	'about' => 'About',
	'abouts' => 'Abouts',
	'aboutlist' => 'About List',
	'aboutdetail' => 'About Details',
	'update-about' => 'Update about',


);
