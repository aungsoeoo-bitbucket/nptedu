<?php
/**
* Language file for File error/success messages
*
*/

return array(

    'file_exists'              => 'File already exists!',
    'file_not_found'           => 'File [:id] does not exist.',

    'success' => array(
        'create'    => 'File was successfully created.',
        'update'    => 'File was successfully updated.',
        'delete'    => 'File was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the File. Please try again.',
        'update'    => 'There was an issue updating the File. Please try again.',
        'delete'    => 'There was an issue deleting the File. Please try again.',
    ),

);
