<?php
/**
* Language file for Year error/success messages
*
*/

return array(

    'year_exists'        => 'Academic Batch already exists!',
    'year_not_found'     => 'Academic Batch [:id] does not exist.',
    'year_name_required' => 'The Academic Batch field is required',
    'year_exists'        => 'Academic Batch contains data, Batch can not be deleted',

    'success' => array(
        'create' => 'Academic Batch was successfully created.',
        'update' => 'Academic Batch was successfully updated.',
        'delete' => 'Academic Batch was successfully deleted.',
    ),

    'delete' => array(
        'create' => 'There was an issue creating the Academic Batch. Please try again.',
        'update' => 'There was an issue updating the Academic Batch. Please try again.',
        'delete' => 'There was an issue deleting the Academic Batch. Please try again.',
    ),

    'error' => array(
        'Year_exists' => 'A Academic Batch already exists with that name, names must be unique for Years.',
        'Year_role_exists' => 'Another role with same slug exists, please choose another name',
        'delete' => 'Error in Delete.',

    ),

);
