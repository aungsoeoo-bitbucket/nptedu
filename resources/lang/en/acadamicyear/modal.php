<?php

/**
* Language file for group delete modal
*
*/
return array(

    'title'         => 'Delete Academic Batch',
    'body'			=> 'Are you sure to delete this Academic year? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',

);
