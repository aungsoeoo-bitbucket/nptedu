<?php
/**
* Language file for group section titles
*
*/

return array(
	'name' =>'Academic Batch',
    'create'			=> 'Create Academic  Batch',
    'edit' 				=> 'Edit Academic Batch',
    'management'	=> 'Manage  Academic Batch',
    'years' => ' Academic Batch',
    'years_list' => 'Academic Batch List',

);
