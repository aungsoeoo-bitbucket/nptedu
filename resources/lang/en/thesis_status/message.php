<?php
/**
* Language file for File error/success messages
*
*/

return array(

    'thesis_exists'              => 'Thesis already exists!',
    'thesis_not_found'           => 'Thesis [:id] does not exist.',

    'success' => array(
        'create'    => 'Data was successfully created.',
        'update'    => 'Data was successfully updated.',
        'delete'    => 'Data was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the exam. Please try again.',
        'update'    => 'There was an issue updating the exam. Please try again.',
        'delete'    => 'There was an issue deleting the exam. Please try again.',
    ),

);
