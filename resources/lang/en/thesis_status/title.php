<?php
/**
* Language file for file section titles
*
*/

return array(

	'title'			=> 'Free Download Files',
	'create' => 'Add New File',
	'edit' => 'Edit File',
	'management' => 'Manage files',
	'add-file' => 'Add New file',
	'file' => 'File',
	'files' => 'Files',
	'filelist' => 'File List',
	'filedetail' => 'File Details',
	'update-file' => 'Update File',
	'statuslist' => 'Thesis Status List',
	'status' => 'Status',
	'create_status' => 'Create New Status',
	'edit_status' => 'Edit Status',
	'viva-list' => 'Batch Viva List',
	'std-name' => 'Student Name',
	'pass-date' => 'Pass Date',
	'complete-date' => 'Complete Date',
	'viva'=>'Batch Viva'
);
