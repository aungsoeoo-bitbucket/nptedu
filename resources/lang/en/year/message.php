<?php
/**
* Language file for Year error/success messages
*
*/

return array(

    'year_exists'        => 'Class of Year already exists!',
    'year_not_found'     => 'Class of Year [:id] does not exist.',
    'year_name_required' => 'Class of  year field is required',
    'data_exists'        => 'Class of Year contains data, Year can not be deleted',

    'success' => array(
        'create' => 'Class of Year was successfully created.',
        'update' => 'Class of Year was successfully updated.',
        'delete' => 'Class of Year was successfully deleted.',
    ),

    'delete' => array(
        'create' => 'There was an issue creating the Class of Year. Please try again.',
        'update' => 'There was an issue updating the Class of Year. Please try again.',
        'delete' => 'There was an issue deleting the Class of Year. Please try again.',
    ),

    'error' => array(
        'Year_exists' => 'Class of Year already exists with that name, names must be unique for Years.',
        'Year_role_exists' => 'Another role with same slug exists, please choose another name',
        'no_role_exists' => 'No Role exists with that id.',

    ),

);
