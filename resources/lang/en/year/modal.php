<?php

/**
* Language file for group delete modal
*
*/
return array(

    'title'         => 'Delete Class of Year',
    'body'			=> 'Are you sure to delete this Class of year? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',

);
