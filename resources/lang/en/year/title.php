<?php
/**
* Language file for group section titles
*
*/

return array(
	'name' =>'Class of Year',
    'create'			=> 'Create Class of Year',
    'edit' 				=> 'Edit Class of Year',
    'management'	=> 'Manage  Class of Year',
    'years' => 'Class of Year',
    'years_list' => 'Class of Year List',

);
