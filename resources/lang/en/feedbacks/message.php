<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Feedback already exists!',
    'category_not_found'           => 'Feedback [:id] does not exist.',

    'success' => array(
        'create'    => 'Feedback was successfully created.',
        'update'    => 'Feedback was successfully updated.',
        'delete'    => 'Feedback was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Feedback. Please try again.',
        'update'    => 'There was an issue updating the Feedback. Please try again.',
        'delete'    => 'There was an issue deleting the Feedback. Please try again.',
    ),

);
