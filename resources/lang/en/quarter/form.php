<?php
/**
* Language file for class management form text
*
*/
return array(

    'name'			=> 'Quarter Name',
    'update-quarter' => 'update Quarter',
    'quarterexists' => 'Quarter already exists',
    'deletequarter' => 'Delete Quarter',

);
