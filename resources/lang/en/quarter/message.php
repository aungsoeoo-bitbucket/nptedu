<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'quarter_exists'              => 'Quarter already exists!',
    'quarter_not_found'           => 'Quarter [:id] does not exist.',
    'data_exists'                 => 'Quarter contains data, Quarter can not be deleted',

    'success' => array(
        'create'    => 'Quarter was successfully created.',
        'update'    => 'Quarter was successfully updated.',
        'delete'    => 'Quarter was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the quarter. Please try again.',
        'update'    => 'There was an issue updating the quarter. Please try again.',
        'delete'    => 'There was an issue deleting the quarter. Please try again.',
    ),

);
