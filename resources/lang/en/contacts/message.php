<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Contact already exists!',
    'category_not_found'           => 'Contact [:id] does not exist.',

    'success' => array(
        'create'    => 'Contact was successfully created.',
        'update'    => 'Contact was successfully updated.',
        'delete'    => 'Contact was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Contact. Please try again.',
        'update'    => 'There was an issue updating the Contact. Please try again.',
        'delete'    => 'There was an issue deleting the Contact. Please try again.',
    ),

);
