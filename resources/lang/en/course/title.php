<?php
/**
* Language file for blog section titles
*
*/

return array(

	'title'			=> 'Course',
	'create' => 'Create New Course',
	'edit' => 'Edit Course',
	'management' => 'Manage Course',
	'add-course' => 'Add New Course',
	'course' => 'Course',
	'courses' => 'Courses',
	'courselist' => 'Course List',
	'coursedetail' => 'Course Details',


);
