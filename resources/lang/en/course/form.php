<?php
/**
* Language file for class management form text
*
*/
return array(
	'photo'		=> "Photo",
    'name'			=> 'Course Name',
    'update-course' => 'update Course',
    'courseexists' => 'Course already exists',
    'deletecourse' => 'Delete Course',

);
