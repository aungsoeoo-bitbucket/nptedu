<?php
/**
* Language file for blog category table headings
*
*/

return array(

    'id'         => 'Id',
    'title'       => 'Course Name',
    'created_at' => 'Created at',
    'actions'	 => 'Actions',
    'view-category' => 'View Category',
    'update-category' => 'update Category',
    'delete-category' => 'delete Category'

);
