<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'category_exists'              => 'Course already exists!',
    'category_not_found'           => 'Course [:id] does not exist.',
    'course_data_exists'           => 'Course  data  exist. Cannot Delete!',

    'success' => array(
        'create'    => 'Course was successfully created.',
        'update'    => 'Course was successfully updated.',
        'delete'    => 'Course was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Course. Please try again.',
        'update'    => 'There was an issue updating the Course. Please try again.',
        'delete'    => 'There was an issue deleting the Course. Please try again.',
    ),

);
