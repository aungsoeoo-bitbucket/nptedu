<?php
/**
* Language file for blog category table headings
*
*/

return array(

    'title'       => 'Title',
    'description'      => 'Description',
    'created_at' => 'Created at',
    'status'     => 'Status',
    'actions'	 => 'Actions',
    'update-file' => 'update file',
    'delete-file' => 'delete file',
    'file' => 'File',

);
