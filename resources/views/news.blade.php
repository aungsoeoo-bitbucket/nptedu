@extends('layouts/default')

{{-- Page title --}}
@section('title')
News
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/news.css') }}">
<link href="{{ asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/timeline.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" />
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
<hr>
   {{--  <div class="breadcum">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="hidden-xs">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">News</a>
                </li>
            </ol>
            <div class="pull-right">
                <i class="livicon icon3" data-name="responsive-menu" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> News
            </div>
        </div>
    </div> --}}
    @stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <div class="row news">
            <div class="col-md-12">
                <!-- News1 Section Start -->
                @forelse ($blogs as $blog)
                <div class="blog thumbnail ">
                    <label>
                         <a href="{{ URL::to('news-item/'.$blog->slug) }}"><h3 class="primary news_headings">{{$blog->title}}</h3></a>
                    </label>
                    @if($blog->image)
                         <img src="{{ URL::to('/uploads/blog/'.$blog->image)  }}" alt="image" class="img-responsive">
                    @endif
                    <div class="news_item_text_1">
                        <p>
                            {!!  $blog->content !!}
                        </p>
                        <p class="text-right">
                            <a href="{{ URL::to('news-item/'.$blog->slug) }}" class="btn btn-primary text-white">
                                Read more
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                    <h3>No Posts Exists!</h3>
                @endforelse
                <ul class="pager">
                    {!! $blogs->render() !!}
                </ul>
                <!-- //News1 Section End -->
               
                <!-- //News3 Section End -->
            </div>
           
            <!-- Tab-content End -->
        </div>
        <!-- //Tabbablw-line End -->
    </div>
    <!-- Tabbable_panel End -->
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <!--tags-->
    <script src="{{ asset('assets/vendors/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('assets/vendors/wow/js/wow.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            new WOW().init();
        });
    </script>
    <!-- end of page level js -->

@stop
