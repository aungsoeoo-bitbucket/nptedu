@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Activity log
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css"/>
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Activity log</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>

            <li class="active">Activity log</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"><i class="livicon" data-name="user" data-size="16" data-loop="true"
                                               data-c="#fff" data-hc="white"></i>
                        Activity Log
                    </h4>
                </div>
                <br/>
                <?php 
                    $keyword = isset($_GET['keyword'])?$_GET['keyword']:'';
                ?>
                <form action="{{ url('/admin/activity-log') }}" method="GET">
                    <div class="row">
                        <div class="col-md-3"  style="float: right;">
                            <input type="text" class="form-control" name="keyword" placeholder="Search .." value="{{ $keyword }}">
                        </div>
                    </div>
                </form>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-bordered " id="table">
                            <thead>
                            <tr class="filters">
                                <th>Name</th>
                                <th>Description</th>
                                <th>created time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($activities as $activity)
                                <tr>
                                    <td>{!! $activity->log_name !!}</td>
                                    <td>{!! $activity->description !!}</td>
                                    <td>{!! $activity->created_at !!}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {!! $activities->render() !!}
                    </div>
                </div>
            </div>
        </div>    <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#table').DataTable({
                "order": [[3, "desc"]]
            });
        });
    </script>
@stop
