@if ($message = Session::get('success'))
	<div class="alert alert-success alert-dismissable margin5">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <strong>{{ $message }}</strong>
	</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-dismissable margin5">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

@if($message = Session::get('warnning'))
<div class="alert alert-warning alert-dismissable margin5">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif