<ul id="menu" class="page-sidebar-menu">
    <li {!! (Request::is('admin') ? 'class="active"' : '') !!}>
        <a href="{{ route('admin.dashboard') }}">
            <i class="livicon" data-name="dashboard" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>
    {{-- class menu --}}
    <li {!! (Request::is('admin/classes') ? 'class="active"' : '') !!}>
        <a href="{{ route('admin.classes.index') }}">
            <i class="livicon" data-name="doc-portrait" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">Class</span>
        </a>
    </li>

    {{-- course menu --}}
      <li {!! (Request::is('admin/quarter') || Request::is('admin/category') || Request::is('admin/course') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="table" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">Courses</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/acadamic_years') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/acadamic_years') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Academic Batch
                </a>
            </li>

            <li {!! (Request::is('admin/years') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/years') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Class of Year
                </a>
            </li>

            <li {!! (Request::is('admin/quarter') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/quarter') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Quarter
                </a>
            </li>
            <li {!! (Request::is('admin/courses') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/courses') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Courses
                </a>
            </li>
            <li {!! (Request::is('admin/course-management') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/course-management') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Course Management
                </a>
            </li>
        </ul>
{{--     <li {!! (Request::is('admin/exam') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/exam') }}">
            <i class="livicon" data-name="table" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C"
               data-loop="true"></i>
            Exam
        </a>
    </li> --}}
    <li {!! (Request::is('admin/exam-result') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/exam-result') }}">
            <i class="livicon" data-name="table" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C"
               data-loop="true"></i>
            Exam Result
        </a>
    </li>

    <li {!! (Request::is('admin/freedownload') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/freedownload') }}">
            <i class="livicon" data-name="download" data-size="18" data-c="#F89A14" data-hc="#F89A14"
               data-loop="true"></i>
            Free Download
        </a>
    </li>

    <li {!! (Request::is('admin/professor') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/professor') }}">
            <i class="livicon" data-name="user" data-size="18" data-c="#F89A14" data-hc="#F89A14"
               data-loop="true"></i>
            Professor
        </a>
    </li>

    <li {!! ((Request::is('admin/blogcategory') || Request::is('admin/blogcategory/create') || Request::is('admin/blog') ||  Request::is('admin/blog/create')) || Request::is('admin/blog/*') || Request::is('admin/blogcategory/*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="move" data-c="#F89A14" data-hc="#F89A14" data-size="18"
               data-loop="true"></i>
            <span class="title">News/Event</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/blogcategory') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/blogcategory') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Category List
                </a>
            </li>
            <li {!! (Request::is('admin/blog') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/blog') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Posts
                </a>
            </li>
        </ul>
    </li>
   {{--  <li {!! (Request::is('admin/news') || Request::is('admin/news_item')  ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="move" data-c="#ef6f6c" data-hc="#ef6f6c" data-size="18"
               data-loop="true"></i>
            <span class="title">News</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/news') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/news') }}">
                    <i class="fa fa-angle-double-right"></i>
                    News
                </a>
            </li>
            <li {!! (Request::is('admin/news_item') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/news_item') }}">
                    <i class="fa fa-angle-double-right"></i>
                    News Details
                </a>
            </li>
        </ul>
    </li>
 --}}
     <li {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="users" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">Users</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/roles') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/roles') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Role
                </a>
            </li>
            <li {!! (Request::is('admin/groups') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/groups') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Groups
                </a>
            </li>
            <li {!! (Request::is('admin/users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Student
                </a>
            </li>
        </ul>
    </li>
    
     <li {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="table" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">Thesis</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/thesis') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/thesis') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Thesis Group
                </a>
            </li>
            <li {!! (Request::is('admin/thesis_status') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/thesis_status') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Thesis Status 
                </a>
            </li>
            <li {!! (Request::is('admin/batch_viva') ? 'class="active" id="active"' : '') !!}>
                <a href="{{  URL::to('admin/batch_viva') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Batch Viva 
                </a>
            </li>
        </ul>
    </li>
    
    <li {!! (Request::is('admin/contacts') ? 'class="active"' : '') !!}>
        <a href="{{  URL::to('admin/contacts') }}">
            <i class="livicon" data-name="mail" data-size="18" data-c="#F89A14" data-hc="#F89A14"
               data-loop="true"></i>
            Contact
        </a>
    </li>

    <li {!! (Request::is('admin/feedback') ? 'class="active"' : '') !!}>

        <a href="{{  URL::to('admin/feedback') }}">
            <i class="livicon" data-name="help" data-size="18" data-c="#1DA1F2" data-hc="#1DA1F2"
               data-loop="true"></i>
            Feedback
        </a>
    </li>

    <li {!! (Request::is('admin/activity-log') ? 'class="active"' : '') !!}>

        <a href="{{  URL::to('admin/activity-log') }}">
            <i class="livicon" data-name="eye-open" data-size="18" data-c="#1DA1F2" data-hc="#1DA1F2"
               data-loop="true"></i>
            Activity Log
        </a>
    </li>
   

    <!-- Menus generated by CRUD generator -->
    @include('admin/layouts/menu')
</ul>
