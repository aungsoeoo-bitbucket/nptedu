@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Edit User
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">

    <link type="text/css" media="screen"  rel="stylesheet"  href="{{ asset('assets/css/croppie.css')}}">
    <!--end of page level css-->

@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Edit user</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li>Users</li>
            <li class="active">Add New User</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> <i class="livicon" data-name="users" data-size="16" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            Editing user : <p class="user_name_max">{!! $user->first_name!!} {!! $user->last_name!!}</p>
                        </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                    </div>
                    <div class="panel-body">
                        <!--main content-->
                        <div class="row">

                            <div class="col-md-12">

                                {!! Form::model($user, ['url' => URL::to('admin/users/'. $user->id.''), 'method' => 'put', 'class' => 'form-horizontal','id'=>'commentForm', 'enctype'=>'multipart/form-data','files'=> true]) !!}
                                    {{ csrf_field() }}

                                     <div id="rootwizard">
                                        <ul>
                                            <li><a href="#tab1" data-toggle="tab">Security</a></li>
                                            <li><a href="#tab2" data-toggle="tab">Profile</a></li>
                                            <li><a href="#tab3" data-toggle="tab">Contact</a></li>
                                            <li><a href="#tab4" data-toggle="tab">Account Type</a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <div class="tab-pane" id="tab1">
                                                <h2 class="hidden">&nbsp;</h2>
                                                <div class="form-group {{ $errors->first('username', 'has-error') }}">
                                                    <label for="username" class="col-sm-2 control-label">Name (MM) *</label>
                                                    <div class="col-sm-10">
                                                        <input id="username" name="username" type="text"
                                                               placeholder="Eg: ေမာင္မင္းမင္း"  style="font-family: Zawgyi, 'Open Sans', Arial, sans-serif !important;" class="form-control required"
                                                               value="{!! old('username',$user->username) !!}"/>

                                                        {!! $errors->first('username', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->first('username_en', 'has-error') }}">
                                                    <label for="username_en" class="col-sm-2 control-label">Name (Eng) *</label>
                                                    <div class="col-sm-10">
                                                        <input id="username_en" name="username_en" type="text"
                                                               placeholder="Eg: Mg Min Min" class="form-control required"
                                                               value="{!! old('username_en',$user->username_en) !!}"/>

                                                        {!! $errors->first('username_en', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                                    <label for="email" class="col-sm-2 control-label">Email *</label>
                                                    <div class="col-sm-10">
                                                        <input id="email" name="email" placeholder="E-Mail" type="text"
                                                               class="form-control required email"
                                                               value="{!! old('email', $user->email) !!}"/>

                                                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->first('password', 'has-error') }}">
                                                    <p class="text-warning">If you don't want to change password... please leave them empty</p>
                                                    <label for="password" class="col-sm-2 control-label">Password </label>
                                                    <div class="col-sm-10">
                                                        <input id="password" name="password" type="password" placeholder="Password"
                                                               class="form-control" value="{!! old('password') !!}"/>
                                                    </div>
                                                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                                </div>

                                                <div class="form-group {{ $errors->first('password_confirm', 'has-error') }}">
                                                    <label for="password_confirm" class="col-sm-2 control-label">Confirm Password </label>
                                                    <div class="col-sm-10">
                                                        <input id="password_confirm" name="password_confirm" type="password"
                                                               placeholder="Confirm Password " class="form-control"
                                                               value="{!! old('password_confirm') !!}"/>
                                                        {!! $errors->first('password_confirm', '<span class="help-block">:message</span>') !!}
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab2" disabled="disabled">
                                                <h2 class="hidden">&nbsp;</h2> <div class="form-group  {{ $errors->first('dob', 'has-error') }}">
                                                    <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
                                                    <div class="col-sm-10">
                                                        <input id="dob" name="dob" type="text" class="form-control" value="{!! old('dob', $user->dob) !!}"
                                                               data-date-format="YYYY-MM-DD"
                                                               placeholder="yyyy-mm-dd"/>
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('dob', ':message') }}</span>
                                                </div>

                                                <div class="form-group {{ $errors->first('gender', 'has-error') }}">
                                                    <label for="email" class="col-sm-2 control-label">Gender *</label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control" title="Select Gender..." name="gender">
                                                            <option value="">Select</option>
                                                            <option value="male" @if($user->gender === 'male') selected="selected" @endif >Male</option>
                                                            <option value="female" @if($user->gender === 'female') selected="selected" @endif >Female</option>

                                                        </select>
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('gender', ':message') }}</span>
                                                </div>

                                                  <div class="form-group {{ $errors->first('acadamic_year', 'has-error') }}">
                                                    <label for="acadamic_year" class="col-sm-2 control-label">Acadamic Batch *</label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control" title="Select Batch..." name="acadamic_year_id">
                                                            <option value="">Select Acadamic Batch</option>
                                                            @foreach($acadamicyears as $acy)
                                                            <option value="{{ $acy->id }}"
                                                                    @if(old('acadamic_year',$user->acadamic_year_id) == $acy->id ) selected="selected" @endif >{{ $acy->acadamic_year}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('acadamic_year_id', ':message') }}</span>
                                                </div>

                                                <div class="form-group {{ $errors->first('class_of_year', 'has-error') }}">
                                                    <label for="class_of_year" class="col-sm-2 control-label">Class of year *</label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control class_of_year" title="Select Year..." name="class_of_year">
                                                            <option value="">Select Year</option>
                                                            @foreach($class_of_years as $cy)
                                                            <option value="{{$cy->id}}"
                                                                    @if(old('class_of_year',$user->class_of_year) == $cy->id) selected="selected" @endif >{{$cy->year}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('class_of_year', ':message') }}</span>
                                                </div>

                                                <div class="form-group {{ $errors->first('subject', 'has-error') }}">
                                                    <label for="subject" class="col-sm-2 control-label">Subject *</label>
                                                    <div class="col-sm-10">
                                                        MBA &nbsp;<input id="subject" name="subject" placeholder="" type="radio"
                                                               class="required subject" checked />
                                                        {!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->first('roll_no', 'has-error') }}">
                                                    <label for="roll_no" class="col-sm-2 control-label">1st Year Roll No *</label>
                                                    <div class="col-sm-10">
                                                        <input id="roll_no" name="roll_no" type="text" placeholder="MBA-your number"
                                                               class="form-control required" value="{!! old('roll_no',$user->roll_no) !!}"/>
                                                        {!! $errors->first('roll_no', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->first('roll_no_2', 'has-error') }}">
                                                    <label for="roll_no_2" class="col-sm-2 control-label"> 2nd Year Roll No *</label>
                                                    <div class="col-sm-10">
                                                        <input id="roll_no_2" name="roll_no_2" type="text" placeholder="MBA-your number"
                                                               class="form-control required roll_no_2" value="{!! old('roll_no_2',$user->roll_no_2) !!}"/>
                                                        {!! $errors->first('roll_no_2', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group required">
                                                    <label for="group" class="col-sm-2 control-label">Group *</label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control required" title="Select group..." name="group_id"
                                                                id="group">
                                                            <option value="">Select</option>
                                                            @foreach($groups as $group)
                                                                <option value="{{ $group->id }}"
                                                                        @if($group->id == old('group_name',$user->group_id)) selected="selected" @endif >{{ $group->group_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        {!! $errors->first('group', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('group', ':message') }}</span>
                                                </div>

                                                <div class="form-group {{ $errors->first('designation', 'has-error') }}">
                                                    <label for="designation" class="col-sm-2 control-label">Designation </label>
                                                    <div class="col-sm-10">
                                                        <input id="designation" name="designation" type="text"
                                                               placeholder="Designation " class="form-control" value="{{ old('designation',$user->designation)}}" />
                                                        {!! $errors->first('designation', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                  <div class="form-group {{ $errors->first('department', 'has-error') }}">
                                                    <label for="department" class="col-sm-2 control-label">Department </label>
                                                    <div class="col-sm-10">
                                                        <input id="department" name="department" type="text"
                                                               placeholder="Department" class="form-control" value="{{ old('department',$user->department)}}"/>
                                                        {!! $errors->first('department', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>

                                                <div class="form-group {{ $errors->first('ministry_company', 'has-error') }}">
                                                        <label for="ministry_company" class="col-sm-2 control-label">Ministry/Company </label>
                                                        <div class="col-sm-10">
                                                            <input id="ministry_company" name="ministry_company" type="text" value="{{ old('ministry_company', $user->ministry_company) }}" 
                                                                   placeholder="Ministry/Company" class="form-control"/>
                                                            {!! $errors->first('ministry_company', '<span class="help-block">:message</span>') !!}
                                                        </div>
                                                </div>




                                               <div class="form-group {{ $errors->first('pic', 'has-error') }}">
                                                    <label for="pic" class="col-sm-2 control-label">Profile picture</label>
                                                    <div class="col-sm-10">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            {{-- <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                                                               
                                                            </div> --}}

                                                            @if($user->pic)
                                                                <img src="{!! url('/').'/uploads/users/'.$user->pic !!}" id="image_preview" alt="image preview" style="max-width: 200px; max-height: 200px;">
                                                                    
                                                            @else
                                                                <img src="{{ asset('assets/images/authors/no_avatar.png') }}" id="image_preview" alt="image preview" style="max-width: 200px; max-height: 200px;">
                                                            @endif
                                                           
                                                            {{-- <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div> --}}
                                                            <div>
                                                                <span class="btn btn-default btn-file">
                                                                    <span class="fileinput-new">Select image</span>
                                                                    <span class="fileinput-exists">Change</span>
                                                                    <input id="pic"  type="file" class="form-control upload"/>
                                                                </span>
                                                                <a href="#" class="btn btn-danger fileinput-exists"
                                                                   id="remove_preview">Remove</a>
                                                            </div>
                                                        </div>
                                                         <input type="hidden" name="pic" value="" id="hidden_photo">

                                                        {!! $errors->first('pic', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab3" disabled="disabled">
                                                

                                                 <div class="form-group">
                                                    <label for="address" class="col-sm-2 control-label">Address *</label>
                                                    <div class="col-sm-10">
                                                        <input id="address" name="address" type="text" class="form-control"
                                                               value="{!! old('address',$user->address) !!}"/>
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('address', ':message') }}</span>
                                                </div>

                                                <div class="form-group">
                                                    <label for="phone" class="col-sm-2 control-label">Phone *</label>
                                                    <div class="col-sm-10">
                                                        <input id="phone" name="phone" type="text" class="form-control"
                                                               value="{!! old('phone',$user->phone) !!}"/>
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('phone', ':message') }}</span>
                                                </div>

                                                <div class="form-group">
                                                    <label for="facebook" class="col-sm-2 control-label">Facebook</label>
                                                    <div class="col-sm-10">
                                                        <input id="facebook" name="facebook" type="text" class="form-control"
                                                               value="{!! old('facebook',$user->facebook) !!}"/>
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('facebook', ':message') }}</span>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab4" disabled="disabled">
                                                <p class="text-danger"><strong>Be careful with group selection, if you give admin access.. they can access admin section</strong></p>
                                                <div class="form-group {{ $errors->first('group', 'has-error') }}">
                                                    <label for="group" class="col-sm-2 control-label">Group *</label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control " title="Select group..." name="groups[]" id="groups" required>
                                                            <option value="">Select</option>
                                                            @foreach($roles as $role)
                                                                <option value="{!! $role->id !!}" {{ (array_key_exists($role->id, $userRoles) ? ' selected="selected"' : '') }}>{{ $role->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div
                                                            {!! $errors->first('group', '<span class="help-block">:message</span>') !!}>
                                                </div>

                                                <div class="form-group">
                                                    <label for="activate" class="col-sm-2 control-label"> Activate User</label>
                                                    <div class="col-sm-10">
                                                        <input id="activate" name="activate" type="checkbox" class="pos-rel p-l-30 custom-checkbox" value="1" @if($status) checked="checked" @endif  >
                                                        <span>To activate your account click the check box</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="pager wizard">
                                                <li class="previous"><a href="#">Previous</a></li>
                                                <li class="next"><a href="#">Next</a></li>
                                                <li class="next finish" style="display:none;"><a href="javascript:;">Finish</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <!--main content end-->
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/edituser.js') }}"></script>

     <script type="text/javascript" src="{{ asset('assets/js/croppie.js')}}"></script>
        <!-- end page level js -->
     <!-- crop image modal -->
    @include('modal.image_crop')

    <script>
        $('document').ready(function(){
           var which_year= $(".class_of_year").val();
           if(which_year!=''){
                if (which_year=='1') {
                    $('#roll_no').attr('disabled',false);
                    $('#roll_no_2').attr('disabled',true);
                }else if(which_year=='2'){
                    $('#roll_no').attr('disabled',true);
                    $('#roll_no_2').attr('disabled',false);
                }else{
                    $('#roll_no').attr('disabled',false);
                    $('#roll_no_2').attr('disabled',false);
                }
           }
        });

        $('.class_of_year').change(function(){
            var which_year= $(".class_of_year").val();
            if(which_year!=''){
                if (which_year=='1') {
                    $('#roll_no').attr('disabled',false);
                    $('#roll_no_2').attr('disabled',true);
                }else if(which_year=='2'){
                    $('#roll_no').attr('disabled',true);
                    $('#roll_no_2').attr('disabled',false);
                }else{
                    $('#roll_no').attr('disabled',false);
                    $('#roll_no_2').attr('disabled',false);
                }
            }
        });
    </script>
    
@stop
