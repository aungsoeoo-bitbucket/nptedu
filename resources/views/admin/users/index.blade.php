@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Student List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Users</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Users</a></li>
        <li class="active">Student List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">

        <div class="panel panel-primary ">
             <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Student List
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/users/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
                <div class="row">
                        <div class="form-group col-md-3">
                            <h5>Acadamic Year</h5>
                            <div class="controls">
                                <select name="acadamic_year_id"  class="form-control" id="acadamic_year_id">
                                    <option value="">Select Acadamic Year</option>
                                    @foreach($acadamicyears as $acd)
                                        <option value="{{ $acd->id }}">{{ $acd->acadamic_year }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <h5>Class of Year</h5>
                            <div class="controls">
                                 <select name="class_of_year"  class="form-control" id="class_of_year">
                                    <option value="">Select Class Year</option>
                                    @foreach($years as $year)
                                        <option value="{{ $year->id }}">{{ $year->year }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                         <div class="form-group col-md-3">
                            <h5>Gender</h5>
                            <div class="controls">
                                <select name="gender"  class="form-control" id="gender">
                                    <option value="">Select Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-1" style="
                        margin-left: 15px;
                        ">
                            <h5>&nbsp;</h5>
                            <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Search</button>
                        </div>
                </div>
                <div class="row">
                    <form action="{{ route('admin.users.export') }}" method="GET" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="acadamic_year" id="ctr_acadamic_year" value="" />
                        <input type="hidden" name="clear_of_year" id="ctr_class_of_year" value="" />
                        <input type="hidden" name="gender" id="ctr_gender" value="" />

                       {{--  <div class="form-group col-md-4">
                            <input type="file" name="file" class="form-control">
                            
                        </div> --}}

                        <div class="form-group col-md-4">
                            {{-- <button class="btn btn-success">Import User Data</button>  --}}
                            <button type="submit" class="btn btn-warning" id="btnExcelExport">Export Excel</button>
                        </div>
                    </form>
                </div>

            <br>
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered " id="table">
                    <thead>
                        <tr class="filters">
                            <th>Roll No</th>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

<script>
    $(document).ready( function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $('#table').DataTable({
             processing: true,
             serverSide: true,
             ajax: {
              url: "{!! route('admin.users.data') !!}",
              type: 'GET',
              data: function (d) {
                  d.acadamic_year_id = $('#acadamic_year_id').val();
                  d.class_of_year = $('#class_of_year').val();
                  d.gender = $('#gender').val();
              }
             },
            columns: [
                { data: 'roll_no', name: 'roll_no' },
                { data: 'username', name: 'username' },
                { data: 'email', name: 'email' },
                { data: 'status', name: 'status'},
                { data: 'created_at', name:'created_at'},
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
          });
       });

      $('#btnFiterSubmitSearch').click(function(){
         $('#table').DataTable().draw(true);
      });

    $('#acadamic_year_id').change(function(){
        $('#ctr_acadamic_year').val($(this).val());
    });
    $('#class_of_year').change(function(){
        $('#ctr_class_of_year').val($(this).val());
    });
    $('#gender').change(function(){
        $('#ctr_gender').val($(this).val());
    });

    // $(function() {

    //     var table = $('#table').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         ajax: '',
    //         columns: [
    //             { data: 'roll_no', name: 'roll_no' },
    //             { data: 'username', name: 'username' },
    //             { data: 'email', name: 'email' },
    //             { data: 'status', name: 'status'},
    //             { data: 'created_at', name:'created_at'},
    //             { data: 'actions', name: 'actions', orderable: false, searchable: false }
    //         ]
    //     });
    //     table.on( 'draw', function () {
    //         $('.livicon').each(function(){
    //             $(this).updateLivicon();
    //         });
    //     } );
    // });

</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
<div class="modal fade" id="users_exists" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                @lang('users/message.data_exists')
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
    $(document).on("click", ".users_exists", function () {

        var username = $(this).data('name');
        $(".modal-header h4").text( username );
    });
</script>
@stop
