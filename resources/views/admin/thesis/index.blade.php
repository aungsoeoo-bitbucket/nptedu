@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Thesis Group 
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Thesis Group List</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#">Thesis</a></li>
        <li class="active">Thesis Group</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                   Thesis Group
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/thesis/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>Student</th>
                            <th>Group</th>
                            <th>Attend Date</th>
                            <th>@lang('thesis/table.created_at')</th>
                            <th>@lang('thesis/table.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($data))
                        @foreach ($data as $d)
                            <tr>
                                <td>
                                    @foreach($d->thesisGp as $gp)
                                        {{ $gp->student->roll_no }} &nbsp;&nbsp;-&nbsp;&nbsp; {{ $gp->student->username_en }} <br>
                                    @endforeach
                                </td>
                                <td>{{ $d->group->group_name }}</td>
                                <td>{{ date('d-m-Y', strtotime($d->attend_date)) }}</td>
                                <td>{{ date('d/m/Y   H:m A', strtotime($d->created_at)) }}</td>
                                <td>
                                    <a href="{{ URL::to('admin/thesis/' . $d->id . '/edit' ) }}">
                                        <i class="livicon"
                                             data-name="edit"
                                             data-size="18"
                                             data-loop="true"
                                             data-c="#428BCA"
                                             data-hc="#428BCA"
                                             title="Edit Exam">
                                                 
                                             </i>
                                         </a>
                                    <a href="{{ route('admin.thesis.confirm-delete', $d->id) }}" data-toggle="modal"
                                       data-target="#delete_confirm">
                                       <i class="livicon" data-name="remove-alt"
                                            data-size="18" data-loop="true" data-c="#f56954"
                                            data-hc="#f56954"
                                            title="Delete Exam">
                                                
                                            </i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
});
</script>
@stop
