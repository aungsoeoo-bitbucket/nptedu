@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('about/title.update-file') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/summernote/summernote.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/blog.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}">
    <!--end of page level css-->

@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <!--section starts-->
    <h1>@lang('about/title.update-about')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="14" data-c="#000" data-loop="true"></i>
                @lang('general.home')
            </a>
        </li>
        <li>
            <a href="#">@lang('about/title.title')</a>
        </li>
        <li class="active">@lang('about/title.update-about')</li>
    </ol>
</section>
<!--section ends-->
<section class="content paddingleft_right15">
    <!--main content-->
    <div class="row">
        <div class="the-box no-border">
            <!-- errors -->
            {!! Form::model($about, ['url' => URL::to('admin/about/' . $about->id), 'method' => 'put', 'class' => 'bf', 'files'=> true]) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                 <div class="row">
                    <div class="col-sm-12">
                        <label for="active">{{ @trans('about/form.title_label')}}</label>
                        <div class="form-group {{ $errors->first('brief_en', 'has-error') }}">
                            {!! Form::text('brief_en', null, array('class' => 'form-control input-lg','placeholder'=> trans('about/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('brief_en', ':message') }}</span>
                        </div>
                        <label for="active">{{ @trans('about/form.desc_label')}}</label>
                        <div class='box-body pad form-group {{ $errors->first('description_en', 'has-error') }}'>
                            {!! Form::textarea('description_en', NULL, array('placeholder'=>trans('about/form.descripiton'),'rows'=>'5','class'=>'textarea form-control','style'=>'style="width: 100%; height: 200px !important; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"')) !!}
                            <span class="help-block">{{ $errors->first('description_en', ':message') }}</span>
                        </div>
                        <label>Photo</label>
                         <div class='{{ $errors->first('images', 'has-error') }}'>
                            <div class="col-sm-10">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>{{$about->images}}</span>
                                        </span>
         
                                    </div>
                                </div>
                            </div>
                            <input type="file" value="{{old('images[]')}}" class="form-control" id="images" name="images[]"  multiple>
                            <span class="help-block">{{ $errors->first('images', ':message') }}</span>
                            <br>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">@lang('about/form.publish')</button>
                            <a href="{!! URL::to('admin/about') !!}"
                               class="btn btn-danger">@lang('about/form.discard')</a>
                        </div>
                    </div>
                    <!-- /.col-sm-8 -->
                    <div class="col-sm-4">
                        
                    </div>

                    <!-- /.col-sm-4 --> </div>

                {!! Form::close() !!}
        </div>
    </div>
    <!--main content ends-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<!--edit blog-->
<script src="{{ asset('assets/vendors/summernote/summernote.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/js/pages/add_newblog.js') }}" type="text/javascript"></script>
<script>
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
</script>

<!-- end page level js -->
@stop
