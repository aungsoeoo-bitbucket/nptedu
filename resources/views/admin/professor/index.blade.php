@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('professor/title.professorlist')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('professor/title.professorlist')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#">@lang('professor/title.professor')</a></li>
        <li class="active">@lang('professor/title.professorlist')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('professor/title.professorlist')
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/professor/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>@lang('professor/table.name')</th>
                            <th>@lang('professor/table.position')</th>
                            <th>Status</th>
                            <th>@lang('professor/table.created_at')</th>
                            <th>@lang('professor/table.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($professors))
                        @foreach ($professors as $professor)
                            <tr>
                                <td>{{ $professor->name }}</td>
                                <td>{{ $professor->position }}</td>
                                <td>{{ ($professor->active==1)?"Activated":"Pending" }}</td>
                                <td>{{ date('d/m/Y   H:m A', strtotime($professor->created_at)) }}</td>
                                <td>
                                    <div class="dropdown">
                                      <button class="btn btn-primary glyphicon glyphicon-cog dropdown-toggle" type="button" data-toggle="dropdown">
                                      <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ URL::to('admin/professor/' . $professor->id ) }}">
                                                <span class="glyphicon glyphicon-info-sign"
                                                    title="@lang('course/table.view-course')"></span>Detail
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::to('admin/professor/' . $professor->id . '/edit' ) }}">
                                                <span class="glyphicon glyphicon-edit"
                                                 title="@lang('class/table.update-class')">
                                                     
                                                 </span>Edit
                                             </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.professor.confirm-delete', $professor->id) }}" data-toggle="modal"
                                           data-target="#delete_confirm">
                                               <span class="glyphicon glyphicon-trash"
                                                    title="@lang('class/table.delete-class')">
                                                        
                                                    </span>Delete
                                            </a>
                                        </li>
                                      </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
});
</script>
@stop
