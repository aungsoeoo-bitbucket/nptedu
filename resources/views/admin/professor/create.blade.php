@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('professor/title.create') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <!--end of page level css-->
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('professor/title.create')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>@lang('professor/title.title')</li>
        <li class="active">
            @lang('professor/title.create')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('professor/title.create')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => URL::to('admin/professor/'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}

                    <div class="form-group {{ $errors->first('name', 'has-error') }}">
                        <label for="name" class="col-sm-2 control-label">
                            @lang('professor/form.name') *
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('name', null, array('class' => 'form-control', 'placeholder'=>trans('professor/form.name'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('name', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('gender', 'has-error') }}">
                        <label for="email" class="col-sm-2 control-label">Gender *</label>
                        <div class="col-sm-5">
                            <select class="form-control" title="Select Gender..." name="gender">
                                <option value="">Select Gender</option>
                                <option value="male"
                                        @if(old('gender') === 'male') selected="selected" @endif >Male
                                </option>
                                <option value="female"
                                        @if(old('gender') === 'female') selected="selected" @endif >
                                    Female
                                </option>

                            </select>
                        </div>
                        <div class="col-sm-4">
                            <span class="help-block">{{ $errors->first('gender', ':message') }}</span>
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('position', 'has-error') }}">
                        <label for="position" class="col-sm-2 control-label">
                            @lang('professor/form.position') *
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('position', null, array('class' => 'form-control', 'placeholder'=>trans('professor/form.position'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('position', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('bio', 'has-error') }}">
                        <label for="bio" class="col-sm-2 control-label">
                            @lang('professor/form.bio')
                        </label>
                        <div class="col-sm-5">
                            <textarea name="bio" class="form-control" >{{ old('bio')}}</textarea>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('bio', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('phone', 'has-error') }}">
                        <label for="phone" class="col-sm-2 control-label">
                            @lang('professor/form.phone') *
                        </label>
                        <div class="col-sm-5">
                            <input type="number" name="phone" class="form-control" placeholder="{{ trans('professor/form.phone-placeholder') }}">
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('phone', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
                    
                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                        <label for="email" class="col-sm-2 control-label">
                            @lang('professor/form.email')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('email', null, array('class' => 'form-control', 'placeholder'=>trans('professor/form.email-placeholder'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('email', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('facebook', 'has-error') }}">
                        <label for="facebook" class="col-sm-2 control-label">
                            @lang('professor/form.facebook')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('facebook', null, array('class' => 'form-control', 'placeholder'=>trans('professor/form.facebook-placeholder'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('facebook', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('twitter', 'has-error') }}">
                        <label for="twitter" class="col-sm-2 control-label">
                            @lang('professor/form.twitter')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('twitter', null, array('class' => 'form-control', 'placeholder'=>trans('professor/form.twitter-placeholder'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('twitter', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('linkedin', 'has-error') }}">
                        <label for="linkedin" class="col-sm-2 control-label">
                            @lang('professor/form.linkedin')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('linkedin', null, array('class' => 'form-control', 'placeholder'=>trans('professor/form.linkedin-placeholder'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('linkedin', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('photo', 'has-error') }}">
                        <label for="pic" class="col-sm-2 control-label">@lang('professor/form.photo')</label>
                            <div class="col-sm-10">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                                        <img src="http://placehold.it/200x200" alt="professor pic">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;">
                                        
                                    </div>
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input id="pic" name="photo" type="file" class="form-control"/>
                                        </span>
                                        <a href="#" class="btn btn-danger fileinput-exists"
                                           data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                                <span class="help-block">{{ $errors->first('photo', ':message') }}</span>
                            </div>
                    </div>

                     <div class="form-group">
                        <label for="activate" class="col-sm-2 control-label"> {{ trans('professor/form.active') }}</label>
                        <div class="col-sm-5">
                            <input id="active" name="active" type="checkbox"
                                   class="pos-rel p-l-30 custom-checkbox"
                                   value="1" checked >
                            <span>&nbsp;&nbsp;To active user account automatically, click the check box</span></div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/professor/') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>
@stop
