@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('professor/title.professordetail')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" href="{{ asset('assets/css/pages/blog.css') }}" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <!--section starts-->
    <h1>@lang('professor/title.professordetail')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="14" data-c="#000" data-loop="true"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li> @lang('professor/title.title')</li>
        <li class="active">@lang('professor/title.professordetail')</li>
    </ol>
</section>
<!--section ends-->

<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('professor/title.professordetail')
                    </h4>
                     <div class="pull-right">
                        <a href="{{ URL::to('admin/professor') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> @lang('button.back')</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="img-file">
                            @if($professor->photo)
                                <img src="{!! url('/').'/uploads/professor/'.$professor->photo !!}" alt="img"
                                     class="img-responsive"/>
                            @elseif($professor->gender === "male")
                                <img src="{{ asset('assets/images/authors/avatar3.png') }}" alt="..."
                                     class="img-responsive"/>
                            @elseif($professor->gender === "female")
                                <img src="{{ asset('assets/images/authors/avatar5.png') }}" alt="..."
                                     class="img-responsive"/>
                            @else
                                <img src="{{ asset('assets/images/authors/no_avatar.jpg') }}" alt="..."
                                     class="img-responsive"/>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="professor">

                                    <tr>
                                        <td>@lang('professor/table.name')</td>
                                        <td>
                                            <p class="user_name_max">{{ $professor->name }}</p>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            @lang('professor/form.gender')
                                        </td>
                                        <td>
                                            {{ $professor->gender }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('professor/form.position')</td>
                                        <td>
                                            {{ $professor->position }}
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>@lang('professor/form.bio')</td>
                                        <td>
                                            {!! $professor->bio !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('professor/form.phone')</td>
                                        <td>
                                            {{ $professor->phone }}
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>@lang('professor/form.email')</td>
                                        <td>
                                            {{ $professor->email }}
                                        </td>
                                    </tr>

                                     <tr>
                                        <td>@lang('professor/form.facebook')</td>
                                        <td>
                                            {{ $professor->facebook }}
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>@lang('professor/form.twitter')</td>
                                        <td>
                                            {{ $professor->twitter }}
                                        </td>
                                    </tr>

                                     <tr>
                                        <td>@lang('professor/form.linkedin')</td>
                                        <td>
                                            {{ $professor->linkedin }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>@lang('professor/form.active')</td>
                                        <td>

                                            @if($professor->active!='')
                                                Activated
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>@lang('professor/table.created_at')</td>
                                        <td>
                                            {!! $professor->created_at->diffForHumans() !!}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>
@stop
