@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
@lang('roles/title.management')
@parent
@stop

{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>@lang('roles/title.management')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#"> @lang('roles/title.roles')</a></li>
        <li class="active">@lang('roles/title.roles_list')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('roles')
                    </h4>
                    <div class="pull-right">
                    <a href="{{ route('admin.roles.create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                    </div>
                </div>
                <br />
                <div class="panel-body">
                    @if ($roles->count() >= 1)
                        <div class="table-responsive">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>@lang('roles/table.id')</th>
                                    <th>@lang('roles/table.name')</th>
                                    <th>@lang('roles/table.users')</th>
                                    <th>@lang('roles/table.created_at')</th>
                                    <th>@lang('roles/table.actions')</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($roles as $role)
                                <tr>
                                    <td>{!! $role->id !!}</td>
                                    <td>{!! $role->name !!}</td>
                                    <td>{!! $role->users()->count() !!}</td>
                                    <td>{!! $role->created_at->diffForHumans() !!}</td>
                                    <td>
                                        <div class="dropdown">
                                          <button class="btn btn-primary glyphicon glyphicon-cog dropdown-toggle" type="button" data-toggle="dropdown">
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ route('admin.roles.edit', $role->id) }}">
                                                    <span class="glyphicon glyphicon-edit"
                                                     title="@lang('class/table.update-class')">
                                                         
                                                     </span>Edit
                                                 </a>
                                            </li>
                                            <li>
                                                @if ($role->id !== 1)
                                                    @if($role->users()->count()>0)
                                                        <a href="#" data-toggle="modal" data-target="#users_exists" data-name="{!! $role->name !!}" class="users_exists">
                                                           <span class="    glyphicon glyphicon-warning-sign"
                                                        title="@lang('class/table.delete-class')"></span>Delete
                                                        </a>
                                                    @else
                                                         <a href="{{ route('admin.roles.confirm-delete', $role->id) }}" data-toggle="modal" data-target="#delete_confirm">
                                                           <span class="glyphicon glyphicon-trash"
                                                                title="@lang('class/table.delete-class')">
                                                                    
                                                                </span>Delete
                                                        </a>
                                                    @endif

                                                @endif
                                               
                                            </li>
                                          </ul>
                                        </div>
                                           
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    @else
                        @lang('general.noresults')
                    @endif   
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>




@stop

{{-- Body Bottom confirm modal --}}
@section('footer_scripts')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
<div class="modal fade" id="users_exists" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                @lang('roles/message.users_exists')
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
    $(document).on("click", ".users_exists", function () {

        var role = $(this).data('name');
        $(".modal-header h4").text( role );
    });</script>
@stop
