@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('freedownload/title.add-file') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/summernote/summernote.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/blog.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}">
    <!--end of page level css-->

@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <!--section starts-->
    <h1>@lang('freedownload/title.add-file')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="14" data-c="#000" data-loop="true"></i>
                @lang('general.home')
            </a>
        </li>
        <li>
            <a href="#">@lang('freedownload/title.title')</a>
        </li>
        <li class="active">@lang('freedownload/title.add-file')</li>
    </ol>
</section>
<!--section ends-->
<section class="content paddingleft_right15">
    <!--main content-->
    <div class="row">
        <div class="the-box no-border">
            <!-- errors -->
            {!! Form::open(array('url' => URL::to('admin/freedownload'), 'method' => 'post', 'class' => 'bf', 'files'=> true)) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                 <div class="row">
                    <div class="col-sm-12">
                        <label for="active">{{ @trans('freedownload/form.title_label')}}</label>
                        <div class="form-group {{ $errors->first('title_en', 'has-error') }}">
                            {!! Form::text('title_en', null, array('class' => 'form-control input-lg','placeholder'=> trans('freedownload/form.title'))) !!}
                            <span class="help-block">{{ $errors->first('title_en', ':message') }}</span>
                        </div>
                        <label for="active">{{ @trans('freedownload/form.desc_label')}}</label>
                        <div class='box-body pad form-group {{ $errors->first('description_en', 'has-error') }}'>
                            {!! Form::textarea('description_en', NULL, array('placeholder'=>trans('freedownload/form.descripiton'),'rows'=>'5','class'=>'textarea form-control','style'=>'style="width: 100%; height: 200px !important; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"')) !!}
                            <span class="help-block">{{ $errors->first('description_en', ':message') }}</span>
                        </div>

                        <div class="form-group">
                           <label for="active">Active &nbsp;&nbsp;</label><input type="checkbox" name="active" id="active" value="1" checked>
                        </div>
                        <div class='{{ $errors->first('file', 'has-error') }}'>
                            <label>Attach File</label>
                            <input type="file" name="file" id="file" class="form-control">
                            <span class="help-block">{{ $errors->first('file', ':message') }}</span>
                            <br>
                        </div>

                        <div class='{{ $errors->first('youtube_url', 'has-error') }}'>
                            <label>Youtube Link</label>
                            <input type="text" name="youtube_url" id="youtube_url" class="form-control" value="{{ old('youtube_url')}}">
                            <span class="help-block">{{ $errors->first('youtube_url', ':message') }}</span>
                            <br>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success">@lang('freedownload/form.publish')</button>
                            <a href="{!! URL::to('admin/freedownload') !!}"
                               class="btn btn-danger">@lang('freedownload/form.discard')</a>
                        </div>
                    </div>
                    <!-- /.col-sm-8 -->
                    <div class="col-sm-4">
                        
                    </div>

                    <!-- /.col-sm-4 --> </div>

                {!! Form::close() !!}
        </div>
    </div>
    <!--main content ends-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<!--edit blog-->
<script src="{{ asset('assets/vendors/summernote/summernote.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('assets/js/pages/add_newblog.js') }}" type="text/javascript"></script>
<script>
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
</script>

<!-- end page level js -->
@stop
