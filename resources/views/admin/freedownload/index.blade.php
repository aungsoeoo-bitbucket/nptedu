@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('freedownload/title.title')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('freedownload/title.filelist')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#">@lang('freedownload/title.title')</a></li>
        <li class="active">@lang('freedownload/title.filelist')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('freedownload/title.filelist')
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/freedownload/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>@lang('freedownload/table.title')</th>
                            <th>Post By</th>
                            <th>URL</th>
                            <th>@lang('freedownload/table.status')</th>
                            <th>@lang('freedownload/table.created_at')</th>
                            <th>@lang('freedownload/table.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($files))
                        @foreach ($files as $file)
                            <tr>
                                <td>{{ str_limit($file->title_en, $limit = 24, $end = '...') }}</td>
                                <td>{{ $file->user->username_en}}</td>
                                <td>{{ $file->youtube_url }}</td>
                                <td>{{ ($file->active==1)?'Active':'Pending' }}</td>
                                <td>{{ $file->created_at}}</td>
                                <td>
                                    <div class="dropdown">
                                      <button class="btn btn-primary glyphicon glyphicon-cog dropdown-toggle" type="button" data-toggle="dropdown">
                                      <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
    
                                        <li>
                                            <a href="{{ URL::to('admin/freedownload/' . $file->id . '/edit' ) }}">
                                                <span class="glyphicon glyphicon-edit"
                                                 title="@lang('class/table.update-class')">
                                                     
                                                 </span>Edit
                                             </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.freedownload.confirm-delete', $file->id) }}" data-toggle="modal"
                                           data-target="#delete_confirm">
                                               <span class="glyphicon glyphicon-trash"
                                                    title="@lang('class/table.delete-class')">
                                                        
                                                    </span>Delete
                                            </a>
                                        </li>
                                      </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                "order": [[ 4, "desc" ]]
            });
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop
