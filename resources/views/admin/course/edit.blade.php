@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
@lang('course/title.edit')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <!--end of page level css-->

@stop

{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('course/title.edit')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li>@lang('class/title.title')</li>
        <li class="active">@lang('course/title.edit')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('course/title.edit')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($course, ['url' => URL::to('admin/courses') . '/' . $course->id, 'method' => 'put', 'class' => 'form-horizontal', 'files'=> true]) !!}
                   
                    <div class="form-group {{ $errors->first('class_id', 'has-error') }}">
                        <label for="class_id" class="col-sm-2 control-label">
                            @lang('class/form.name')
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="class_id">
                                <option value="">Select Class First</option>
                                @foreach($classes as $class)
                                  <option value="{{$class->id}}" {{ ($course->class_id==$class->id) ? 'selected':''}} >{{$class->class_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('class_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                     <div class="form-group {{ $errors->first('academic_year_id', 'has-error') }}">
                        <label for="academic_year_id" class="col-sm-2 control-label">
                            Academic Batch
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="academic_year_id">
                                <option value="">Select Batch</option>
                                @foreach($academic_years as $acy)
                                  <option value="{{$acy->id}}" {{ ($course->academic_year_id==$acy->id) ? 'selected':''}} >{{$acy->acadamic_year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('academic_year_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('year_id', 'has-error') }}">
                        <label for="year_id" class="col-sm-2 control-label">
                            @lang('year/form.name')
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="year_id">
                                <option value="">Select Year</option>
                                @foreach($years as $y)
                                  <option value="{{$y->id}}" {{ ($course->year_id==$y->id) ? 'selected':''}} >{{$y->year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('year_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('quarter_id', 'has-error') }}">
                        <label for="quarter_id" class="col-sm-2 control-label">
                            @lang('quarter/form.name')
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="quarter_id">
                                <option value="">Select Quarter</option>
                                @foreach($quarters as $q)
                                  <option value="{{$q->id}}" {{ ($course->quarter_id==$q->id) ? 'selected':''}} >{{$q->quarter_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('quarter_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('course_name', 'has-error') }}">
                        <label for="course_name" class="col-sm-2 control-label">
                            @lang('course/form.name')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('course_name', null, array('class' => 'form-control', 'placeholder'=>trans('course/form.name'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('course_name', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('course_code', 'has-error') }}">
                        <label for="course_code" class="col-sm-2 control-label">
                            Course Code
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('course_code', null, array('class' => 'form-control', 'placeholder'=>'Course Code')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('course_code', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                     <div class="form-group {{ $errors->first('course_photo', 'has-error') }}">
                        <label for="pic" class="col-sm-2 control-label">@lang('course/form.photo')</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                                    @if($course->course_photo)
                                        <img src="{!! url('/').'/uploads/course/'.$course->course_photo !!}" alt="img"
                                             class="img-responsive"/>
                                    @else
                                        <img src="http://placehold.it/200x200" alt="course photo" 
                                             class="img-responsive"/>
                                    @endif
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;">
                                    
                                </div>
                                <div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input id="pic" name="course_photo" type="file"
                                           class="form-control"/>
                                </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput" style="color: black !important;">Remove</a>
                                </div>
                            </div>
                            {!! $errors->first('pic_file', '<span class="help-block">:message</span>') !!}
                        </div>
                        </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/courses/') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/edituser.js') }}"></script>

</script>
@stop
