@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    Exam Result :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <!--end of page level css-->
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        Create New Exam
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>Exam Result</li>
        <li class="active">
            Exam Result Create
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Exam Result Creates
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => URL::to('admin/exam-result/'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}

                    <div class="form-group {{ $errors->first('batch_id', 'has-error') }}">
                        <label for="batch_id" class="col-sm-2 control-label">
                           Academic Batch
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="batch_id" id="batch_id">
                                <option value="">Select Batch</option>
                                @foreach($batchs as $b)
                                  <option value="{{$b->id}}" {{ (old('batch_id')==$b->id)?'selected':'' }}>{{$b->acadamic_year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('batch_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('year_id', 'has-error') }}">
                        <label for="year_id" class="col-sm-2 control-label">
                           Class of Year
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="year_id" id="year_id">
                                <option value="">Select Year</option>
                                @foreach($years as $y)
                                  <option value="{{$y->id}}" {{ (old('year_id')==$y->id)?'selected':'' }}>{{$y->year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('year_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
                   
                    <div class="form-group {{ $errors->first('quarter_id', 'has-error') }}">
                        <label for="quarter_id" class="col-sm-2 control-label">
                           Quarter
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="quarter_id" id="quarter_id">
                              {{--   <option value="">Select Quarter</option>
                                @foreach($quarters as $q)
                                  <option value="{{$q->id}}" {{ (old('quarter_id')==$q->id)?'selected':'' }}>{{$q->quarter_name}}</option>
                                @endforeach --}}
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('quarter_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('user_id', 'has-error') }}">
                        <label for="user_id" class="col-sm-2 control-label">
                            Student Roll No
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control select2students" name="user_id"></select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('user_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    

                    <div id="grade_div">
                       
                    </div>



                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/exam-result/') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <input type="hidden" id="ctr_token" value="{{ csrf_token()}}">
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>

    <script>
        $(document).ready(function () {

            $baseurl = "https://naypyitawmba.com/";
            // $baseurl = "http://localhost:8888/nptedu/public/";

            var quarter_id = $('#quarter_id').val();
            // if (exam_id!=null || exam_id!=undefined) {
            //      getcourses(exam_id);
            // }else{
            //     return false;
            // }
                

        });

         $('.select2students').select2({
            placeholder: 'Select Student',
            ajax: {
              url: $baseurl+'select2-autocomplete-ajax-student',
              delay: 250,
              dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        batch: $('#batch_id').val()
                    };
                },

                processResults: function (data) {
                    console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: "("+item.roll_no+ ")" + item.username_en,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
          }); 

        $('#year_id').change(function(){
                getQuarterByYear($('#year_id').val());
        });

        function getQuarterByYear($year_id) {
            $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                 $.ajax({
                    url : $baseurl+'exam-result/getQuarterByYear',
                    dataType : 'html',
                    method : 'post',
                    data : {
                            'year_id' : $year_id,
                            '_token': $('#ctr_token').val()
                    },
                    success : function(data){
                        $('#quarter_id').html(data);
                        if(data==""){
                            $('#quarter_id').html('<option value="">Select Quarter</option>');
                        }
                    },
                    error : function(error){
                        console.log(error);
                    }
                });
        }


        $('#quarter_id').change(function(){
                getcourses($('#quarter_id').val());
        });

        function getcourses($quarter_id) {
                $('#grade_div').html('');
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                $.ajax({
                    url : $baseurl+'exam-result/getCoursesByQuarter',
                    dataType : 'json',
                    method : 'post',
                    data : {
                            // 'batch_id':$('#batch_id').val(),
                            'year_id':$('#year_id').val(),
                            'quarter_id' : $quarter_id,
                            '_token': $('#ctr_token').val()
                    },
                    success : function(response){

                        $.each(response.data, function(key,obj) {
                            // console.log(obj.course_id);
                           $('#grade_div').append('<div class="form-group"><label for="course" class="col-sm-2 control-label">Course</label><div class="col-sm-5"><label class="control-label">'+obj.course_code+'</label>'+
                                '<select class="form-control" name="grade[]">'+
                                    '<option value="">Select grade</option>'+
                                    '<option value="A+">A+</option>'+
                                    '<option value="A">A</option>'+
                                    '<option value="A-">A-</option>'+
                                    '<option value="B+">B+</option>'+
                                    '<option value="B">B</option>'+
                                    '<option value="B-">B-</option>'+
                                    '<option value="C">C</option>'+
                                    '<option value="D">D</option>'+
                                    '<option value="E">E</option>'+
                                '</select>'+
                            '<input type="hidden" class="form-control" name="course_id[]" value="'+obj.id+'"></div></div>');
                        });
                        
                    },
                    error : function(error){
                        console.log(error);
                    }
                });
            
        }

    </script>
@stop
