@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    Exam Result Edit :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <!--end of page level css-->
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        Exam Result Edit
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>Exam Result</li>
        <li class="active">
            Exam Result Edit
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Exam Result Edit
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($exam_result, ['url' => URL::to('admin/exam-result/'. $exam_result->id.''), 'method' => 'put', 'class' => 'form-horizontal','id'=>'commentForm', 'enctype'=>'multipart/form-data','files'=> true]) !!}

                    <div class="form-group {{ $errors->first('batch_id', 'has-error') }}">
                        <label for="batch_id" class="col-sm-2 control-label">
                           Academic Batch
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="batch_id" id="batch_id">
                                <option value="">Select Batch</option>
                                @foreach($batchs as $b)
                                  <option value="{{$b->id}}" {{ (old('batch_id',$exam_result->batch_id)==$b->id)?'selected':'' }}>{{$b->acadamic_year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('batch_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
                    

                    <div class="form-group {{ $errors->first('year_id', 'has-error') }}">
                        <label for="year_id" class="col-sm-2 control-label">
                           Class of Year
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="year_id" id="year_id">
                                <option value="">Select Year</option>
                                @foreach($years as $y)
                                  <option value="{{$y->id}}" {{ (old('year_id',$exam_result->year_id)==$y->id)?'selected':'' }}>{{$y->year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('year_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                   
                    <div class="form-group {{ $errors->first('quarter_id', 'has-error') }}">
                        <label for="quarter_id" class="col-sm-2 control-label">
                           Quarter
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="quarter_id" id="quarter_id">
                                 <option value="">Select Quarter</option>
                                @foreach($quarters as $q)
                                  <option value="{{$q->id}}" {{ (old('quarter_id',$exam_result->quarter_id)==$q->id)?'selected':'' }}>{{$q->quarter_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('quarter_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('user_id', 'has-error') }}">
                        <label for="user_id" class="col-sm-2 control-label">
                            Student Roll No
                        </label>
                        <div class="col-sm-5">
                           {{--  <select class="form-control" name="user_id">
                                <option value="">Select Student</option>
                                @foreach($students as $student)
                                  <option value="{{$student->id}}" {{ (old('user_id',$exam_result->user_id)==$student->id)?'selected':'' }}>{{$student->roll_no}} &nbsp; {{$student->username}}
                                  </option>
                                @endforeach
                            </select> --}}
                            <select class="form-control select2students" name="user_id"></select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('user_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    

                    <div id="grade_div">
                       
                    </div>



                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/exam-result/') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <input type="hidden" id="ctr_token" value="{{ csrf_token()}}">
                <input type="hidden" id="ctr_result" value="{{ $exam_result->id }}">
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>


    <script>
         $baseurl = "https://naypyitawmba.com/";
        $(document).ready(function () {

            var student_id = "<?php echo $exam_result->user_id; ?>";

            var student_name_en = "<?php echo $exam_result->student->username_en; ?>";

            var student_roll_no = "<?php echo $exam_result->student->roll_no; ?>";


            var year_id = $('#year_id').val();
            var exam_result_id = $('#ctr_result').val();
            getresults(exam_result_id);

            // if(year_id!='' || year_id!=undefined){
            //     getQuarterByYear(year_id);
            // }

           
            // setTimeout(function() {
            //      var exam_result_id = $('#ctr_result').val();
            //       getresults(exam_result_id);

            // $('.select2students').select2('data', {id: '123', text: 'res_data.primary_email'});

            var data = {
                id: student_id,
                text: '('+student_roll_no + ')' +student_name_en
            };

            var newOption = new Option(data.text, data.id, false, false);
            $('.select2students').append(newOption).trigger('change');

            

        });

        $('.select2students').select2({
            placeholder: 'Select Student',
            ajax: {
              url: $baseurl+'select2-autocomplete-ajax-student',
              delay: 250,
              dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                        batch: $('#batch_id').val()
                    };
                },

                processResults: function (data) {
                    console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: "("+item.roll_no+ ")" + item.username_en,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
          }); 



        $('#year_id').change(function(){
                getQuarterByYear($('#year_id').val());
        });

        function getQuarterByYear($year_id) {
            $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                 $.ajax({
                    url : $baseurl+'exam-result/getQuarterByYear',
                    dataType : 'html',
                    method : 'post',
                    data : {
                            'year_id' : $year_id,
                            '_token': $('#ctr_token').val()
                    },
                    success : function(data){
                        $('#quarter_id').html(data);
                        if(data==""){
                            $('#quarter_id').html('<option value="">Select Quarter</option>');
                        }

                        
                    },
                    error : function(error){
                        console.log(error);
                    }
                });
        }


        $('#quarter_id').change(function(){
                getresults($('#quarter_id').val());
        });


        function getresults($exam_result_id) {
                $('#grade_div').html('');
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                $.ajax({
                    url : $baseurl+'exam-result/getCoursesByExamEdit',
                    dataType : 'json',
                    method : 'post',
                    data : {
                            'id' : $('#ctr_result').val(),
                            '_token': $('#ctr_token').val()
                    },
                    success : function(response){

                        var grade_arr = ['A+','A','A-','B+','B','B-','C','D','E'];

                        $.each(response.data, function(key,obj) {
                           $('#grade_div').append('<div class="form-group"><label for="course" class="col-sm-2 control-label">Course</label><div class="col-sm-5"><label class="control-label">'+obj.course_code+'</label>'+
                            '<select class="form-control" name="grade[]" id="ctr_grade_'+key+'">'+
                                    '<option value="">Select grade</option>'+
                                    '<option value="A+" >A+</option>'+
                                    '<option value="A" >A</option>'+
                                    '<option value="A-" >A-</option>'+
                                    '<option value="B+" >B+</option>'+
                                    '<option value="B" >B</option>'+
                                    '<option value="B-" >B-</option>'+
                                    '<option value="C" >C</option>'+
                                    '<option value="D">D</option>'+
                                    '<option value="E" >E</option>'+
                            '</select>'+
                            '<input type="hidden" class="form-control" name="course_id[]" value="'+obj.course_id+'"></div></div>');

                            $.each(grade_arr, function(i,g) {
                               if(g=="A+"){
                                    $('#ctr_grade_'+key).val(g);

                               }
                            });

                        

                        });
                        
                    },
                    error : function(error){
                        console.log(error);
                    }
                });
            
        }

        //  function getresults($exam_result_id) {
              
        //         $.ajaxSetup({
        //           headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //           }
        //         });

        //         $.ajax({
        //             url : $baseurl+'exam-result/getExamResult',
        //             dataType : 'json',
        //             method : 'post',
        //             data : {
        //                     'id' : $exam_result_id,
        //                     '_token': $('#ctr_token').val()
        //             },
        //             success : function(response){
        //                var results = (response.data[0].grade); 
        //                 var arr=  results.split(',');
        //                 $.each(arr, function(key,res) {
        //                   var d= $("#grade_"+key).val(res);
        //                 });
                        
        //             },
        //             error : function(error){
        //                 console.log(error);
        //             }
        //         });
            
        // }

    </script>
@stop
