@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Exam Result
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />

    
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Exam Result</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#">Exam Result</a></li>
        <li class="active">Exam Result List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Exam Result List 
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/exam-result/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
            <div class="row">
                <?php 
                    $batch_id=(isset($_GET['batch_id'])?$_GET['batch_id']:'');
                    $keyword=(isset($_GET['keyword'])?$_GET['keyword']:'');
                    $quarter_id=(isset($_GET['quarter_id'])?$_GET['quarter_id']:'');
                    $course_id=(isset($_GET['course_id'])?$_GET['course_id']:'');
                    $grade=(isset($_GET['grade'])?$_GET['grade']:'');


                ?>

               {!! Form::open(array('url' => URL::to('admin/exam-result'), 'method' => 'get', 'class' => 'form-horizontal', 'files'=> true)) !!}
                    
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <div class="col-md-12 ">
                            <select class="form-control" id="batch_id" name="batch_id">
                                <option value="">Select Batch</option>
                                @foreach($batchs as $b)
                                  <option value="{{$b->id}}" {{ (old('batch_id',$batch_id)==$b->id)?'selected':'' }}>{{$b->acadamic_year}}</option>
                                @endforeach
                            </select>
                        </div>
                        </div>
                    </div>


                    <div class="col-md-4 ">
                        <div class="form-group">
                            <select class="form-control" id="quarter_id" name="quarter_id">
                                <option value="">Select Quarter</option>
                                @foreach($quarters as $q)
                                  <option value="{{$q->id}}" {{ (old('quarter_id',$quarter_id)==$q->id)?'selected':'' }}>{{$q->quarter_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="col-md-4 ">
                         <div class="form-group">
                             <div class="col-sm-12">
                                 <select class="form-control" name="course_id" id="course_id">
                                
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3 ">
                         <div class="form-group">
                             <div class="col-sm-12">
                                <input type="text" name="keyword" value="{{ old('keyword',$keyword) }}" id="keyword" class="form-control" placeholder="Enter Roll No/ Name" >
                            </div>
                        </div>
                    </div>

                  {{--   <div class="col-md-3">
                        <select class="form-control" name="user_id" id="users"></select>
                    </div> --}}

                    <div class="col-md-3 ">
                         <div class="form-group">
                             <div class="col-sm-8">
                                <input type="text" name="grade" value="{{ old('grade',$grade) }}" id="grade" class="form-control" placeholder="Enter Grade" >
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary">
                                    Search
                                </button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
                <div class="card bg-light mt-3">
                    <div class="card-body">
                        <a class="btn btn-warning" href="{{ route('admin.results.export') }}">Export Exam Results</a>

                    </div>
                </div>
                <br>
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>Roll No</th>
                            <th>Student</th>
                            <th>Quarter</th>
                            <th>Course</th>
                            <th>Grade</th>
                            <th>Post By</th>
                            <th>@lang('course/table.created_at')</th>
                            <th>@lang('course/table.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($results))
                        @foreach ($results as $result)
                            <tr>
                                <td>{{ $result->roll_no }}</td>
                                <td>{{ $result->username }}</td>

                                <td>{{ $result->quarter_name  }}</td>
   
                                
                                <td>
            
                                    {{ $result->course_code }}

                                </td>
                                @if($result->grade=='B-')
                                    <td style="background-color:#FFFF00">
                                    {{ $result->grade}}
                                    </td>
                                @elseif($result->grade=='C')
                                    <td style="background-color:#2a6dcc; color: #fff">
                                    {{ $result->grade}}
                                    </td>
                                @else
                                    <td >
                                    {{ $result->grade}}
                                    </td>
                                @endif
                                <td>
                                   {{--  {{ $result->postby }} --}}
                                </td>

                                <td>{{ date('d/m/Y', strtotime($result->created_at)) }}</td>
                                <td>
                                    <div class="dropdown">
                                      <button class="btn btn-primary glyphicon glyphicon-cog dropdown-toggle" type="button" data-toggle="dropdown">
                                      <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                       {{--  <li>
                                                <a href="{{ URL::to('admin/exam-result/' . $result->id ) }}">
                                                <span class="glyphicon glyphicon-info-sign"
                                                    title="@lang('course/table.view-course')"></span>Detail
                                            </a>
                                        </li> --}}
                                        <li>
                                            <a href="{{ URL::to('admin/exam-result/' . $result->id . '/edit' ) }}">
                                                <span class="glyphicon glyphicon-edit"
                                                 title="@lang('class/table.update-class')">
                                                     
                                                 </span>Edit
                                             </a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::to('admin/exam-result/' . $result->id . '/confirm-delete' ) }}" data-toggle="modal"
                                           data-target="#delete_confirm">
                                               <span class="glyphicon glyphicon-trash"
                                                    title="@lang('class/table.delete-class')">
                                                        
                                                    </span>Delete
                                            </a>
                                        </li>
                                      </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {!! $results->appends(request()->input())->render() !!}
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
  </div>
</div>

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
     $baseurl = "https://naypyitawmba.com/";
        $(function () {
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });

        $('#users').select2({
            placeholder: 'Select user',
            ajax: {
              url : $baseurl+'select2-autocomplete-ajax',
              dataType: 'json',
              delay: 250,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                        return {
                            text: item.roll_no +'   '+ item.username,
                            id: item.id
                        }
                    })
                };
              },
              cache: true
            }
        });

        $(document).ready(function(){

            var quarter_id = $('#quarter_id').val();
            if (quarter_id!='' || quarter_id!='undefined') {
                 getcourses(quarter_id);
            }
        });

        $('#quarter_id').change(function(){
                getcourses($(this).val());
        });

        function getcourses($qid) {
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                $.ajax({
                    url : $baseurl+'course-management/getCoursesByQuarter',
                    dataType : 'html',
                    method : 'post',
                    data : {
                            'quarter_id' : $qid,
                            '_token': $('#ctr_token').val()
                    },
                    success : function(data){
                        $('#course_id').html(data);
                        if(data==""){
                            $('#course_id').html('<option value="">Select Course</option>');
                        }

                        $course_id = '<?php echo $course_id;?>'
                        $("#course_id").val($course_id);
                    },
                    error : function(error){
                        console.log(error);
                    }
                });
        }
</script>
@stop
