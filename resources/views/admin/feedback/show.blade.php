@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Feedback Detail
@stop

{{-- page level styles --}}
@section('header_styles')    
    <!--page level css starts here-->
	<link href="{{ asset('assets/css/pages/mail_box.css') }}" rel="stylesheet" type="text/css" />
    
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <h1>Feedback Detail</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/feedback') }}">Feedback</a>
                    </li>
                    <li class="active">Feedback Detail</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row web-mail">
                    
                    <div class="col-lg-10 col-md-9 col-sm-8">
                        <div class="whitebg">
                            <table class="table table-striped table-advance">
                                <tbody>
                                    <tr>
                                        <td colspan="4">
                                            <div>
                                                <div class="col-md-2 col-lg-1 col-sm-2 col-xs-3">
                                                    <a href="#">
                                                        <img data-src="holder.js/42x42/#000:#fff" class="img-responsive" alt="riot" />
                                                    </a>
                                                </div>
                                                <div col-xs-11>
                                                    <a href="#" class="graytext">
                                                        <strong>{{ $feedback->name }}</strong>
                                                        <br>&lt; {{ $feedback->email }}&gt;</a>
                                                </div>
                                                <div>{{ date('d M Y   H:m A', strtotime($feedback->created_at)) }}</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <br>
                                                    <p>
                                                       {{ $feedback->message }}
                                                    </p>
                                                    <br>
                                                    @if($feedback->photo!='')
                                                     <img src="{{ asset('/uploads/feedbacks/'.$feedback->photo) }}" style="width: auto; height: auto;" alt="document" class="img-responsive" />
                                                    @endif
                                                  
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="nopadmar">
                                                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                                        <a href="{{ URL::to('admin/feedback') }}" class="btn btn-sm btn-warning">
                                                            <span class="livicon" data-n="back" data-s="12" data-c="white" data-hc="white"></span>
                                                            &nbsp;&nbsp;Back
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td width="3%"></td>
                                        <td width="13%" class="view-message text-right">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <!-- content -->
        
    @stop

{{-- page level scripts --}}
@section('footer_scripts')
@stop
