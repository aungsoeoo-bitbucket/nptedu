@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Feedback Inbox
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/css/pages/alertmessage.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/mail_box.css') }}" rel="stylesheet" type="text/css" />

    <!-- page level css ends-->
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <h1>FeedBacks</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/feedback') }}">Feedback</a>
                    </li>
                    <li class="active">Inbox</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row web-mail">
                    <div class="col-lg-10 col-md-9 col-sm-8">
                        <div class="whitebg">
                            <table class="table table-striped table-advance table-hover" id="inbox-check">
                                <tbody>
                                    @if(count($feedbacks)>0)
                                        @foreach ($feedbacks as $feedback)
                                            <tr data-messageid="1" class="unread">
                                                <td style="width:7%;" class="inbox-small-cells">
                                                    <div class="checker">
                                                        <span>
                                                            <a href="{{ route('admin.feedback.confirm-delete', $feedback->id) }}" data-toggle="modal"
                                                               data-target="#delete_confirm">
                                                               <i class="livicon" data-name="trash"
                                                                    data-size="18" data-loop="true" data-c="#f56954"
                                                                    data-hc="#f56954">
                                                                        
                                                                </i>
                                                            </a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td style="width:22%;" class="view-message hidden-xs">
                                                    <a href="{{ URL::to('admin/feedback/' . $feedback->id ) }}">
                                                        {{ $feedback->name }}</a>
                                                </td>
                                                <td style="width:56%;" class="view-message ">
                                                    <a href="{{ URL::to('admin/feedback/' . $feedback->id ) }}">{{ str_limit($feedback->message, $limit = 100, $end = '...') }}</a>
                                                </td>
                                                <td style="width:13%;" class="view-message text-right">
                                                    <a href="{{ URL::to('admin/feedback/' . $feedback->id ) }}">{{ date('d/m/Y   H:m A', strtotime($feedback->created_at)) }}</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>No Feedback data!</td>
                                        </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <!-- content -->
        
    @stop
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
      </div>
    </div>
{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/js/pages/mail_box.js') }}"></script>

    <script>
    $(function () {
        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
    });
    </script>
@stop
