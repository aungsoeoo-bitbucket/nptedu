@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('quarter/title.create') :: @parent
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('quarter/title.create')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>@lang('quarter/title.title')</li>
        <li class="active">
            @lang('quarter/title.create')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('quarter/title.create')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => URL::to('admin/quarter/'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}

                              <div class="form-group {{ $errors->first('year_id', 'has-error') }}">
                                <label for="year_id" class="col-sm-2 control-label">
                                    @lang('year/form.name')
                                </label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="year_id">
                                        <option value="">Select Year</option>
                                        @foreach($years as $y)
                                          <option value="{{$y->id}}">{{$y->year}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    {!! $errors->first('year_id', '<span class="help-block">:message</span> ') !!}
                                </div>
                            </div>
                    
                            <div class="form-group {{ $errors->first('quarter_name', 'has-error') }}">
                                <label for="quarter_name" class="col-sm-2 control-label">
                                    @lang('quarter/form.name')
                                </label>
                                <div class="col-sm-5">
                                    {!! Form::text('quarter_name', null, array('class' => 'form-control', 'placeholder'=>trans('quarter/form.name'))) !!}
                                </div>
                                <div class="col-sm-4">
                                    {!! $errors->first('quarter_name', '<span class="help-block">:message</span> ') !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-4">
                                    <a class="btn btn-danger" href="{{ URL::to('admin/quarter/') }}">
                                        @lang('button.cancel')
                                    </a>
                                    <button type="submit" class="btn btn-success">
                                        @lang('button.save')
                                    </button>
                                </div>
                            </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop
