@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
   @lang('groups/title.management')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('class/title.classlist')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#">@lang('groups/title.groups')</a></li>
        <li class="active">@lang('groups/title.management')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('groups/title.management')
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/groups/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>@lang('groups/table.name')</th>
                            <th>Students</th>
                            <th>@lang('groups/table.created_at')</th>
                            <th>@lang('groups/table.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($groups))
                        @foreach ($groups as $group)
                            <tr>
                                <td>{{ $group->group_name }}</td>
                                <td>{{ $group->students()->count()}}</td>
                                <td>{{ date('d/m/Y   H:m A', strtotime($group->created_at)) }}</td>
                                <td>
                                     <div class="dropdown">
                                          <button class="btn btn-primary glyphicon glyphicon-cog dropdown-toggle" type="button" data-toggle="dropdown">
                                          <span class="caret"></span></button>
                                          <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ URL::to('admin/groups/' . $group->id . '/edit' ) }}">
                                                    <span class="glyphicon glyphicon-edit"></span>Edit
                                                 </a>
                                            </li>
                                            <li>

                                                @if($group->students()->count()>0)
                                                    <a href="#" data-toggle="modal" data-target="#users_exists" data-name="{!! $group->group_name !!}" class="users_exists">
                                                       <span class="glyphicon glyphicon-warning-sign"></span>Delete
                                                    </a>
                                                @else
                                                     <a href="{{ route('admin.groups.confirm-delete', $group->id) }}" data-toggle="modal" data-target="#delete_confirm">
                                                       <span class="glyphicon glyphicon-trash">
                                                                
                                                            </span>Delete
                                                    </a>
                                                @endif

                                               
                                            </li>
                                          </ul>
                                        </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
});
</script>

<div class="modal fade" id="users_exists" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                @lang('groups/message.users_exists')
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
    $(document).on("click", ".users_exists", function () {

        var group_name = $(this).data('name');
        $(".modal-header h4").text( group_name );
    });
</script>

@stop

