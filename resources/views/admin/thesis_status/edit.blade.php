@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
@lang('class/title.edit')
@parent
@stop

{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('class/title.edit')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li>@lang('thesis/title.status')</li>
        <li class="active"> @lang('thesis/title.edit_status')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('thesis/title.edit_status')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($thesis_status, ['url' => URL::to('admin/thesis_status') . '/' . $thesis_status->id, 'method' => 'put', 'class' => 'form-horizontal']) !!}

                    {{ Form::hidden('id', $thesis_status->id, array('id' => 'status_id')) }}

                    <div class="form-group {{ $errors->first('status_name', 'has-error') }}">

                        <label for="status_name" class="col-sm-2 control-label">
                            @lang('thesis/form.name')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('status_name', null, array('class' => 'form-control', 'placeholder'=>trans('class/form.status_name'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('status_name', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('order_no', 'has-error') }}">
                        <label for="order_no" class="col-sm-2 control-label">
                            Order No
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('order_no', null, array('class' => 'form-control', 'placeholder'=>'1')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('order_no', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <a class="btn btn-danger" href="{{ URL::to('admin/thesis_status') }}">
                                    @lang('button.cancel')
                                </a>
                                <button type="submit" class="btn btn-success">
                                    @lang('button.update')
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>

@stop
