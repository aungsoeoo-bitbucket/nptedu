@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('thesis/title.statuslist')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('thesis/title.statuslist')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#">@lang('thesis/title.status')</a></li>
        <li class="active">@lang('thesis/title.statuslist')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('thesis/title.statuslist')
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/thesis_status/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>@lang('thesis/title.status')</th>
                            <th>Order No</th>
                            <th>@lang('class/table.created_at')</th>
                            <th>@lang('class/table.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($thesis_statuses)>0)
                        @foreach($thesis_statuses as $thesis_status)
                        <tr>
                            <td>{{$thesis_status->status_name}}</td>
                            <td>{{ $thesis_status->order_no }}</td>
                            <td>{{ date('d/m/Y', strtotime($thesis_status->created_at)) }}</td>
                            <td>
                               <a href="{{ URL::to('admin/thesis_status/' . $thesis_status->id . '/edit' ) }}">
                                <span class="glyphicon glyphicon-edit"
                                 title="@lang('class/table.update-class')">
                                     
                                 </span>
                              </a>
                              <a href="{{ route('admin.thesis_status.confirm-delete',$thesis_status->id) }}" data-toggle="modal" data-target="#delete_confirm">
                               <span class="glyphicon glyphicon-trash">
                                        
                                    </span>
                            </a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
});
</script>

<div class="modal fade" id="users_exists" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                @lang('class/message.class_has_course')
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
    $(document).on("click", ".users_exists", function () {

        var group_name = $(this).data('name');
        $(".modal-header h4").text( " Class" );
    });
</script>
@stop
