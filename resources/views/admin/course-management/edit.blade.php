@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
@lang('course-management/title.edit')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
   <style type="text/css">
        .form-horizontal .control-label {
            text-align: left !important;
        }
   </style>

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <!--end of page level css-->
@stop

{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('course-management/title.edit')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li>@lang('class/title.title')</li>
        <li class="active">@lang('course-management/title.edit')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('course-management/title.edit')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($management, ['url' => URL::to('admin/course-management') . '/' . $management->id, 'method' => 'put', 'class' => 'form-horizontal', 'files'=> true]) !!}

                    {{ Form::hidden('id', $management->id, array('id' => 'class_id')) }}

                        <div class="form-group {{ $errors->first('quarter_id', 'has-error') }}">
                            <label for="class_id" class="col-sm-2 control-label">
                                @lang('quarter/form.name')
                            </label>
                            <div class="col-sm-5">
                                <select class="form-control" id="quarter_id" name="quarter_id">
                                    <option value="">Select Class Quarter</option>
                                    @foreach($quarters as $q)
                                      <option value="{{$q->id}}" {{(old('quarter_id',$management->quarter_id)==$q->id)?'selected':''}}>{{$q->quarter_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4">
                                {!! $errors->first('quarter_id', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>


                        <div class="form-group {{ $errors->first('course_id', 'has-error') }}">
                            <label for="course_id" class="col-sm-2 control-label">
                                @lang('course/form.name')
                            </label>
                            <div class="col-sm-5">
                                <select class="form-control" name="course_id" id="course_id">
                                    @foreach($courses as $c)
                                      <option value="{{$c->id}}" {{(old('quarter_id',$management->course_id)==$c->id)?'selected':''}}>{{$c->course_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4">
                                {!! $errors->first('course_id', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->first('title', 'has-error') }}">
                            <label for="title" class="col-sm-2 control-label">
                                @lang('course-management/form.title')
                            </label>
                            <div class="col-sm-5">
                                {!! Form::text('title', null, array('class' => 'form-control', 'placeholder'=>trans('course-management/form.title'))) !!}
                            </div>
                            <div class="col-sm-4">
                                {!! $errors->first('title', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>


                        <div class="form-group {{ $errors->first('description', 'has-error') }}">
                            <label for="description" class="col-sm-2 control-label">
                                @lang('course-management/form.description')
                            </label>
                            <div class="col-sm-5">
                                <textarea name="description" id="description" class="form-control resize_vertical"
                                      rows="4">{!! old('description',$management->description) !!}</textarea>
                            </div>
                            <div class="col-sm-4">
                                {!! $errors->first('description', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>

                    

                        <div class="form-group {{ $errors->first('publish_date', 'has-error') }}">
                            <label for="publish_date" class="col-sm-2 control-label">
                                @lang('course-management/form.publish_date')
                            </label>
                            <div class="col-sm-5">
                                <input id="dob" name="publish_date" type="text" class="form-control"
                                                           data-date-format="YYYY-MM-DD"
                                                           placeholder="yyyy-mm-dd" value="{{ old('publish_date',$management->publish_date) }}" />
                            </div>
                            <div class="col-sm-4">
                                {!! $errors->first('publish_date', '<span class="help-block">:message</span> ') !!}
                            </div>
                        </div>
                        

                        <div class="form-group {{ $errors->first('document', 'has-error') }}">
                            <label for="pic" class="col-sm-2 control-label">@lang('course-management/form.document')</label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 50px; height: 50px;">
                                            <img src="{{ asset('/assets/img/document.png') }}" alt="document">
                                        </div>
                                        {{$management->document}}
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 50px;">
                                            
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new">Select Document</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" id="document" name="document" placeholder="Upload Document file" accept=".doc,.docx,.ppt,.pptx,.pdf, application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="form-control">
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('document', ':message') }}</span>
                                </div>
                        </div>

                        <div class="form-group {{ $errors->first('audio', 'has-error') }}">
                            <label for="pic" class="col-sm-2 control-label">@lang('course-management/form.audio')</label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 50px; height: 50px;">
                                            <img src="{{ asset('/assets/img/audio.png') }}" alt="audio">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 100px;">
                                            
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new">Select audio</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" id="audio" name="audio" placeholder="Upload audio file"  accept="audio/*" class="form-control">
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('audio', ':message') }}</span>
                                </div>
                        </div>

                        <div class="form-group {{ $errors->first('video', 'has-error') }}">
                            <label for="pic" class="col-sm-2 control-label">@lang('course-management/form.video')</label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 50px; height: 50px;">
                                            <img src="{{ asset('/assets/img/video.png') }}" alt="video">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 100px;">
                                            
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new">Select video</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" id="video" name="video" placeholder="Upload video file"  accept="video/*" class="form-control">
                                            </span>
                                            <a href="#" class="btn btn-danger fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('video', ':message') }}</span>
                                </div>
                        </div>
                       
                         
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <a class="btn btn-danger" href="{{ URL::to('admin/course-management') }}">
                                    @lang('button.cancel')
                                </a>
                                <button type="submit" class="btn btn-success">
                                    @lang('button.update')
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>

    <script type="text/javascript" >
        $(document).ready(function(){

            $baseurl = "https://naypyitawmba.com";

            // var quarter_id = $('#quarter_id').val();
            // if (quarter_id!='' || quarter_id!='undefined') {
            //      getcourses(quarter_id);
            // }
        });

        $('#quarter_id').change(function(){
                getcourses($(this).val());
        });

        function getcourses($qid) {
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                $.ajax({
                    url : $baseurl+'course-management/getCoursesByQuarter',
                    dataType : 'html',
                    method : 'post',
                    data : {
                            'quarter_id' : $qid,
                            '_token': $('#ctr_token').val()
                    },
                    success : function(data){
                        $('#course_id').html(data);
                        if(data==""){
                            $('#course_id').html('<option value="">Select Sub Category</option>');
                        }
                    },
                    error : function(error){
                        console.log(error);
                    }
                });
        }
    </script>
@stop
