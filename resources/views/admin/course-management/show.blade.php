@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('course-management/title.course-managementdetail')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" href="{{ asset('assets/css/pages/blog.css') }}" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <!--section starts-->
    <h1>@lang('course-management/title.course-managementdetail')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="14" data-c="#000" data-loop="true"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li> @lang('course-management/title.title')</li>
        <li class="active">@lang('course-management/title.course-managementdetail')</li>
    </ol>
</section>
<!--section ends-->

<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('course-management/title.course-managementdetail')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($course, ['class' => 'form-horizontal', 'files'=> true]) !!}
                   
                    <div class="form-group">
                        <label for="class_id" class="col-sm-2 control-label">
                            @lang('class/form.name')
                        </label>
                        <div class="col-sm-5">
                            <input type="text" value="{{ $course[0]->class_name }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>

                     <div class="form-group ">
                        <label for="category_name" class="col-sm-2 control-label">
                            @lang('year/title.name')
                        </label>
                        <div class="col-sm-5">
                            <input type="text" value="{{ $course[0]->year }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="category_name" class="col-sm-2 control-label">
                            @lang('quarter/form.name')
                        </label>
                        <div class="col-sm-5">
                            <input type="text" value="{{ $course[0]->quarter_name }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="quarter_id" class="col-sm-2 control-label">
                            @lang('course/form.name')
                        </label>
                        <div class="col-sm-5">
                           <input type="text" value="{{ $course[0]->course_name }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>

                     <div class="form-group">
                        <label for="quarter_id" class="col-sm-2 control-label">
                            @lang('course-management/form.publish_date')
                        </label>
                        <div class="col-sm-5">
                           <input type="text" value="{{ $course[0]->publish_date }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>

                   

                    <div class="form-group">
                        <label for="doc" class="col-sm-2 control-label">@lang('course-management/form.document')</label>

                        @if($course[0]->document!='')
                            <div class="col-sm-1">
                                <a href="{{route('viewfile',$course[0]->id)}}" target="_blank"> 
                                <img src="{{ asset('/assets/img/pdf.png') }}" style="width: 50px; height: 50px;" alt="document" class="img-responsive" />
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a href="{{route('downloadfile',$course[0]->id)}}">
                                    <i class="fa fa-download" aria-hidden="true">&nbsp;Download</i>
                                </a>
                            </div>
                        @else
                            <div class="col-sm-10">
                                <p>No document file</p>
                            </div>                           
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="audio" class="col-sm-2 control-label">@lang('course-management/form.audio')</label>
                        @if($course[0]->audio!='')
                            <div class="col-sm-4">
                               <audio controls>
                                      <source src="{{ asset('/uploads/files/'.$course[0]->audio)}}" type="audio/mp3">
                                        Your browser does not support the audio element.
                                </audio>
                            </div>
                        @else
                            <div class="col-sm-10">
                                <p>No audio file</p>
                            </div>                           
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="video" class="col-sm-2 control-label">@lang('course-management/form.video')</label>
                        @if($course[0]->video!='')
                            <div class="col-sm-4">
                               <video width="400" controls>
                                  <source src="{{ asset('/uploads/files/'.$course[0]->video)}}" type="video/mp4">
                                  Your browser does not support HTML5 video.
                                </video>
                            </div>
                        @else
                            <div class="col-sm-10">
                                <p>No Video file</p>
                            </div>                           
                        @endif
                    </div>
                     <div class="form-group">
                        <div class="col-sm-2">
                            
                        </div>
                        <div class="table-responsive col-sm-6">
                            <table class="table table-bordered" id="table">
                                <thead>
                                    <tr class="filters">
                                        <th>Download User</th>
                                        <th>Download Count</th>

                                    </tr>
                                </thead>
                                <tbody>
                                @if(!empty($downloadfiles))
                                    @foreach ($downloadfiles as $download)
                                        <tr>
                                            <td>{{ $download->username }}</td>
                                            <td>{{ $download->download_count }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <div class="form-group">
                              <div class="col-sm-2">
                            
                             </div>
                            <div class="col-sm-4">
                                   {!! $downloadfiles->render() !!}
                            </div>
                             

                        </div>

                     <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/course-management/') }}">
                                @lang('button.back')
                            </a>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>
@stop
