@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('thesis/title.statuslist')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('thesis/title.statuslist')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#">@lang('thesis/title.viva')</a></li>
        <li class="active">@lang('thesis/title.viva-list')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('thesis/title.viva-list')
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/batch_viva/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>Roll NO</th>
                            <th>@lang('thesis/title.std-name')</th>
                            <th>Group</th>
                            <th>@lang('thesis/title.status')</th>
                            <th>@lang('thesis/title.pass-date')</th>
                            <th>@lang('thesis/title.complete-date')</th>
                            <th>@lang('class/table.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- batchVivas -->
                        @if(!empty($batchVivas))
                        @foreach($batchVivas as $batchViva)
                        <tr>
                            <td>
                                {{ $batchViva->student->roll_no}}
                            </td>
                            <td>
                               {{$batchViva->student->username_en}}
                            </td>
                            <td>
                                <!-- {{($batchViva->student->group)?$batchViva->student->group->group_name:''}} -->
                                
                            </td>
                            <td>{{$batchViva->status->status_name}}</td>
                            @if($batchViva->pass_date != null)
                            <td>
                                {{ date('d-m-Y', strtotime($batchViva->pass_date)) }}
                            </td>
                            @else
                            <td></td>
                            @endif

                            @if($batchViva->complete_date != null)
                            <td>
                                {{ date('d-m-Y', strtotime($batchViva->complete_date)) }}
                            </td>
                            @else
                            <td></td>
                            @endif
                            <td>
                                <a href="{{ URL::to('admin/batch_viva/' . $batchViva->id . '/edit' ) }}">
                                        <i class="livicon"
                                             data-name="edit"
                                             data-size="18"
                                             data-loop="true"
                                             data-c="#428BCA"
                                             data-hc="#428BCA"
                                             title="Edit">
                                                 
                                             </i>
                                         </a>
                                    <a href="{{ route('admin.batch_viva.confirm-delete', $batchViva->id) }}" data-toggle="modal"
                                       data-target="#delete_confirm">
                                       <i class="livicon" data-name="remove-alt"
                                            data-size="18" data-loop="true" data-c="#f56954"
                                            data-hc="#f56954"
                                            title="Delete">
                                                
                                            </i>
                                    </a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script> 
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
});
</script>

<div class="modal fade" id="users_exists" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                @lang('class/message.class_has_course')
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
    $(document).on("click", ".users_exists", function () {

        var group_name = $(this).data('name');
        $(".modal-header h4").text( " Class" );
    });
</script>
@stop
