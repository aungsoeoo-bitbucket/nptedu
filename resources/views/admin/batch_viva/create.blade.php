@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    Thesis Group Create :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">


    <style>
        .select2-container{
            width:100% !important;
        }
        /*github repository css*/
        .select2-result-repository__avatar {
            float: left;
            width: 60px;
            margin-right: 10px;
        }
        .select2-result-repository__avatar img {
            width: 100%;
            height: auto;
            border-radius: 2px;
        }
        .select2-result-repository__meta {
            margin-left: 70px;
        }
    </style>
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('thesis/title.create')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>Thesis</li>
        <li class="active">
            @lang('thesis/title.viva')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="doc-portrait" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                       Batch Viva
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => URL::to('admin/batch_viva/'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}

                    <div class="form-group {{ $errors->first('user_id', 'has-error') }}">
                        <label for="user_id" class="col-sm-2 control-label">
                            Students
                        </label>
                        <div class="col-sm-5">

                            <select id="multiSelect" name="user_id[]" class="form-control" width="100%" multiple></select>
                                


                            {{-- <select class="form-control" name="user_id">
                                <option value="">Select Students</option>
                                @foreach($students as $std)
                                  <option value="{{$std->id}}" {{ (old('user_id')==$std->id)?'selected':'' }}>{{$std->username_en}}</option>
                                @endforeach
                            </select> --}}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('user_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('status_id', 'has-error') }}">
                        <label for="status_id" class="col-sm-2 control-label">
                            Thesis Status
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="status_id">
                                <option value="">Select Status</option>
                                @foreach($statuses as $status)
                                  <option value="{{$status->id}}" {{ (old('status_id')==$status->id)?'selected':'' }}>{{$status->status_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('status_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('pass_date', 'has-error') }}">
                        <label for="pass_date" class="col-sm-2 control-label">
                            Pass Date
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('pass_date', null, array('class' => 'form-control', 'id'=>'pass_date' , 'placeholder'=>'Thesis pass date')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('pass_date', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('complete_date', 'has-error') }}">
                        <label for="complete_date" class="col-sm-2 control-label">
                            Complete Date
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('complete_date', null, array('class' => 'form-control', 'id'=>'complete_date' , 'placeholder'=>'Thesis complete date')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('complete_date', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/batch_viva/') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>


    <script>

        $("#pass_date").datetimepicker({
            format: 'DD-MM-YYYY',
            widgetPositioning:{
                vertical:'bottom'
            },
            // keepOpen:false,
            // useCurrent: true,
            // maxDate: moment().add(1,'h').toDate()
        });
        $("#complete_date").datetimepicker({
            format: 'DD-MM-YYYY',
            widgetPositioning:{
                vertical:'bottom'
            },
            // keepOpen:false,
            // useCurrent: true,
            // maxDate: moment().add(1,'h').toDate()
        });
        // http://localhost:8888/nptedu/public/
        console.log($baseurl);
         $('#multiSelect').select2({
            placeholder: "Search Students...",
            ajax: {
                url: $baseurl+'select2-autocomplete-users',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    </script>
@stop
