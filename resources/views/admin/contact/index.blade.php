@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Inbox
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/css/pages/alertmessage.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/mail_box.css') }}" rel="stylesheet" type="text/css" />

    <!-- page level css ends-->
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <h1>Inbox</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/inbox') }}">Email</a>
                    </li>
                    <li class="active">Inbox</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row web-mail">
                    {{-- <div class="col-lg-2 col-md-3 col-sm-4">
                        <div class="whitebg">
                            <ul>
                                <li class="compose">
                                    <a href="{{ URL::to('admin/compose') }}">
                                        <i class="livicon" data-n="pen" data-s="16" data-c="white"></i>
                                        &nbsp; &nbsp;Compose
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="{{ URL::to('admin/contacts') }}">
                                        <i class="livicon" data-n="inbox" data-s="16" data-c="white"></i>
                                        &nbsp; &nbsp;Inbox
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="livicon" data-n="check-circle" data-s="16" data-c="gray"></i>
                                        &nbsp; &nbsp; Sent
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="livicon" data-n="trash" data-s="16" data-c="gray"></i>
                                        &nbsp; &nbsp; Trash
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="livicon" data-n="eye-close" data-s="16" data-c="gray"></i>
                                        &nbsp; &nbsp; Spam
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="livicon" data-n="unlink" data-s="16" data-c="gray"></i>
                                        &nbsp; &nbsp; Draft
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div> --}}
                    <div class="col-lg-10 col-md-9 col-sm-8">
                        <div class="whitebg">
                            <table class="table table-striped table-advance table-hover" id="inbox-check">
                                {{-- <thead>
                                    <tr>
                                        <td colspan="6">
                                            <div class="col-md-8">
                                                <h4>
                                                    <strong>Inbox</strong>
                                                </h4>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="6">
                                            <div class="row no-padding">
                                                <div class="col-md-8 col-lg-9 col-xs-12">
                                                    <div class="btn-group pull-left table-bordered paddingrightleft_10 paddingtopbottom_5px">
                                                        <input type="checkbox" class="square-blue" id="checkall">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                            <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu ul">
                                                            <!-- dropdown menu links -->
                                                            <li>
                                                                <a href="#">All</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">None</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Read</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">UnRead</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Starred</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Unstarred</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="btn-group pull-left table-bordered paddingrightleft_10 paddingtopbottom_5px margin_left">
                                                        <i class="livicon" data-n="refresh" data-s="17"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-lg-3 col-xs-12">
                                                    <div class="pull-right">
                                                        <ul class="pagination no-padding">
                                                            {!! $contacts->render() !!}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                </thead> --}}
                                <tbody>
                                    @if(!empty($contacts))
                                        @foreach ($contacts as $contact)
                                            <tr data-messageid="1" class="unread">
                                                <td style="width:7%;" class="inbox-small-cells">
                                                    <div class="checker">
                                                        <span>
                                                            <a href="{{ route('admin.contacts.confirm-delete', $contact->id) }}" data-toggle="modal"
                                                               data-target="#delete_confirm">
                                                               <i class="livicon" data-name="trash"
                                                                    data-size="18" data-loop="true" data-c="#f56954"
                                                                    data-hc="#f56954">
                                                                        
                                                                </i>
                                                            </a>
                                                        </span>
                                                    </div>
                                                </td>
                                                {{-- <td style="width:2%;" class="inbox-small-cells">
                                                    <i class="livicon" data-n="star-full" data-s="15"></i>
                                                </td> --}}
                                                <td style="width:22%;" class="view-message hidden-xs">
                                                    <a href="{{ URL::to('admin/contacts/' . $contact->id ) }}">
                                                        {{ $contact->name }}</a>
                                                </td>
                                                <td style="width:56%;" class="view-message ">
                                                    <a href="{{ URL::to('admin/contacts/' . $contact->id ) }}">{{ str_limit($contact->message, $limit = 100, $end = '...') }}</a>
                                                </td>
                                           {{--      <td style="width:3%;" class="view-message inbox-small-cells">
                                                    <a href="{{ URL::to('admin/contacts/' . $contact->id ) }}">
                                                        <i class="fa fa-paperclip"></i>
                                                    </a>
                                                </td> --}}
                                                <td style="width:13%;" class="view-message text-right">
                                                    <a href="{{ URL::to('admin/contacts/' . $contact->id ) }}">{{ date('d/m/Y   H:m A', strtotime($contact->created_at)) }}</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <!-- content -->
        
    @stop
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
      </div>
    </div>
{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/js/pages/mail_box.js') }}"></script>

    <script>
    $(function () {
        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
    });
    </script>
@stop
