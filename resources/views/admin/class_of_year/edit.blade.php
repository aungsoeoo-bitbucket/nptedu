@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
@lang('year/title.edit')
@parent
@stop

{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('year/title.edit')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li>@lang('year/title.year')</li>
        <li class="active">@lang('year/title.edit')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('year/title.edit')
                    </h4>
                </div>
                <div class="panel-body">
                    @if($year)
                        {!! Form::model($year, ['url' => URL::to('admin/years/'. $year->id), 'method' => 'put', 'class' => 'form-horizontal']) !!}
                        
                            <!-- CSRF Token -->
                            {{ csrf_field() }}

                            <div class="form-group {{ $errors->
                                first('year', 'has-error') }}">
                                <label for="title" class="col-sm-2 control-label">
                                    @lang('year/form.name')
                                </label>
                                <div class="col-sm-5">
                                    <input type="text" id="year" name="year" class="form-control"
                                           placeholder=@lang('year/form.name') value="{!! old('year',$year->year) !!}">
                                </div>
                                <div class="col-sm-4">
                                    {!! $errors->first('group_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <a class="btn btn-danger" href="{{ route('admin.years.index') }}">
                                    @lang('button.cancel')
                                </a>
                                <button type="submit" class="btn btn-success">
                                    @lang('button.save')
                                </button>
                            </div>
                        </div>
                    </form>
                    @else
                        <h1>@lang('year/message.error.no_role_exists')</h1>
                            <a class="btn btn-danger" href="{{ route('admin.years.index') }}">
                                @lang('button.back')
                            </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>

@stop
