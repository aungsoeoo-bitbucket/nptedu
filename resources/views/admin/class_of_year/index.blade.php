@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
   @lang('year/title.management')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('year/title.years_list')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li><a href="#">@lang('year/title.name')</a></li>
        <li class="active">@lang('year/title.management')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="table" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('year/title.management')
                </h4>
                <div class="pull-right">
                    <a href="{{ URL::to('admin/years/create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body">
                <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>@lang('year/table.name')</th>
                            <th>@lang('year/table.created_at')</th>
                            <th>@lang('year/table.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($years))
                        @foreach ($years as $year)
                            <tr>
                                <td>{{ $year->year }}</td>

                                <td>{{ date('d/m/Y   H:m A', strtotime($year->created_at)) }}</td>
                                <td>
                                    <div class="dropdown">
                                      <button class="btn btn-primary glyphicon glyphicon-cog dropdown-toggle" type="button" data-toggle="dropdown">
                                      <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ URL::to('admin/years/' . $year->id . '/edit' ) }}">
                                                <span class="glyphicon glyphicon-edit"
                                                 title="@lang('class/table.update-class')">
                                                     
                                                 </span>Edit
                                             </a>
                                        </li>
                                        <li>
                                                @if($year->quarter()->count()>0)
                                                    <a href="#" data-toggle="modal" data-target="#users_exists" data-name="{!! $year->year !!}" class="users_exists">
                                                       <span class="glyphicon glyphicon-warning-sign"></span>Delete
                                                    </a>
                                                @else
                                                    <a href="{{ route('admin.year.confirm-delete', $year->id) }}" data-toggle="modal"
                                                   data-target="#delete_confirm">
                                                       <span class="glyphicon glyphicon-trash"></span>Delete
                                                    </a>
                                                @endif
                                            
                                        </li>
                                      </ul>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
});
</script>

<div class="modal fade" id="users_exists" tabindex="-2" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                @lang('year/message.data_exists')
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});
    $(document).on("click", ".users_exists", function () {

        var year = $(this).data('name');
        $(".modal-header h4").text( year );
    });
</script>
@stop
