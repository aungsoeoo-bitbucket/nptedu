@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
 Exam Edit ::@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <!--end of page level css-->

@stop

{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
        Exam Edit
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li>Exam</li>
        <li class="active">Exam Eidt</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                       Exam Edit
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($exam, ['url' => URL::to('admin/exam') . '/' . $exam->id, 'method' => 'put', 'class' => 'form-horizontal', 'files'=> true]) !!}
                   

                    <div class="form-group {{ $errors->first('year_id', 'has-error') }}">
                        <label for="year_id" class="col-sm-2 control-label">
                            @lang('year/form.name')
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="year_id">
                                <option value="">Select Year</option>
                                @foreach($years as $y)
                                  <option value="{{$y->id}}" {{ ($exam->year_id==$y->id) ? 'selected':''}} >{{$y->year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('year_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>


                    <div class="form-group {{ $errors->first('quarter_id', 'has-error') }}">
                        <label for="quarter_id" class="col-sm-2 control-label">
                            @lang('quarter/form.name')
                        </label>
                        <div class="col-sm-5">
                            <select class="form-control" name="quarter_id">
                                <option value="">Select Quarter</option>
                                @foreach($quarters as $q)
                                  <option value="{{$q->id}}" {{ ($exam->quarter_id==$q->id) ? 'selected':''}} >{{$q->quarter_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('quarter_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->first('title', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            Exam Title
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('title', null, array('class' => 'form-control', 'placeholder'=>'Exam Title')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('title', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/exam/') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                Update
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/edituser.js') }}"></script>

</script>
@stop
