@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('course/title.coursedetail')
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" href="{{ asset('assets/css/pages/blog.css') }}" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <!--section starts-->
    <h1>@lang('course/title.coursedetail')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="14" data-c="#000" data-loop="true"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li> @lang('course/title.title')</li>
        <li class="active">@lang('course/title.coursedetail')</li>
    </ol>
</section>
<!--section ends-->

<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('course/title.coursedetail')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($course, ['class' => 'form-horizontal', 'files'=> true]) !!}
                   
                    <div class="form-group">
                        <label for="class_id" class="col-sm-2 control-label">
                            @lang('class/form.name')
                        </label>
                        <div class="col-sm-5">
                            <input type="text" value="{{ $course[0]->class_name }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="class_id" class="col-sm-2 control-label">
                            @lang('year/title.name')
                        </label>
                        <div class="col-sm-5">
                            <input type="text" value="{{ $course[0]->year }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="quarter_id" class="col-sm-2 control-label">
                            @lang('quarter/form.name')
                        </label>
                        <div class="col-sm-5">
                           <input type="text" value="{{ $course[0]->quarter_name }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="course_name" class="col-sm-2 control-label">
                            @lang('course/form.name')
                        </label>
                        <div class="col-sm-5">
                            <input type="text" value="{{ $course[0]->course_name }}" class="form-control" readonly="true"></p>
                        </div>
                        <div class="col-sm-4">
                            
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="pic" class="col-sm-2 control-label">@lang('course/form.photo')</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                                    @if($course[0]->course_photo)
                                        <img src="{!! url('/').'/uploads/course/'.$course[0]->course_photo !!}" style="width: 200px; height: 200px;" alt="img"
                                             class="img-responsive" />
                                    @else
                                        <img src="http://placehold.it/200x200" alt="course photo" 
                                             class="img-responsive"/>
                                    @endif
                                </div>
                                
                            </div>
                           
                        </div>
                        </div>

                         <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <a class="btn btn-danger" href="{{ URL::to('admin/courses/') }}">
                                    @lang('button.back')
                                </a>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop


{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>
@stop
