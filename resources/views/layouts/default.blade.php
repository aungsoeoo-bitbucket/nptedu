<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon ICON-->
    <link rel="icon" href="{{  asset('favicon.ico') }}" type="image/x-icon" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <title>
    	@section('title')
        Home   | Nay Pyi Taw EDU
        @show
    </title>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{  asset('assets/css/lib.css') }}">
     <link rel="stylesheet" type="text/css" href="{{  asset('assets/css/fontstyle.css') }}">
    <!--end of global css-->
    <!--page level css-->
    @yield('header_styles')
    <!--end of page level css-->
</head>

<body>
    <!-- Header Start -->
    <header>
        <!-- Icon Section Start -->
        <div class="icon-section">
            <div class="container">
                <ul class="list-inline">
                   {{--  <li>
                        <a href="#"> <i class="livicon" data-name="facebook" data-size="18" data-loop="true" data-c="#fff" data-hc="#757b87"></i>
                        </a>
                    </li> --}}
                    {{-- <li>
                        <a href="#"> <i class="livicon" data-name="twitter" data-size="18" data-loop="true" data-c="#fff" data-hc="#757b87"></i>
                        </a>
                    </li>

                    <li>
                        <a href="#"> <i class="livicon" data-name="linkedin" data-size="18" data-loop="true" data-c="#fff" data-hc="#757b87"></i>
                        </a>
                    </li> --}}
                    <li class="pull-right">
                        <ul class="list-inline icon-position">
                            <li>
                                <a href="mailto:"><i class="livicon" data-name="mail" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff"></i></a>
                                <label class="hidden-xs"><a href="mailto:" class="text-white">nptmba@gmail.com</a></label>
                            </li>
                           {{--  <li>
                                <a href="tel:"><i class="livicon" data-name="phone" data-size="18" data-loop="true" data-c="#fff" data-hc="#fff"></i></a>
                                <label class="hidden-xs"><a href="tel:" class="text-white">(95) 09 123456789</a></label>
                            </li> --}}
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- //Icon Section End -->
        <!-- Nav bar Start -->
        <nav class="navbar navbar-default container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                    <span><a href="#"><i class="livicon" data-name="responsive-menu" data-size="25" data-loop="true" data-c="#757b87" data-hc="#ccc"></i>
                    </a></span>
                </button>
                <a class="navbar-brand" href="{{ route('home') }}"><img src="{{  asset('assets/images/logo.png') }}" alt="logo" class="logo_position" style="height: 50px;">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li {!! (Request::is('/') ? 'class="active"' : '') !!}><a href="{{ route('home') }}"> Home</a>
                    </li>

                  {{--   <li {!! (Request::is('/mba') ? 'class="active"' : '') !!}><a href="{{ URL::to('class/mba') }}"> MBA</a>
                    </li> --}}
                    @if(Sentinel::check())
                        @if(Sentinel::inRole('admin') || Sentinel::inRole('teacher'))
                            <li class="dropdown {!! (Request::is('class/mba')  ? 'active' : '') !!}">
                                <a href="{{ URL::to('class/mba') }}" class="dropdown-toggle" data-toggle="dropdown"> MBA   <i class="fa fa-angle-down" ></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($batchs as $batch)
                                        @if(Sentinel::getUser()->acadamic_year_id==$batch->id)
                                            <li>
                                                <a href="{{ URL::to('class/mba?batch='.$batch->id) }}">{{ $batch->acadamic_year}}</a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="{{ URL::to('class/mba?batch='.$batch->id) }}">{{ $batch->acadamic_year}}</a>
                                            </li>
                                        @endif
                                    @endforeach


                                </ul>
                            </li>

                            <li class="dropdown {!! (Request::is('courses')  ? 'active' : '') !!}">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Course <i class="fa fa-angle-down" ></i></a>
                                <ul class="dropdown-menu" role="menu">
                                   @foreach($batchs as $batch)
                                        @if(Sentinel::getUser()->acadamic_year_id==$batch->id)
                                        <li>
                                            <a href="{{ URL::to('courses?batch='.$batch->id) }}">{{ $batch->acadamic_year}}</a>
                                        </li>
                                        @else
                                        <li>
                                            <a href="{{ URL::to('courses?batch='.$batch->id) }}">{{ $batch->acadamic_year}}</a>
                                        </li>
                                        @endif
                                    @endforeach

                                </ul>
                            </li>
                        @else
                            <li class="dropdown {!! (Request::is('class/mba')  ? 'active' : '') !!}">
                                <a href="{{ URL::to('class/mba') }}" class="dropdown-toggle" data-toggle="dropdown"> MBA   <i class="fa fa-angle-down" ></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($batchs as $batch)
                                        @if(Sentinel::getUser()->acadamic_year_id==$batch->id)
                                            <li>
                                                <a href="{{ URL::to('class/mba?batch='.$batch->id) }}">{{ $batch->acadamic_year}}</a>
                                            </li>
                                        @endif
                                    @endforeach


                                </ul>
                            </li>

                            <li class="dropdown {!! (Request::is('courses')  ? 'active' : '') !!}">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Course <i class="fa fa-angle-down" ></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($batchs as $batch)
                                        @if(Sentinel::getUser()->acadamic_year_id==$batch->id)
                                            <li>
                                                <a href="{{ URL::to('courses?batch='.$batch->id) }}">{{ $batch->acadamic_year}}</a>
                                            </li>
                                        @endif
                                    @endforeach


                                </ul>
                            </li>
                           
                        @endif
                    @else
                         <li class="dropdown {!! (Request::is('class/mba')  ? 'active' : '') !!}">
                                <a href="{{ URL::to('class/mba') }}" class="dropdown-toggle" data-toggle="dropdown"> MBA   <i class="fa fa-angle-down" ></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($batchs as $batch)
                                            <li>
                                                <a href="{{ URL::to('class/mba?batch='.$batch->id) }}">{{ $batch->acadamic_year}}</a>
                                            </li>
                                    @endforeach


                                </ul>
                            </li>

                            <li class="dropdown {!! (Request::is('courses')  ? 'active' : '') !!}">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Course <i class="fa fa-angle-down" ></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($batchs as $batch)
                                        
                                            <li>
                                                <a href="{{ URL::to('courses?batch='.$batch->id) }}">{{ $batch->acadamic_year}}</a>
                                            </li>

                                    @endforeach


                                </ul>
                            </li>

                    @endif

                   
                    <li {!! (Request::is('professors') ? 'class="active"' : '') !!}>
                        <a href="{{ URL::to('professors') }}"> Professors</a>
                    </li>
                    

                    {{-- <li class="dropdown {!! (Request::is('news') || Request::is('news-item/*') || Request::is('events/calender') || Request::is('event/detail/*')  ? 'active' : '') !!}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Post   <i class="fa fa-angle-down" ></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ URL::to('news') }}">News</a>
                            </li>
                            <li><a href="{{ URL::to('events/calender') }}">Events</a>
                            </li>
                        </ul>
                    </li> --}}
                    <li {!! (Request::is('news')  ? 'class="active"' : '') !!}>
                        <a href="{{ URL::to('news') }}"> News</a>
                    </li>

                    <li {!! (Request::is('free-download')  ? 'class="active"' : '') !!}>
                        <a href="{{ URL::to('free-download') }}"> Download</a>
                    </li>

                    <li class="dropdown {!! (Request::is('thesis-title')  ? 'active' : '') !!}">
                        <a href="{{ URL::to('thesis-title') }}" class="dropdown-toggle" data-toggle="dropdown"> Thesis <i class="fa fa-angle-down" ></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ URL::to('thesis-title') }}">Thesis Group</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('status') }}">Thesis Status</a>
                            </li>

                        </ul>
                    </li>

                    <li class="dropdown {!! (Request::is('aboutus') || Request::is('contact')  ? 'active' : '') !!}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> About   <i class="fa fa-angle-down" ></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ URL::to('aboutus') }}">About</a>
                            </li>
                            <li><a href="{{ URL::to('contact') }}">Contact</a>
                            </li>
                        </ul>
                    </li>
                    
                
                    {{--based on anyone login or not display menu items--}}
                    @if(Sentinel::guest())
                        <li><a href="{{ URL::to('login') }}">Login</a>
                        </li>
                        <li><a href="{{ URL::to('register') }}">Register</a>
                        </li>
                    @else

                         <li {!! (Request::is('aboutus') ? 'class="active"' : '') !!}>
                            <a href="{{ URL::to('feedback') }}">Feedback </a>
                        </li>
                        <li class="dropdown {!! (Request::is('my-account')  ? 'active' : '') !!}">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                                {{ Sentinel::getuser()->username }}   <i class="fa fa-angle-down" ></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ URL::to('my-account') }}"><i class="livicon icon3" data-name="user" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> My Account</a>
                                </li>
                                @if(Sentinel::inRole('admin'))
                                <li>
                                    <a href="{{ URL::to('admin') }}"><i class="livicon icon3" data-name="dashboard" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> Backend</a>
                                </li>
                                @endif
                                <li>
                                    <a href="{{ URL::to('logout') }}"> <i class="livicon icon3" data-name="sign-out" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>Logout</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </nav>
        <!-- Nav bar End -->
    </header>
    <!-- //Header End -->
    
    <!-- slider / breadcrumbs section -->
    @yield('top')

    <!-- Content -->
    @yield('content')

    <!-- Footer Section Start -->
   {{--  <footer>
        <div class="container footer-text">
            <div class="col-sm-4">
                <h4>About Us</h4>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </p>                
            </div>
            <div class="col-sm-4">
                <h4>Contact Us</h4>
                <ul class="list-unstyled">
                    <li><i class="livicon icon4 icon3" data-name="location" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>GSAD, Yarzathingaha Street, Ottarathiri Township, National Archives Department, Nay Pyi Taw</li>
                    <li><i class="livicon icon4 icon3" data-name="cellphone" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>Phone:-----</li>
                    <li><i class="livicon icon4 icon3" data-name="printer" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i> Fax:----</li>
                    <li><i class="livicon icon3" data-name="mail-alt" data-size="20" data-loop="true" data-c="#ccc" data-hc="#ccc"></i> Email:<span class="text-success" style="cursor: pointer;">
                        info@naypyitawedu.com</span>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4">
               <h4 class="menu">Follow Us</h4>
                <ul class="list-inline">
                    <li>
                        <a href="#"> <i class="livicon" data-name="facebook" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#"> <i class="livicon" data-name="twitter" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#"> <i class="livicon" data-name="linkedin" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </footer> --}}
    <!-- //Footer Section End -->
    <div class="copyright">
        <div class="container">
        <p>Copyright &copy; naypyitawmba.com, 2019</p>
        </div>
    </div>
   {{--  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a> --}}
    <!--global js starts-->
    <script type="text/javascript" src="{{  asset('assets/js/frontend/lib.js') }}"></script>
    <!--global js end-->
    <!-- begin page level js -->
    @yield('footer_scripts')
    <!-- end page level js -->
</body>

</html>
