
@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            naypyitawedu.com
        @endcomponent
    @endslot

    {{-- Body --}}
# Hello  {!! $user->user_name !!},<br>

Welcome to naypyitawedu.com! Please click on the following link to confirm your naypyitawedu.com account:<br />
@component('mail::button', ['url' =>  $user->activationUrl  ])
    Activate Account
@endcomponent


    Thanks,

    {{-- Footer --}}
    @slot('footer')
    @component('mail::footer')
    &copy; 2018 All Copy right received
@endcomponent
@endslot
@endcomponent