@extends('layouts/default')

{{-- Page title --}}
@section('title')
News | Nay Pyi Taw EDU
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/news.css') }}">
<link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/blog.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
<div class="breadcum"></div>
<br>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <div class="row">
            <!-- Jelly-o sesame Section Strat -->
            <div class="col-sm-7 col-md-8" >
                <div class="col-md-12">
                    <div class="news_item_image thumbnail">
                        <label>
                            <a href="{{ URL::to('news_item') }}">
                                <h3 class="primary news_headings">
                            {{$blog->title}}</h3>
                            </a>
                        </label>
                        @if($blog->image!='')
                        <img src="{{ URL::to('/uploads/blog/'.$blog->image)  }}" alt="image" class="img-responsive img_b2">
                        @endif
                        <div class="news_item_text_1">
                            <p>
                               {!! $blog->content !!}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                </div>
            </div>
            <div class="col-sm-5 col-md-4 col-full-width-left">
                <!-- Featured Author Section Start -->
                {{-- <div class="the-box  no-margin more-padding martop3 wow slideInDown" data-wow-duration="3.5s">
                    <h3>Featured Authors</h3>
                    <br>
                    <div class="row">
                        <div class="col-xs-3">
                            <p>
                                <a href="#">
                                    <img src="{{ secure_asset('assets/images/authors/avatar.jpg') }}" class="img-responsive img-circle" alt="riot">
                                </a>
                            </p>
                        </div>

                        <div class="col-xs-3">
                            <p>
                                <a href="#">
                                    <img src="{{ secure_asset('assets/images/authors/avatar1.jpg') }}" class="img-responsive img-circle" alt="riot">
                                </a>
                            </p>
                        </div>

                        <div class="col-xs-3">
                            <p>
                                <a href="#">
                                    <img src="{{ secure_asset('assets/images/authors/avatar2.jpg') }}" class="img-responsive img-circle" alt="riot">
                                </a>
                            </p>
                        </div>
                        <div class="col-xs-3">
                            <p>
                                <a href="#">
                                    <img src="{{ secure_asset('assets/images/authors/avatar3.jpg') }}" class="img-responsive img-circle" alt="riot">
                                </a>
                            </p>
                        </div>
       
                    </div>
 
                    <button class="btn btn-success btn-block">Browse all author</button>
                </div> --}}
                <!-- //Featured Author Section End -->
                <!-- /.the-box .bg-primary .no-border .text-center .no-margin -->
                <!-- Recent Post Section Start -->
                <div class="the-box">
                    <h3 class="small-heading more-margin-bottom">Recent News</h3>
                    <hr>
                    <ul class="media-list">
                        @foreach($latests as $latest)
                        <li class="media">
                            <div class="media-body">
                                <div class="media-heading">
                                    <a href="{{ URL::to('news-item/'.$latest->slug) }}">
                                        <h4 class="primary news_headings"> {{ $latest->title }} </h4>
                                    </a>
                                    <h6 class="text-danger">{{ $latest->created_at}}</h6>
                                </div>
                            </div>
                        </li>
                        @endforeach
                       
                        
                    </ul>
                </div>

            </div>
            <!-- //Jelly-o sesame Section End -->
        </div>
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ secure_asset('assets/vendors/wow/js/wow.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            new WOW().init();
        });
    </script>

@stop
