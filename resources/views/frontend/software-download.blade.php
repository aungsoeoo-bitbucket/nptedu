@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Software  Download | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/news.css') }}">
    <link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/timeline.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('assets/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" />

    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.transitions.css') }}" rel="stylesheet" type="text/css">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
   <div class="breadcum"></div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container news mt-4">
        <div class="row">
            <div class="col-md-12">
                <h4>
                    <p class="text-primary">IBM SPSS Statistics V23</p>
                </h4>
                <p>Important Note: SPSS does not work with Windows XP Home Edition. It requires Windows XP Professional, Windows 7 Enterprise and later windows verisons. <br>
                IBM SPSS Statistics is the ultimate tool for managing your statistics data and research.</p>
            </div>
        </div>
        <br>

         <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="card-columns">
                      <div class="card ">
                        <div class="card-body text-center">
                          <p class="card-text text-center">For window 32 bit</p>
                          <img src="{{ asset('assets/images/32.png') }}" alt="32bit" width="50%"><br><br>
                           <a href="https://drive.google.com/uc?id=1oJbKWwsnfTdaN0abfWeG4aEEqPk40ApD&export=download" download target="_blank" > <img src="{{ asset('assets/images/download.png') }}" alt="32bit" width="50%"></a>
                        </div>

                      </div>
                </div>
            </div>
             <div class="col-md-3">
                <div class="card">
                    <div class="card-body text-center">
                      <p class="card-text text-center">For Window 64 Bit</p>
                      <img src="{{ asset('assets/images/64.png') }}" alt="64bit" width="50%"><br><br>
                      <a href="https://drive.google.com/a/linncomputer.com/uc?id=1fSjMytr2u4ynJhmyDMeBvyCPWw_YNySJ&export=download" download target="_blank"> <img src="{{ asset('assets/images/download.png') }}" alt="32bit" width="50%"></a>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-3"></div>
           
        </div>
        <div class="row">
            <div class="space">
                &nbsp;&nbsp;<br><br><br><br><br>
            </div>
        </div>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

@stop
