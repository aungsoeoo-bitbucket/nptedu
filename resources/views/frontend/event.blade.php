@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Events | Nay Pyi Taw EDU
@stop
<?php
    $arr1 = []; 
    $arr0 = [];
    foreach($post as $p){
        $darr = explode("-", $p->publish_date);
        if(sizeof($darr)==3){
            if(checkdate($darr[1],$darr[2],$darr[0])){
                $a = ['title'=>$p->title,'url'=>'https://naypyitawmba.com/event/detail/'.$p->id, 'start'=>$p->publish_date];
                array_push($arr1, $a);
                array_push($arr0, $p->publish_date);
            }
        }
    }
    $date = date('Ymd')."";
?>  
    <link href="{{ secure_asset('assets/fullcalendar/core/main.css') }}" rel='stylesheet' />
    <link href="{{ secure_asset('assets/fullcalendar/daygrid/main.css') }}" rel='stylesheet' />

    <script src="{{ secure_asset('assets/fullcalendar/core/main.js') }}"></script>
    <script src="{{ secure_asset('assets/fullcalendar/interaction/main.js') }}"></script>
    <script src="{{ secure_asset('assets/fullcalendar/daygrid/main.js') }}"></script>
<script>
 var events = <?php echo json_encode($arr0); ?>;
  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      defaultDate: '2019-04-12',
      navLinks: true,
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: <?php echo json_encode($arr1); ?>
    });

    calendar.render();
  });

</script>
{{-- page level styles --}}
@section('header_styles')

    <style>

        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }
        .fc-title{
            color: #ffffff !important;
        }
    </style>
@stop

{{-- breadcrumb --}}
@section('top')
   <div class="breadcum"></div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <br>
    <center><h4 style="color: #418bca;"> Our Events </h4></center>
    <br>
    <div id='calendar'></div>
    <br>
    <!-- Container End -->

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->

 
    <!-- end of page level js -->

@stop
