@extends('layouts/default')

{{-- Page title --}}
@section('title')
MBA Class | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    {{-- <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/shopping.css') }}"> --}}
    <link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick-theme.css') }}">
    
    <style type="text/css" media="screen">
        .search_div{
            margin: 10px;
        }

        * {
          box-sizing: border-box;
        }

        .slider {
            width: 90%;
            margin: 30px auto;
        }

        .slick-slide {
          margin: 0px 20px;
        }

        .slick-slide img {
          width: 100%;
        }

        .slick-prev:before,
        .slick-next:before {
          color: black;
        }


        .slick-slide {
          transition: all ease-in-out .3s;
          opacity: .2;
        }

        .slick-active {
          opacity: 1;
        }

        .slick-current {
          opacity: 1;
        }
        h4.text-primary, h5.text-primary{
            text-align: center;
        }

    </style>
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum"></div><br>
@stop
<?php 
    $keyword=(isset($_GET['keyword'])?$_GET['keyword']:'');
    $year_id=(isset($_GET['year_id'])?$_GET['year_id']:'');
    $group_id=(isset($_GET['group_id'])?$_GET['group_id']:'');


?>

{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <!--Content Section Start -->
        <div class="row">
           {!! Form::open(array('url' => URL::to('/class/mba'), 'method' => 'get', 'class' => 'form-horizontal', 'files'=> true)) !!}
{{-- 
                <div class="col-md-2 search_div">
                    <div class="form-group">
                        <select class="form-control" name="year_id">
                            <option value="">Select Year</option>
                            @foreach($years as $y)
                              <option value="{{$y->id}}" {{ (old('year_id',$year_id)==$y->id)?'selected':'' }}>{{$y->year}}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}

                <div class="col-md-2 search_div ">
                    <div class="form-group">
                        <select class="form-control" name="group_id">
                            <option value="">Select Group</option>
                            @foreach($group_select as $g)
                              <option value="{{$g->id}}" {{ (old('group_id',$group_id)==$g->id)?'selected':'' }}>{{$g->group_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2 search_div">
                     <div class="form-group">
                            <input type="text" name="keyword" value="{{ old('keyword',$keyword) }}" id="keyword" class="form-control" placeholder="Enter Roll Number" >
                    </div>
                </div>

                <div class="col-md-2 search_div">
                    <button type="submit" class="btn btn-primary">
                        Search
                    </button>
                </div>
            {!! Form::close() !!}
        </div>
         @foreach($groups as $group)
            @if($group->students->count()>0)
                <h3 class="text-primary">{{ $group->group_name }}</h3>

                <section class="regular slider">
                    @foreach($group->students as $student)
                    <div>
                        @if(Sentinel::check())
                        <a href="{{ URL::to('users/detail/'.$student->id) }}">
                            @if($student->pic!='')
                                <img  src="{!! url('/').'/uploads/users/'.$student->pic !!}" alt="profile pic">
                            @elseif($student->gender === "male")
                                <img  src="{{ asset('assets/images/authors/avatar3.png') }}"
                                     alt="..." />
                            @elseif($student->gender === "female")
                                <img  src="{{ asset('assets/images/authors/avatar5.png') }}"
                                     alt="..." />
                            @else
                                <img  src="{{ asset('assets/images/authors/no_avatar.jpg') }}"
                                     alt="..." />
                            @endif
                        </a>
                        @else

                            @if($student->pic!='')
                                <img  src="{!! url('/').'/uploads/users/'.$student->pic !!}" alt="profile pic">
                            @elseif($student->gender === "male")
                                <img  src="{{ asset('assets/images/authors/avatar3.png') }}"
                                     alt="..." />
                            @elseif($student->gender === "female")
                                <img  src="{{ asset('assets/images/authors/avatar5.png') }}"
                                     alt="..." />
                            @else
                                <img  src="{{ asset('assets/images/authors/no_avatar.jpg') }}"
                                     alt="..." />
                            @endif

                        @endif
                            <h4 class="text-primary mt-4 mb-2">{{ $student->roll_no }}</h4>
                            @if($student->username_en!='')
                                <h5 class="text-primary mt-4 mb-2">{{ strtoupper($student->username_en) }}</h5>
                            @else
                                <h5 class="text-primary mt-4 mb-2">{{ $student->username }}</h5>
                            @endif    
                        @if(Sentinel::check())
                            <a href="{{ URL::to('users/detail/'.$student->id) }}" class="btn btn-primary btn-block text-white">View</a>
                        @endif
                    </div>
                    @endforeach
                </section>
                <hr>
            @endif
        @endforeach
        <!-- Best Deal Section Start -->
       {{--  @foreach($groups as $group)
            <h3 class="text-primary">{{ $group->group_name }}</h3>
            <hr>
            <div class="row">
                @foreach($group->students as $student)
                    <div class="col-sm-2" >
                        <div class="thumbnail text-center">
                            <div class="img-file" align="center" >
                                @if(Sentinel::check())
                                <a href="{{ URL::to('users/detail/'.$student->id) }}">
                                    @if($student->pic!='')
                                        <img style="border-radius: 50%; width: 50%;" src="{!! url('/').'/uploads/users/'.$student->pic !!}" alt="profile pic" class="img-max">
                                    @elseif($student->gender === "male")
                                        <img style="border-radius: 50%; width: 50%;" src="{{ asset('assets/images/authors/avatar3.png') }}"
                                             alt="..."
                                             class="img-responsive"/>
                                    @elseif($student->gender === "female")
                                        <img style="border-radius: 50%; width: 50%;" src="{{ asset('assets/images/authors/avatar5.png') }}"
                                             alt="..."
                                             class="img-responsive"/>
                                    @else
                                        <img style="border-radius: 50%; width: 50%;" src="{{ asset('assets/images/authors/no_avatar.jpg') }}"
                                             alt="..."
                                             class="img-responsive"/>
                                    @endif
                                </a>
                                @else

                                    @if($student->pic!='')
                                        <img style="border-radius: 50%; width: 50%;" src="{!! url('/').'/uploads/users/'.$student->pic !!}" alt="profile pic" class="img-max">
                                    @elseif($student->gender === "male")
                                        <img style="border-radius: 50%; width: 50%;" src="{{ asset('assets/images/authors/avatar3.png') }}"
                                             alt="..."
                                             class="img-responsive"/>
                                    @elseif($student->gender === "female")
                                        <img style="border-radius: 50%; width: 50%;" src="{{ asset('assets/images/authors/avatar5.png') }}"
                                             alt="..."
                                             class="img-responsive"/>
                                    @else
                                        <img style="border-radius: 50%; width: 50%;" src="{{ asset('assets/images/authors/no_avatar.jpg') }}"
                                             alt="..."
                                             class="img-responsive"/>
                                    @endif

                                @endif
                            </div>
                            <br/>
                            <h4 class="text-primary mt-4 mb-2">{{ $student->roll_no }}</h4>
                            <h5 class="text-primary mt-4 mb-2">{{ $student->username }}</h5>
     
                            @if(Sentinel::check())
                            <a href="{{ URL::to('users/detail/'.$student->id) }}" class="btn btn-primary btn-block text-white">View</a>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach --}}
         <div>
          

        </div>

        <!-- //Best Deal Section End -->
        <!-- //Content Section End -->
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/wow/js/wow.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('slick/slick.min.js') }} " type="text/javascript" charset="utf-8"></script>
      <script type="text/javascript">
        $(document).on('ready', function() {
          
          $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 5
          });

          
        });
    </script>
    <script>
        jQuery(document).ready(function () {
            changedesign();
            $( window ).resize(function() {
                changedesign();
            });
          
        });

        function changedesign(){
            if ($(window).width() < 540) { 
                $(".regular").slick({
                    dots: true,
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                  });
            }else{
                $(".regular").slick({
                    dots: true,
                    infinite: true,
                    slidesToShow: 5,
                    slidesToScroll: 5
                  });
            }
        }
    </script>
@stop
