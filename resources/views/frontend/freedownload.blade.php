@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Free Download | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/news.css') }}">
    <link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/timeline.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('assets/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" />

    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.transitions.css') }}" rel="stylesheet" type="text/css">


    <style>
        .news-body .media {
            padding-bottom: 10px;
            border-bottom: 1px solid #ececec;
        }

        .news-body .media-object {
            height: 95px;
            width: 95px;
        }

        .lifestyle img {
            height: 150px;
            width: 100%;
        }



        .newsticker {
            height: 350px !important;
        }

        #carousel img {
            height: 150px;
            width: 100%;
        }

        .owl-pagination {
            display: none;
        }

        .slider-content {
            height: 65px;
            overflow: hidden;
        }

        .height_180 {
            height: 180px;
            width: 100%;
        }

        .sports-content {
            height: 120px;
            overflow: hidden;
        }

        .sports-height {
            height: 200px;
            overflow: hidden;
        }

        .mt-20 {
            margin-top: 20px;
        }

        p a {
            color: #418bca !important;
        }

        .tabbable-line > .nav-tabs > li{
            background-color: transparent !important;
        }

       .news .tabbable-line > .nav-tabs .nav-link.active {
            border-bottom: 4px solid #f3565d !important;
        }

    </style>
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
   <div class="breadcum"></div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container news mt-4">
        <div class="row news">
            <div class="col-md-12">
                <h4>
                    <p class="text-primary">Free Download files</p>
                </h4>
                <hr>
                <div class="row">
                    @if( $files->count() != 0)
                        <div class="col-sm-12 news-body">
                            @foreach($files as $file)
                                <div class="media my-2">
                                    <div class="media-left col-md-8">
                                        <a >
                                            <h5 class="media-heading ">{{ $file->title_en }}</h5>
                                        </a>
                                        <p> {!! $file->description_en !!}</p>
                                       
                                    </div>
                                    <div class="media-body" class="col-md-4">

                                            <a href="{{route('downloadfile',$file->id)}}">
                                                <i class="fa fa-download" aria-hidden="true">&nbsp;Download</i>
                                            </a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            {{-- <span class="text-danger">
                                            {!! date('d-m-Y', strtotime($file->created_at)) !!}
                                            </span> --}}
                                            @if($file->youtube_url!='')
                                            <a href="{{ $file->youtube_url }}" target="_blank">
                                                <i class="fa fa-link" aria-hidden="true">&nbsp;Youtube Link</i>
                                            </a>
                                            @endif

                                      
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    @else
                       <p style="text-align: center">No download file found.</p> 
                    @endif
                   
                </div>
               {!! $files->render() !!}

            </div>

            <!-- Tab-content End -->
        </div>
        <!-- //Tabbablw-line End -->
    </div>
    <!-- Tabbable_panel End -->

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <!--tags-->
    <script src="{{ secure_asset('assets/vendors/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ secure_asset('assets/vendors/jquery_newsTicker/js/jquery.newsTicker.js') }}" type="text/javascript"></script>
    <script src="{{ secure_asset('assets/vendors/owl_carousel/js/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script>

        $('.newsticker').newsTicker({
            direction: 'down',
            row_height: 85,
            max_rows: 3,
            duration: 2000
        });
        var owl = $('#carousel');
        owl.owlCarousel({
            autoPlay: 2000, //Set AutoPlay to 3 seconds
            items: 1,
            loop: true,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],

        });


    </script>
    <!-- end of page level js -->

@stop
