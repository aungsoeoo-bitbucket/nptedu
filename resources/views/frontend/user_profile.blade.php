@extends('layouts/default')

{{-- Page title --}}
@section('title')
    User Profile | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/bootstrap-magnify/bootstrap-magnify.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/pages/user_profile.css') }}" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <link type="text/css" media="screen"  rel="stylesheet"  href="{{ asset('assets/css/croppie.css')}}">

    
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum"></div><br>
@stop

{{-- Page content --}}
@section('content')
    <div class="container">
        <div  class="row ">
            <div class="col-md-12">
                <div class="row ">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="text-center">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 50%">
                                        @if($user->pic)
                                            <img src="{!! url('/').'/uploads/users/'.$user->pic !!}" data-src="holder.js/366x218/#fff:#000" class="img-responsive" alt="img"
                                                     class="img-responsive"/>
                                        @elseif($user->gender === "male")
                                            <img src="{{ asset('assets/images/authors/avatar3.png') }}" data-src="holder.js/366x218/#fff:#000" alt="..."
                                                 class="img-responsive"/>
                                        @elseif($user->gender === "female")
                                            <img src="{{ asset('assets/images/authors/avatar5.png') }}" data-src="holder.js/366x218/#fff:#000" alt="..."
                                                 class="img-responsive"/>
                                        @else
                                            <img src="{{ asset('assets/images/authors/no_avatar.jpg') }}" data-src="holder.js/366x218/#fff:#000" alt="..."
                                                 class="img-responsive"/>
                                        @endif
                                    </div>
                                   {{--  <div class="fileinput-preview fileinput-exists thumbnail" ></div>
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new">
                                                Select image
                                            </span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="..."></span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table  table-striped" id="users">
                                 <tr>
                                    <td>Roll Number</td>
                                    <td>
                                   {{--      <a href="#" data-pk="1" class="editable" data-title="Edit User Name">{{ $user->username}}</a> --}}
                                        <a >{{ $user->roll_no}}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Name(MM)</td>
                                    <td>
                                        <a >{{ $user->username}}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Name(Eng)</td>
                                    <td>
                                        <a >{{ $user->username_en}}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>
                                        <a >
                                            {{ $user->email }}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Date of Birth</td>
                                    <td>
                                        <a >
                                            {{ $user->dob }}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Designation
                                    </td>
                                    <td>
                                        <a >
                                            {{$user->designation}}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Department</td>
                                    <td>
                                        <a >
                                            {{ $user->department }}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Ministry/Company
                                    </td>
                                    <td>
                                        <a >
                                            {{$user->ministry_company}}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Phone Number
                                    </td>
                                    <td>
                                        <a >
                                            {{$user->phone}}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>
                                        <a >
                                            {{ $user->address }}
                                        </a>
                                    </td>
                                </tr>
                               {{--  <tr>
                                    <td>Status</td>
                                    <td>
                                        <a href="#" id="status" data-type="select" data-pk="1" data-value="1" data-title="Status"></a>
                                    </td>
                                </tr> --}}
                            </table>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <!-- Notifications -->
                        <div id="notific">
                            @include('admin.layouts.notification')
                        </div>
                        <ul class="nav nav-tabs ul-edit responsive">
                            <li class="active">
                                <a href="#tab-result" data-toggle="tab">
                                   Exam Result
                                </a>
                            </li>
                            

                            <li >
                                <a href="#tab-editprofile" data-toggle="tab">
                                    Edit Profile
                                </a>
                            </li>
                            <li>
                                <a href="#tab-change-pwd" data-toggle="tab">
                                    Change Password
                                </a>
                            </li>

                           
                        </ul>
                        <div class="tab-content">
                            <div id="tab-result" class="tab-pane fade in active">
                                <div class="activity">
                                     <?php 
                                        $keyword=(isset($_GET['keyword'])?$_GET['keyword']:'');
                                        $quarter_id=(isset($_GET['quarter_id'])?$_GET['quarter_id']:'');
                                        $course_id=(isset($_GET['course_id'])?$_GET['course_id']:'');
                                        $grade=(isset($_GET['grade'])?$_GET['grade']:'');

                                    ?>

                               {!! Form::open(array('url' => URL::to('my-account'), 'method' => 'get', 'class' => 'form-horizontal', 'files'=> true)) !!}

                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <select class="form-control" id="quarter_id" name="quarter_id">
                                                <option value="">Select Quarter</option>
                                                @foreach($quarters as $q)
                                                  <option value="{{$q->id}}" {{ (old('quarter_id',$quarter_id)==$q->id)?'selected':'' }}>{{$q->quarter_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-3 ">
                                         <div class="form-group">
                                             <div class="col-sm-12">
                                                 <select class="form-control" name="course_id" id="course_id">
                                                
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 ">
                                         <div class="form-group">
                                             <div class="col-sm-8">
                                                <input type="text" name="grade" value="{{ old('grade',$grade) }}" id="grade" class="form-control" placeholder="Enter Grade" >
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                 <input type="hidden" id="ctr_token" value="{{ csrf_token()}}">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <a class="btn btn-warning" href="{{ route('export') }}">Download Result xlsx</a>
                                    </div>
                                </div>
                                <br>

                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Quarter</th>
                                                    <th>Course</th>
                                                    <th>Code</th>
                                                    <th>Result</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($results))
                                                    @foreach ($results as $result)
                                                        <tr>
                                                            <td>{{ $result->quarter_name }}</td>
                                                            <td>{{ $result->course_name}} </td>
                                                            <td>{{ $result->course_code}} </td>
                                                            @if($result->grade=='B-')
                                                                <td style="background-color:#FFFF00">
                                                                {{ $result->grade}}
                                                                </td>
                                                            @elseif($result->grade=='C')
                                                                <td style="background-color:#2a6dcc; color: #fff">
                                                                {{ $result->grade}}
                                                                </td>
                                                            @else
                                                                <td >
                                                                {{ $result->grade}}
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-change-pwd" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12 pd-top">
                                        <form  class="form-horizontal">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="form-group">

                                                    <label  class="col-md-3 control-label">
                                                        Password

                                                    </label>
                                                    <div class="col-md-9">

                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                            </span>
                                                            <input type="password" id="password" placeholder="Password" name="password" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Confirm Password

                                                    </label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                            </span>
                                                            <input type="password" id="password-confirm" placeholder="Confirm Password" name="confirm_password" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-primary" id="change-password">Submit</button>
                                                    &nbsp;
                                                    <button type="reset" class="btn btn-danger">Cancel</button>
                                                    &nbsp;
                                                    <input type="reset" class="btn btn-default hidden-xs" value="Reset"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div id="tab-editprofile" class="tab-pane fade in">
                                 <div class="col-md-12">

                                {!! Form::model($user, ['url' => URL::to('my-account'), 'method' => 'put', 'class' => 'form-horizontal','enctype'=>"multipart/form-data"]) !!}
                                    {{ csrf_field() }}

                                     <div id="rootwizard">

                                            <div class="form-group {{ $errors->first('username', 'has-error') }}">
                                                <label for="username" class="col-sm-2 control-label">Name(MM) *</label>
                                                <div class="col-sm-10">
                                                    <input id="username" name="username" type="text"
                                                           placeholder="User Name" class="form-control required"
                                                           value="{!! old('username', $user->username) !!}"/>

                                                    {!! $errors->first('username', '<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->first('username_en', 'has-error') }}">
                                                <label for="username_en" class="col-sm-2 control-label">Name(Eng) *</label>
                                                <div class="col-sm-10">
                                                    <input id="username_en" name="username_en" type="text"
                                                           placeholder="User Name" class="form-control required"
                                                           value="{!! old('username_en', $user->username_en) !!}"/>

                                                    {!! $errors->first('username_en', '<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                                <label for="email" class="col-sm-2 control-label">Email *</label>
                                                <div class="col-sm-10">
                                                    <input id="email" name="email" placeholder="E-Mail" type="text"
                                                           class="form-control required email"
                                                           value="{!! old('email', $user->email) !!}"/>

                                                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                            
                                            <div class="form-group  {{ $errors->first('dob', 'has-error') }}">
                                                    <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
                                                    <div class="col-sm-10">
                                                        <input id="dob" name="dob" type="text" class="form-control" value="{!! old('dob', $user->dob) !!}"
                                                               data-date-format="YYYY-MM-DD"
                                                               placeholder="yyyy-mm-dd"/>
                                                    </div>
                                                    <span class="help-block">{{ $errors->first('dob', ':message') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->first('gender', 'has-error') }}">
                                                <label for="email" class="col-sm-2 control-label">Gender *</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" title="Select Gender..." name="gender">
                                                        <option value="">Select</option>
                                                        <option value="male" @if($user->gender === 'male') selected="selected" @endif >Male</option>
                                                        <option value="female" @if($user->gender === 'female') selected="selected" @endif >Female</option>
                                                        <option value="other" @if($user->gender === 'other') selected="selected" @endif >Other</option>

                                                    </select>
                                                </div>
                                                <span class="help-block">{{ $errors->first('gender', ':message') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->first('acadamic_year', 'has-error') }}">
                                                <label for="acadamic_year" class="col-sm-2 control-label">Acadamic Year *</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" title="Select Year..." name="acadamic_year_id" disabled>
                                                        <option value="">Select Acadamic Year</option>
                                                        @foreach($acadamicyears as $acy)
                                                        <option value="{{ $acy->id }}"
                                                                @if(old('acadamic_year',$user->acadamic_year_id) === $acy->id ) selected="selected" @endif >{{ $acy->acadamic_year}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="help-block">{{ $errors->first('acadamic_year_id', ':message') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->first('class_of_year', 'has-error') }}">
                                                <label for="class_of_year" class="col-sm-2 control-label">Class of year *</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" title="Select Year..." name="class_of_year" disabled>
                                                        <option value="">Select Year</option>
                                                        @foreach($class_of_years as $cy)
                                                        <option value="{{$cy->id}}"
                                                                @if(old('class_of_year',$user->class_of_year) == $cy->id) selected="selected" @endif >{{$cy->year}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <span class="help-block">{{ $errors->first('class_of_year', ':message') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->first('subject', 'has-error') }}">
                                                <label for="subject" class="col-sm-2 control-label">Subject *</label>
                                                <div class="col-sm-10">
                                                    MBA &nbsp;<input id="subject" name="subject" placeholder="" type="radio"
                                                           class="required subject" checked />
                                                    {!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->first('roll_no', 'has-error') }}">
                                                <label for="roll_no" class="col-sm-2 control-label">Roll No *</label>
                                                <div class="col-sm-10">
                                                    <input id="roll_no" name="roll_no" type="text" placeholder="MBA-your number"
                                                           class="form-control required" value="{!! old('roll_no',$user->roll_no) !!}" disabled />
                                                    {!! $errors->first('roll_no', '<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                                
                                            <div class="form-group required">
                                                <label for="group" class="col-sm-2 control-label">Group *</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control required" title="Select group..." name="group_id" disabled id="group">
                                                        <option value="">Select</option>
                                                        @foreach($groups as $group)
                                                            <option value="{{ $group->id }}"
                                                                    @if($group->id == old('group_name',$user->group_id)) selected="selected" @endif >{{ $group->group_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    {!! $errors->first('group', '<span class="help-block">:message</span>') !!}
                                                </div>
                                                <span class="help-block">{{ $errors->first('group', ':message') }}</span>
                                            </div>

                                            <div class="form-group {{ $errors->first('designation', 'has-error')    }}">
                                                <label for="designation" class="col-sm-2 control-label">Designation </label>
                                                <div class="col-sm-10">
                                                    <input id="designation" name="designation" type="text"
                                                           placeholder="Designation " class="form-control" value="{{ old('designation',$user->designation)}}" />
                                                    {!! $errors->first('designation', '<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->first('department', 'has-error') }}">
                                                <label for="department" class="col-sm-2 control-label">Department </label>
                                                <div class="col-sm-10">
                                                    <input id="department" name="department" type="text"
                                                           placeholder="Department" class="form-control" value="{{ old('department',$user->department)}}"/>
                                                    {!! $errors->first('department', '<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>

                                            <div class="form-group {{ $errors->first('ministry_company', 'has-error') }}">
                                                    <label for="ministry_company" class="col-sm-2 control-label">Ministry/Company </label>
                                                    <div class="col-sm-10">
                                                        <input id="ministry_company" name="ministry_company" type="text" value="{{ old('ministry_company', $user->ministry_company) }}" 
                                                               placeholder="Ministry/Company" class="form-control"/>
                                                        {!! $errors->first('ministry_company', '<span class="help-block">:message</span>') !!}
                                                    </div>
                                            </div>




                                            <div class="form-group {{ $errors->first('pic', 'has-error') }}">
                                                <label for="pic" class="col-sm-2 control-label">Profile picture</label>
                                                <div class="col-sm-10">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                          @if($user->pic)
                                                                <img src="{!! url('/').'/uploads/users/'.$user->pic !!}" id="image_preview" alt="image preview" style="max-width: 200px; max-height: 200px;">
                                                                    
                                                            @else
                                                                <img src="{{ asset('assets/images/authors/no_avatar.png') }}" id="image_preview" alt="image preview" style="max-width: 200px; max-height: 200px;">
                                                            @endif
 
                                                            <div>
                                                                <span class="btn btn-default btn-file">
                                                                    <span class="fileinput-new">Select image</span>
                                                                    <span class="fileinput-exists">Change</span>
                                                                    <input id="pic"  type="file" class="form-control upload"/>
                                                                </span>
                                                                <a href="#" class="btn btn-danger fileinput-exists"
                                                                   id="remove_preview">Remove</a>
                                                            </div>
           
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="pic" value="" id="hidden_photo">
                                                    {!! $errors->first('pic', '<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="address" class="col-sm-2 control-label">Address *</label>
                                                <div class="col-sm-10">
                                                    <input id="address" name="address" type="text" class="form-control"
                                                           value="{!! old('address',$user->address) !!}"/>
                                                </div>
                                                <span class="help-block">{{ $errors->first('address', ':message') }}</span>
                                            </div>

                                            <div class="form-group">
                                                <label for="phone" class="col-sm-2 control-label">Phone *</label>
                                                <div class="col-sm-10">
                                                    <input id="phone" name="phone" type="text" class="form-control"
                                                           value="{!! old('phone',$user->phone) !!}"/>
                                                </div>
                                                <span class="help-block">{{ $errors->first('phone', ':message') }}</span>
                                            </div>

                                            <div class="form-group">
                                                <label for="facebook" class="col-sm-2 control-label">Facebook</label>
                                                <div class="col-sm-10">
                                                    <input id="facebook" name="facebook" type="text" class="form-control"
                                                           value="{!! old('facebook',$user->facebook) !!}"/>
                                                </div>
                                                <span class="help-block">{{ $errors->first('facebook', ':message') }}</span>
                                            </div>

            
                                           <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-primary" type="submit">Save</button>
                                                </div>
                                            </div>
                                               
                                        </div>
                                    </div>
                                </form>

                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jquery-mockjax/js/jquery.mockjax.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/x-editable/js/bootstrap-editable.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrap-magnify/bootstrap-magnify.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/holder.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/user_profile.js') }}"  type="text/javascript"></script>

    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/edituser.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/croppie.js')}}"></script>
        <!-- end page level js -->
     <!-- crop image modal -->
    @include('modal.image_crop')

    <script type="text/javascript">
        $(document).ready(function () {
            $('#change-password').click(function (e) {
                e.preventDefault();
                var check = false;
                if ($('#password').val() ===""){
                    alert('Please Enter password');
                }
                else if  ($('#password').val() !== $('#password-confirm').val()) {
                    alert("confirm password should match with password");
                }
                else if  ($('#password').val() === $('#password-confirm').val()) {
                    check = true;
                }

                if(check == true){
                var sendData =  '_token=' + $("input[name='_token']").val() +'&password=' + $('#password').val() +'&id=' + {{ $user->id }};
                    var path = "passwordreset";
                    $.ajax({
                        url: path,
                        type: "post",
                        data: sendData,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                        },
                        success: function (data) {
                            alert('password reset successful');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert('error in password reset');
                        }
                    });
                }
            });
        });
    </script>

    <script>
    $baseurl = "https://naypyitawmba.com/";

        $(document).ready(function(){

            var quarter_id = $('#quarter_id').val();
            if (quarter_id!='' || quarter_id!='undefined') {
                 getcourses(quarter_id);
            }
        });

        $('#quarter_id').change(function(){
                getcourses($(this).val());
        });

        function getcourses($qid) {
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                $.ajax({
                    url : $baseurl+'frontend/getCoursesByQuarter',
                    dataType : 'html',
                    method : 'post',
                    data : {
                            'quarter_id' : $qid,
                            '_token': $('#ctr_token').val()
                    },
                    success : function(data){
                        $('#course_id').html(data);
                        if(data==""){
                            $('#course_id').html('<option value="">Select Course</option>');
                        }

                        $course_id = '<?php echo $course_id;?>';
                        $("#course_id").val($course_id);
                    },
                    error : function(error){
                        console.log(error);
                    }
                });
        }
</script>
@stop
