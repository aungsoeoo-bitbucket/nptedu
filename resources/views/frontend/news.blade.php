@extends('layouts/default')

{{-- Page title --}}
@section('title')
News | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/news.css') }}">
<link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/timeline.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('assets/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" />
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
<div class="breadcum"></div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <div class="row news">
            <div class="col-md-12">
                <!-- News1 Section Start -->
                @forelse ($blogs as $blog)
                <div class="blog thumbnail ">
                    <div class="row">
                        <div class="col-md-3">
                            @if($blog->image)
                                <a href="{{ URL::to('news-item/'.$blog->slug) }}">
                                    <img src="{{ URL::to('/uploads/blog/'.$blog->image)  }}" alt="image" class="img-responsive">
                                </a>
                            @endif
                        </div>
                        <dir class="col-md-7">
                                <a href="{{ URL::to('news-item/'.$blog->slug) }}">
                                    <h4 class="primary news_headings">{{$blog->title}}</h4>
                                </a>
                        </dir>
                        <div class="col-md-2">
                            <p style="float: right;">{{ $blog->created_at->diffForHumans() }}</p>
                        </div>
                    </div>
                    
                    
                    <div class="news_item_text_1">
                        <p class="text-right">
                            <a href="{{ URL::to('news-item/'.$blog->slug) }}" class="btn btn-primary text-white">
                                Read more
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                    <h3>No Posts Exists!</h3>
                @endforelse
                <ul class="pager">
                    {!! $blogs->render() !!}
                </ul>
                <!-- //News1 Section End -->
               
                <!-- //News3 Section End -->
            </div>
           
            <!-- Tab-content End -->
        </div>
        <!-- //Tabbablw-line End -->
    </div>
    <!-- Tabbable_panel End -->
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <!--tags-->
    <script src="{{ secure_asset('assets/vendors/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ secure_asset('assets/vendors/wow/js/wow.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            new WOW().init();
        });
    </script>
    <!-- end of page level js -->

@stop
