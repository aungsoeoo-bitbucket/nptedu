@extends('layouts/default')

{{-- Page title --}}
@section('title')
Course Detail | Nay Pyi Taw EDU
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/news.css') }}">
<link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/blog.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
<div class="breadcum"></div><br>
@stop


{{-- Page content --}}
@section('content')
    <div class="container">
        <div class="row news_item_image thumbnail">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Class:</label>
                            </div>
                            <div class="col-md-10">
                                <h5 class="primary news_headings">MBA </h5>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Year:</label>
                            </div>
                            <div class="col-md-10">
                                <h5 class="primary news_headings">{{ $course[0]->year->year }} </h5>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Quarter:</label>
                            </div>
                            <div class="col-md-10">
                                <h5 class="primary news_headings">{{ $course[0]->quarter->quarter_name}}</h5>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Course:</label>
                            </div>
                            <div class="col-md-10">
                                <h5 class="primary news_headings">{{$course[0]->course_name}} </h5>
                            </div>
                        </div>
                    </div>
                    
                </div>
               
            </div>
            <div class="col-md-8">
                <div >
                    @if($course[0]->course_photo!='')
                        <img src="{{ URL::to('/uploads/course/'.$course[0]->course_photo)  }}" alt="image" class="img-responsive thumbnail" style="width: 150px; height: 200px; ">
                    @else
                        <img src="{{ secure_asset('assets/images/no_cover.jpg') }}" alt="..." class="img-responsive thumbnail" style="width: 150px; height: 200px; " />
                    @endif
                </div>
            </div>
        </div>
        <div class="row news_item_image thumbnail">
            <div class="col-lg-12">
                <ul class="nav  nav-tabs ">
                    <li class="active">
                        <a href="#tab1" data-toggle="tab">Document</a>
                    </li>
                    <li>
                        <a href="#tab2" data-toggle="tab">Audio</a>
                    </li>
                    <li>
                        <a href="#tab3" data-toggle="tab">Video</a>
                    </li>

                </ul>
                <div  class="tab-content mar-top">
                    <div id="tab1" class="tab-pane fade active in">
                        @if($course[0]->coursemanagement->count()>0)
                            @foreach($course[0]->coursemanagement as $c)
                                @if(($c->document!=''))
                                    <div class="row" style="margin:10px;">
                                        
                                        
                                        <div class="col-md-8">
                                            <h5 class="primary news_headings">{{$c->title}} </h5>
                                            <p style="margin-left: 20px;">{{ $c->description}}</p>
                                        </div>
                                       {{--  <div class="col-md-1">
                                            <a href="{{route('viewfile',$c->document)}}" target="_blank"> 
                                               <img src="{{ secure_asset('/assets/img/pdf.png') }}" style="width: 50px; height: 50px;" alt="document" class="img-responsive" />
                                            </a>
                                        </div> --}}
                                        <div class="col-md-2" style="margin-top: 10px;">
                                            <a href="{{route('downloadfile',$c->id)}}" >
                                                <i class="fa fa-download" aria-hidden="true">&nbsp;Download</i>
                                            </a>
                                        </div>
                                    </div>
                                     <hr>
                                @endif
                            @endforeach
                            {{-- @else
                            <div class="row" style="margin:10px;">
                                <p>No document File</p>
                            </div>
                            @endif --}}
                        @else
                            <div class="row" style="margin:10px;">
                                <p>No data</p>
                            </div>
                        @endif
                    </div>
                    <div id="tab2" class="tab-pane">
                        @if($course[0]->coursemanagement->count()>0)
                        <div class="row">
{{--                             @if($course[0]->coursemanagement[0]->audio!='') --}}
                            @foreach($course[0]->coursemanagement as $c)
                             @if($c->audio!='')
                                <div class="row" style="margin:10px;">
                                   
                                   
                                     <div class="col-md-8">
                                        <h5 class="primary news_headings">{{$c->title}} </h5>
                                        <p style="margin-left: 20px;">{{ $c->description}}</p>
                                    </div>
                                        <div class="col-md-1">
                                            {{-- <a href="{{route('viewfile',$c->audio)}}" target="_blank">  --}}
                                              {{--  <img src="{{ secure_asset('/assets/img/audio.png') }}" style="width: 50px; height: 50px;" alt="audio" class="img-responsive" /> --}}
                                            {{-- </a> --}}
                                        </div>
                                        <div class="col-md-2" style="margin-top: 10px;">
                                            <a href="{{route('getAudio',$c->audio)}}" >
                                                <i class="fa fa-download" aria-hidden="true">&nbsp;Download</i>
                                            </a>
                                        </div>
                                    
                                  
                                    {{-- <div class="col-md-4" style="margin-top: 10px;">
                                         @if($c->audio!='')
                                           <audio controls>
                                                <source src="{{ secure_asset('/uploads/files/'.$c->audio)}}" type="audio/mp3">
                                                    Your browser does not support the audio element.
                                            </audio>
                                        @else
                                            <p>No audio file</p>                         
                                        @endif
                                    </div> --}}
                                </div>
                                <hr>
                                 @endif
                            @endforeach
                            {{-- @else
                            <div class="row" style="margin:10px;">
                                <p>No audio File</p>
                            </div>
                            @endif --}}
                        </div>
                        @else
                            <div class="row" style="margin:10px;">
                                <p>No data</p>
                            </div>
                        @endif
                    </div>
                    <div id="tab3" class="tab-pane">
                        @if($course[0]->coursemanagement->count()>0)
                            {{-- @if($course[0]->coursemanagement[0]->video!='') --}}
                            <div class="row">
                                @foreach($course[0]->coursemanagement as $c)
                                    @if($c->video!='')
                                        <div class="row" style="margin:10px;">
                                           
                                            
                                                 <div class="col-md-8">
                                                    <h5 class="primary news_headings">{{$c->title}} </h5>
                                                    <p style="margin-left: 20px;">{{ $c->description}}</p>
                                                </div>
                                                <div class="col-md-1">
                                                    {{-- <a href="{{route('viewfile',$c->video)}}" target="_blank">  --}}
                                                      {{--  <img src="{{ secure_asset('/assets/img/video.png') }}"  alt="video" class="img-responsive" /> --}}
                                                    {{-- </a> --}}
                                                </div>
                                                <div class="col-md-2" style="margin-top: 10px;">
                                                    <a href="{{route('getVideo',$c->video)}}" >
                                                        <i class="fa fa-download" aria-hidden="true">&nbsp;Download</i>
                                                    </a>
                                                </div>
                                           
                                          
                                           {{--  <div class="col-md-6" style="margin-top: 10px;">
                                                @if($c->video!='')
                                                    <video width="400" controls>
                                                        <source src="{{ secure_asset('/uploads/files/'.$c->video)}}" type="video/mp4">
                                                        Your browser does not support HTML5 video.
                                                    </video>
                                                @else
                                                    <p>No data</p>                         
                                                @endif
                                            </div> --}}
                                        </div>
                                        <hr>                                        
                                    @endif
                                @endforeach
                            </div>
                          {{--   @else
                            <div class="row" style="margin:10px;">
                                <p>No Video File</p>
                            </div>
                            @endif --}}
                        @else
                            <div class="row" style="margin:10px;">
                                <p>No data</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ secure_asset('assets/vendors/wow/js/wow.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            new WOW().init();
        });
    </script>

@stop
