@extends('layouts/default')

{{-- Page title --}}
@section('title')
Contact | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/contact.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
    </div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <div class="row">
            <!-- Contact form Section Start -->
            <div class="col-md-6">
                <h2>Contact Form</h2>
                <!-- Notifications -->
                <div id="notific">
                @include('admin.layouts.notification')
                </div>
                <form class="contact" id="contact" action="{{route('contact')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    @if($user!="")
                        <div class="form-group">
                            <input type="text" name="name" class="form-control input-lg" placeholder="Your name" value="{{ old('name',$user->username )}}" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="contact_email" class="form-control input-lg" placeholder="Your email address" value="{{ old('email',$user->email )}}"  required>
                        </div>
                    @else
                     <div class="form-group">
                            <input type="text" name="name" class="form-control input-lg" placeholder="Your name" value="{{ old('name' )}}" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="contact_email" class="form-control input-lg" placeholder="Your email address" value="{{ old('email')}}"  required>
                        </div>
                    @endif
                    <div class="form-group">
                        <textarea name="message" class="form-control input-lg no-resize resize_vertical" rows="6" placeholder="Your comment" required></textarea>
                    </div>
                    <div class="input-group">
                        <button class="btn btn-primary" type="submit">submit</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                    </div>
                </form>
            </div>
            <!-- //Conatc Form Section End -->
            <!-- Address Section Start -->
            <div class="col-md-6 col-sm-6" id="address_margt">
                <div class="media media-right">
                    <div class="media-left media-top">
                        <a href="#">
                            <div class="box-icon">
                                <i class="livicon" data-name="location" data-size="22" data-loop="true" data-c="#fff" data-hc="#fff"></i>
                            </div>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Address:</h4>
                        <address>
                            GSAD, Yarzathingaha Street, Ottarathiri Township, National Archives Department, Nay Pyi Taw.
                        </address>
                    </div>
                </div>
{{--                 <div class="media padleft10">
                    <div class="media-left media-top">
                        <a href="#">
                            <div class="box-icon">
                                <i class="livicon" data-name="phone" data-size="22" data-loop="true" data-c="#fff" data-hc="#fff"></i>
                            </div>
                        </a>
                    </div>
                    <div class="media-body padbtm2">
                        <h4 class="media-heading">Telephone:</h4> (+95) 09 1234 56789
                        <br /> Fax: --
                    </div>
                </div> --}}

                <div class="media padleft10">
                    <div class="media-left media-top">
                        <a href="#">
                            <div class="box-icon">
                                <i class="livicon" data-name="mail" data-size="22" data-loop="true" data-c="#fff" data-hc="#fff"></i>
                            </div>
                        </a>
                    </div>
                    <div class="media-body padbtm2">
                        <h4 class="media-heading">Email:</h4> nptmba@gmail.com
                    </div>
                </div>
            </div>
            <!-- //Address Section End -->
        </div>
    </div>

        <!-- Map Section Start -->
    <div align="center">
        {{-- <div id="map" style="width:100%; height:400px;"></div> --}}
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d40703.26233329964!2d96.15744215359803!3d19.838620915774147!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c8b8afaa6ffbd5%3A0xccad63151a1b58cc!2sYarzathingaha+Rd!5e0!3m2!1sen!2smm!4v1557744143377!5m2!1sen!2smm" width="80%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!-- //map Section End -->
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    {{-- <script src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="{{ secure_asset('assets/vendors/gmaps/js/gmaps.min.js') }}" ></script>
    <!--page level js ends-->
    <script>

        $(document).ready(function() {
            var map = new GMaps({
                el: '#map',
                lat: 19.8332519,
                lng: 96.1594892
            });
            map.addMarker({
                lat: 19.8332519,
                lng: 96.1594892,
                title: 'NPT MBA'
            });
        });
    </script> --}}

@stop
