@extends('layouts/default')

{{-- Page title --}}
@section('title')
User Detail | Nay Pyi Taw EDU
@stop


{{-- page level styles --}}
@section('header_styles')
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <link href="{{ secure_asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet"/>
    <link href="{{ secure_asset('assets/vendors/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet"/>

    <link href="{{ secure_asset('assets/css/pages/user_profile.css') }}" rel="stylesheet"/>
@stop
@section('top')
    <div class="breadcum"></div><br>
@stop

{{-- Page content --}}
@section('content')
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div  class="tab-content mar-top">
                    <div id="tab1" class="tab-pane fade active in">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">

                                            {{-- User Data --}}
                                        </h3>

                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-3">
                                            <div align="center">
                                                @if($data[0]->pic)
                                                    <img src="{!! url('/').'/uploads/users/'.$data[0]->pic !!}" alt="img"
                                                         class="img-responsive"  width="50%" />
                                                @elseif($data[0]->gender === "male")
                                                    <img src="{{ secure_asset('assets/images/authors/avatar3.png') }}" alt="..."
                                                         class="img-responsive" width="50%" />
                                                @elseif($data[0]->gender === "female")
                                                    <img src="{{ secure_asset('assets/images/authors/avatar5.png') }}" alt="..."
                                                         class="img-responsive" width="50%" />
                                                @else
                                                    <img src="{{ secure_asset('assets/images/authors/no_avatar.jpg') }}" alt="..."
                                                         class="img-responsive" width="50%" />
                                                @endif
                                            </div>
                                            <br>
                                        </div>


                                        <div class="col-md-9">
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped" id="users">

                                                        <tr>
                                                            <td>@lang('users/title.user_name')</td>
                                                            <td>
                                                                <p class="user_name_max">{{ $data[0]->username }}</p>
                                                            </td>

                                                        </tr>

                                                         <tr>
                                                            <td>@lang('users/title.roll_no')</td>
                                                            <td>
                                                                {{$data[0]->roll_no}}

                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                @lang('users/title.gender')
                                                            </td>
                                                            <td>
                                                                {{ $data[0]->gender }}
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>@lang('users/title.email')</td>
                                                            <td>
                                                                {{ $data[0]->email }}
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td>@lang('users/title.phone')</td>
                                                            <td>
                                                                {{ $data[0]->phone }}
                                                            </td>
                                                        </tr>
                                                       
                                                        <tr>
                                                            <td>@lang('users/title.address')</td>
                                                            <td>
                                                                {{ $data[0]->address }}
                                                            </td>
                                                        </tr>
                                                       
                                                        <tr>
                                                            <td>Status</td>
                                                            <td>
                                                                {{ $statuses[0]->status->status_name }} @if($statuses[0]->pass_date!='' && $statuses[0]->pass_date!=null) ({{ date('d-m-Y', strtotime($statuses[0]->pass_date ))}} ) @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Group</td>
                                                            <td>{{ $data[0]->group_name }} </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- Bootstrap WYSIHTML5 -->
    <script  src="{{ secure_asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
@stop
