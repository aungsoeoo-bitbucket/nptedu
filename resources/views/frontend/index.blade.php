@extends('layouts/default')

{{-- Page title --}}
@section('title')
Home | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/tabbular.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/animate/animate.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/jquery.circliful.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.theme.css') }}">
    <!--end of page level css-->
    <style type="text/css" media="screen">
        @media (min-width:320px) and (max-width:425px){
             .purchase-styl {
                  padding: 5px 5px !important; 
                background-color: #01bc8c;
                border: none;
                box-shadow: 0 2px 0 #268a70;
                color: #fff !important;
                margin-right: 20px;
                margin-top: 10px;
            }

            .purchae-hed {
                margin-bottom: 20px;
                margin-left: 15px;
                color: #01bc8c;
                text-transform: uppercase;
                font-size: 16px;
                font-weight: 300;
            }
        }
    </style>
@stop

{{-- slider --}}
@section('top')

    <!--Carousel Start -->
    <div id="owl-demo" class="owl-carousel owl-theme">
        <div class="item img-fluid">
                <img src="{{ asset('assets/images/slide1.png') }}" alt="slider-image">
        </div>
        <div class="item img-fluid">
                <img src="{{ asset('assets/images/slide2.png') }}" alt="slider-image">
        </div>
        <div class="item img-fluid">
                <img src="{{ asset('assets/images/slide3.png') }}" alt="slider-image">
        </div>
    </div>
    <!-- //Carousel End -->
@stop

{{-- content --}}
@section('content')
    <div class="container">
        <section class="purchas-main">
            <div class="container bg-border" align="center">
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <h1 class="purchae-hed">Welcome to naypyitawmba.com</h1>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <a href="{{ url('/register') }}" class="btn btn-primary purchase-styl pull-right">Enroll now</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Service Section Start-->
        <div class="row">
            <!-- Responsive Section Start -->
            <div class="text-center">
                <h3 class="border-primary"><span class="heading_border bg-primary"> MBA PROGRAMME</span></h3>
            </div>
             <div class="col-md-12">
                <!-- Responsive Section Start -->
                <div class="col-sm-4 col-md-">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <div class="info">
                            <h3 class="success text-center">Vision</h3>
                            <p>To create the socially responsible business leaders that can effectively utilize in development of national economy.</p>
                             <p>&nbsp;</p>
                             <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
                <!-- //Responsive Section End -->
                <!-- Easy to Use Section Start -->
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-icon box-icon1">
                            <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#418bca" data-hc="#418bca"></i>
                        </div>
                        <div class="info">
                            <h3 class="primary text-center">Mission</h3>
                            <p>To imporove cognitive ability in applying business and managerial concepts.</p>
                             <p>&nbsp;</p>
                             <p>&nbsp;</p>
                             <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
                <!-- //Easy to use Section End -->
                 <!-- Easy to Use Section Start -->
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-icon box-icon2">
                            <i class="livicon icon" data-name="gears" data-size="55" data-loop="true" data-c="#f89a14" data-hc="#f89a14"></i>
                        </div>
                        <div class="info">
                            <h3 class="primary text-center">Objective</h3>
                             <ol>
                                <li style="text-align: left;"> To build up the required competence, skills and techniques needed in leading business organizations.</li>
                                <li style="text-align: left;">To provide the education services that can assure the quality of our graduates to the modern economic environment.</li>
                                <li style="text-align: left;">To fulfill the interests of stakeholders and society.</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- //Easy to use Section End -->
            </div>

        </div>
        <!-- //Services Section End -->
    </div>

    <div class="text-center">
        <h3 class="border-primary"><span class="heading_border bg-primary"> About Us</span></h3>
    </div>
    <!-- Layout Section Start -->
    <section class="feature-main">
         <div class="row">
            <!-- Responsive Section Start -->
            <div class="col-sm-1 col-md-1"></div>
            <div class="col-sm-10 col-md-10">
                <p>Meiktila University of Economic is offering MBA Degree at three campuses: Meiktila University of Economics, University of Mandalay and Graduate School of Administration and Development (GSAD) in Nay Pyi Taw. The MBA programme was launched in 2004 at Meiktila University of Economics and in 2011 at University of Mandalay. In 2018, it was extended at Graduate School of Administration and Development (GSAD) in Nay Pyi Taw.</p>
                <div align="right">
                    <a href="{{ url('/aboutus') }}" >Read More  &nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right" ></i>
                    </a>                  
                </div>
            </div>
            <div class="col-sm-1 col-md-1"></div>
        </div>
    </section>
    <!-- //Layout Section Start -->
    <!-- Accordions Section End -->
    <div class="container">
        <!-- Our Team Start -->
        <div class="row text-center">
            <h3 class=" border-primary"><span class="heading_border bg-primary">Featured Professors</span></h3>
            @foreach($professors as $professor)
            <div class="col-md-3 col-sm-5 profile">
                <div class="thumbnail bg-white">
                    @if($professors[0]->photo)
                        <img src="{!! url('/').'/uploads/professor/'.$professor->photo !!}" alt="img"
                             class="img-responsive"/ style="width: 150px;">
                    @elseif($professor->gender === "male")
                        <img src="{{ asset('assets/images/authors/avatar3.png') }}" alt="..."
                             class="img-responsive"/ style="width: 150px;">
                    @elseif($professor->gender === "female")
                        <img src="{{ asset('assets/images/authors/avatar5.png') }}" alt="..."
                             class="img-responsive"/ style="width: 150px;">
                    @else
                        <img src="{{ asset('assets/images/authors/no_avatar.jpg') }}" alt="..."
                             class="img-responsive"/ style="width: 150px;">
                    @endif

                    <div class="caption">
                        <b>{{ $professor->name}}</b>
                        <p class="text-center"> {{ $professor->position }}</p>
                        <p class="text-center"> {!! $professor->bio !!}</p>
                        <div class="divide">
                            <a href="#" class="divider">
                                <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="#">
                                <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="#">
                                <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <!-- //Our Team End -->

        <!-- Testimonial Start -->
        <div class="row">
            <!-- Testimonial Section -->
            <div class="text-center">
                <h3 class="border-primary"><span class="heading_border bg-primary">Academic Latest News</span></h3>
            </div>
            @foreach($latest_news as $news)
            <div class="col-md-4">
                <div class="author">
                    @if($news->image)
                        <img src="{!! url('/').'/uploads/blog/'.$news->image !!}" alt="img"
                             class="img-responsive img-circle pull-left" />
                    @endif
                    <p>
                        <label>{{ $news->title }}</label>
                    </p>
                </div>
                 <div>
                    <a href="{{ URL::to('news-item/'.$news->slug) }}" >Read More  &nbsp;&nbsp;&nbsp;<i class="fa fa-angle-double-right" ></i>
                    </a>                  
                </div>
            </div>
            @endforeach
           
            <!-- Testimonial Section End -->
        </div>
        <br>
        <!-- Testimonial End -->        
        <div class="text-center marbtm10">
            <h3 class=" border-primary"><span class="heading_border bg-primary">Programme Design & Structure</span></h3>
        </div>
            </div>
        <div class="sliders">
            <!-- Our skill Section start -->
            <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 text-center">
                <div class="text-center center-block">
                    <strong class="success">Professors</strong>
                    <div>
                        <span class="fa fa-users"></span>
                    </div>
                    <div id="myStat3" class="center-block" data-startdegree="0" data-dimension="150" data-text="{{ $no_of_professors }}" data-width="4" data-fontsize="28"  data-fgcolor="#3abec0" data-bgcolor="#eee"></div>
                </div>
            </div>
                <div class="col-md-3 col-sm-6 text-center" >
                <div class="text-center center-block">
                    <strong class="success">Academic Year</strong>
                    <div>
                        <span class="fa fa-graduation-cap"></span>
                    </div>
                    <div id="myStat4" class="center-block" data-startdegree="0" data-dimension="150" data-text="{{ $no_of_year}}" data-width="4" data-fontsize="28"  data-fgcolor="#3abec0" data-bgcolor="#eee"></div>
                </div>
            </div>
                <div class="col-md-3 col-sm-6 text-center ">
                <div class="text-center center-block">
                <strong class="success">Quarters</strong>
                <div>
                    <span class=" fa fa-graduation-cap"></span>
                </div>
                <div id="myStat5" class="center-block" data-startdegree="0" data-dimension="150" data-text="{{ $no_of_quarter}}" data-width="4" data-fontsize="28"  data-fgcolor="#3abec0" data-bgcolor="#eee"></div>
            </div>
            </div>
                <div class="col-md-3 col-sm-6 text-center" >
                <div class="text-center center-block">
                    <strong class="success">Students</strong>
                    <div>
                        <span class="fa fa-users"></span>
                    </div>
                    <div id="myStat6" class="center-block" data-startdegree="0" data-dimension="150" data-text="{{ $no_of_students }}" data-width="4" data-fontsize="28"  data-fgcolor="#3abec0" data-bgcolor="#eee"></div>
                </div>
            </div>
        </div>
            <!-- Our skills Section End -->
        </div>
        <!-- //Our Skills End -->
    </div>
    <!-- //Container End -->
@stop
{{-- footer scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('assets/js/frontend/jquery.circliful.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/wow/js/wow.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/index.js') }}"></script>
    <!--page level js ends-->
@stop
