@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Courses | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/news.css') }}">
    <link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/timeline.css') }}">
    <link rel="stylesheet" href="{{ secure_asset('assets/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.css') }}" />

    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ secure_asset('assets/vendors/owl_carousel/css/owl.transitions.css') }}" rel="stylesheet" type="text/css">


    <style>
        .news-body .media {
            padding-bottom: 10px;
            border-bottom: 1px solid #ececec;
        }

        .news-body .media-object {
            height: 95px;
            width: 95px;
        }

        .lifestyle img {
            height: 150px;
            width: 100%;
        }



        .newsticker {
            height: 350px !important;
        }

        #carousel img {
            height: 150px;
            width: 100%;
        }

        .owl-pagination {
            display: none;
        }

        .slider-content {
            height: 65px;
            overflow: hidden;
        }

        .height_180 {
            height: 180px;
            width: 100%;
        }

        .sports-content {
            height: 120px;
            overflow: hidden;
        }

        .sports-height {
            height: 200px;
            overflow: hidden;
        }

        .mt-20 {
            margin-top: 20px;
        }

        p a {
            color: #418bca !important;
        }

        .tabbable-line > .nav-tabs > li{
            background-color: transparent !important;
        }

       .news .tabbable-line > .nav-tabs .nav-link.active {
            border-bottom: 4px solid #f3565d !important;
        }
        .the-box {
            padding: 15px;
            margin-bottom: 30px;
            border: 1px solid #D5DAE0;
            position: relative;
            background: white;
        }
        .thumbnail{
            margin: 10px;
        }

    </style>
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum"></div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container news mt-4">
        @if( $years->count() != 0)
            @foreach($years as $y)
                <h3>{{ $y->year }}</h3>
                <div class="row news">
                    @if($y->quarter->count() )
                        @foreach($y->quarter as $q)
                            <div class="col-md-12">
                                <div class="the-box">
                                    <a>
                                        <h4 class="small-heading more-margin-bottom">{{$q->quarter_name}}</h4>
                                    </a>
                                    
                                    <hr>
                                    <div class="row">
                                        @if($q->course->count())
                                            @foreach($q->course as $c)

                                                <div class="col-sm-2 thumbnail">
                                                    <a href="{{ URL::to('course-detail/'.$c->id) }}" >
                                                        @if($c->course_photo!='')
                                                            <img  src="{!! url('/').'/uploads/course/'.$c->course_photo !!}" alt="course photo" class="img-max">
                                                        @else
                                                            <img src="{{ secure_asset('assets/images/no_cover.jpg') }}"
                                                                 alt="..."
                                                                 class="img-responsive"/>
                                                        @endif
                                                    </a>
                                                    <a href="{{ URL::to('course-detail/'.$c->id) }}">
                                                        <h5 class="text-primary" style="text-align: center;">{{ $c->course_name }}</h5>
                                                    </a>
                                                </div>
                                            @endforeach 
                                        @else
                                            <div>
                                                <p style="margin:10px;">No Data</p>
                                            </div>
                                        @endif  
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div>
                            <p style="margin:10px;">No Data</p>
                        </div>
                    @endif
                    {{-- <div class="col-md-4">
                        <div class="the-box">
                            <a >
                                <h4 class="small-heading more-margin-bottom">Recent Courses</h4>
                            </a>
                            
                            <hr>
                            <ul class="media-list">
                                @foreach($courses as $course)
                                <li class="media">
                                    <div class="media-body">
                                        <div class="media-heading">
                                            <a href="{{ URL::to('course-detail/'.$course->id) }} ">
                                                <h5 class="news_headings"> {{ $course->course_name }} </h5>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                @endforeach   
                            </ul>
                        </div>
                    </div>
         --}}
                    <!-- Tab-content End -->
                </div>
                <hr>
            <!-- //Tabbablw-line End -->
            @endforeach
        @else
             <div class="the-box">No data</div>
        @endif
    </div>
    <!-- Tabbable_panel End -->

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <!--tags-->
    <script src="{{ secure_asset('assets/vendors/bootstrap-tagsinput/js/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ secure_asset('assets/vendors/jquery_newsTicker/js/jquery.newsTicker.js') }}" type="text/javascript"></script>
    <script src="{{ secure_asset('assets/vendors/owl_carousel/js/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script>

        $('.newsticker').newsTicker({
            direction: 'down',
            row_height: 85,
            max_rows: 3,
            duration: 2000
        });
        var owl = $('#carousel');
        owl.owlCarousel({
            autoPlay: 2000, //Set AutoPlay to 3 seconds
            items: 1,
            loop: true,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],

        });


    </script>
    <!-- end of page level js -->

@stop
