@extends('layouts/default')

{{-- Page title --}}
@section('title')
MBA Class | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    {{-- <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/shopping.css') }}"> --}}
    <link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick-theme.css') }}">
    
    <style type="text/css" media="screen">
        .search_div{
            margin: 10px;
        }

        * {
          box-sizing: border-box;
        }

      
        h4.text-primary, h5.text-primary{
            text-align: center;
        }

    </style>
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum"></div><br>
@stop
<?php 
    $keyword=(isset($_GET['keyword'])?$_GET['keyword']:'');
    $year_id=(isset($_GET['year_id'])?$_GET['year_id']:'');
    $status_id=(isset($_GET['status_id'])?$_GET['status_id']:'');


?>

{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <!--Content Section Start -->
        <div class="row">
           {!! Form::open(array('url' => URL::to('status'), 'method' => 'get', 'class' => 'form-horizontal', 'files'=> true)) !!}
{{-- 
                <div class="col-md-2 search_div">
                    <div class="form-group">
                        <select class="form-control" name="year_id">
                            <option value="">Select Year</option>
                            @foreach($years as $y)
                              <option value="{{$y->id}}" {{ (old('year_id',$year_id)==$y->id)?'selected':'' }}>{{$y->year}}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}

                <div class="col-md-2 search_div ">
                    <div class="form-group">
                        <select class="form-control" name="status_id">
                            <option value="">Select Status</option>
                            @foreach($status_select as $g)
                              <option value="{{$g->id}}" {{ (old('status_id',$status_id)==$g->id)?'selected':'' }}>{{$g->status_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2 search_div">
                     <div class="form-group">
                            <input type="text" name="keyword" value="{{ old('keyword',$keyword) }}" id="keyword" class="form-control" placeholder="Enter Roll Number" >
                    </div>
                </div>

                <div class="col-md-2 search_div">
                    <button type="submit" class="btn btn-primary">
                        Search
                    </button>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="row">
                        Total : {{ $total}}
        </div>

        @if($batchvivas->count()>0)
            
        <div class="row">
            @foreach($batchvivas as $student)
                <div class="col-md-3" align="center" style=" padding: 10px;">
                    @if(Sentinel::check())
                    <a href="{{ URL::to('status_user/detail/'.$student->user_id) }}">
                        @if($student->pic!='')
                            <img  src="{!! url('/').'/uploads/users/'.$student->pic !!}" alt="profile pic" style="padding: 10px; width:200px; height:230px;">
                        @elseif($student->gender === "male")
                            <img  src="{{ asset('assets/images/authors/avatar3.png') }}"
                                 alt="..." style="padding: 10px; width:200px; height:230px;" />
                        @elseif($student->gender === "female")
                            <img  src="{{ asset('assets/images/authors/avatar5.png') }}"
                                 alt="..." style="padding: 10px; width:200px; height:230px;" />
                        @else
                            <img  src="{{ asset('assets/images/authors/no_avatar.jpg') }}"
                                 alt="..." style="padding: 10px; width:200px; height:230px;" />
                        @endif
                    </a>
                    @else

                        @if($student->pic!='')
                            <img  src="{!! url('/').'/uploads/users/'.$student->pic !!}" alt="profile pic" style="padding: 10px; width:200px; height:230px;">
                        @elseif($student->gender === "male")
                            <img  src="{{ asset('assets/images/authors/avatar3.png') }}"
                                 alt="..." style="padding: 10px; width:200px; height:230px;"/>
                        @elseif($student->gender === "female")
                            <img  src="{{ asset('assets/images/authors/avatar5.png') }}"
                                 alt="..." style="padding: 10px; width:200px; height:230px;"/>
                        @else
                            <img  src="{{ asset('assets/images/authors/no_avatar.jpg') }}"
                                 alt="..." style="padding: 10px; width:200px; height:230px;" />
                        @endif

                    @endif
                        <h4 class="text-primary mt-4 mb-2">{{  ($student->roll_no)?$student->roll_no:'-' }}</h4>
                        @if($student->username_en!='')
                            <h5 class="text-primary mt-4 mb-2">{{ strtoupper($student->username_en) }}</h5>
                        @else
                            <h5 class="text-primary mt-4 mb-2">{{ $student->username }}</h5>
                        @endif  
                        <h6 class="text-primary mt-4 mb-2">{{ $student->status_name }} @if($student->pass_date!='' && $student->pass_date!=null) ({{ date('d-m-Y', strtotime($student->pass_date)) }} ) @endif</h6>  
                    @if(Sentinel::check())
                        <a href="{{ URL::to('status_user/detail/'.$student->user_id) }}" class="btn btn-primary btn-block text-white" style="width: 60%;">View</a>
                    @endif
                </div>
            
            @endforeach
        </div>

        @endif
       
        <!-- Best Deal Section Start -->
      
         <div class="row">
            <ul class="pager">
            {!! $batchvivas->appends(request()->query()) !!}
            </ul>

        </div>

        <!-- //Best Deal Section End -->
        <!-- //Content Section End -->
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')

@stop
