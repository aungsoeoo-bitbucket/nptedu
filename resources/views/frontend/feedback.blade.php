@extends('layouts/default')

{{-- Page title --}}
@section('title')
Feedback | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/contact.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
    </div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>
                    <label class="text-primary">Students Feedback</label>
                </h3>
            </div>
            <!-- Address Section Start -->
            <div class="col-md-6 col-sm-6">
                <div class="media media-right">
                    <div class="media-left media-top">
                    </div>
                    <div class="media-body">
                        <p class="media-heading" align="justify">ေက်ာင္းသား/သူမ်ား ခင္ဗ်ာ။ <br>
naypyitawmba.com  website အသံုးျပဳျခင္းနွင့္ပတ္သတ္၍ အဆင္မေျပမွုမ်ားရွိပါက ပံု(Screen shot) နွင့္တကြ ေပးပို့နိုင္ပါျပီခင္ဗ်ာ။<br> 
အျခားျဖည့္စြက္အၾကံျပဳလိုသည္မ်ားကိုလည္း ေပးပို့နုိင္ပါသည္။</p>
                    </div>
                </div>

                <div class="media padleft10">
                    <div class="media-left media-top">
                    </div>
                    <div class="media-body padbtm2">
                        <p  class="media-heading">Email: nptmba@gmail.com</p > 
                    </div>
                </div>
            </div>
            <!-- //Address Section End -->

                        <!-- Contact form Section Start -->
            <div class="col-md-6">
                <p>Feedback Form</p>
                <!-- Notifications -->
                <div id="notific">
                @include('admin.layouts.notification')
                </div>
                <form class="contact" id="contact" action="{{route('feedback.post')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    @if($user!="")
                        <div class="form-group">
                            <input type="text" name="name" class="form-control input-lg" placeholder="Your name" value="{{ old('name',$user->username )}}" required style="font-size: 14px !important;">
                        </div>
                        <div class="form-group">
                            <input type="email" name="contact_email" class="form-control input-lg" placeholder="Your email address" value="{{ old('email',$user->email )}}"  required style="font-size: 14px !important;">
                        </div>
                    @else
                     <div class="form-group">
                            <input type="text" name="name" class="form-control input-lg" placeholder="Your name" value="{{ old('name' )}}" required style="font-size: 14px !important;">
                        </div>
                        <div class="form-group">
                            <input type="email" name="contact_email" class="form-control input-lg" placeholder="Your email address" value="{{ old('email')}}"  required style="font-size: 14px !important;">
                        </div>
                    @endif
                    <div class="form-group">
                        <textarea name="message" class="form-control input-lg no-resize resize_vertical" rows="6" placeholder="Your Feedback" required style="font-size: 14px !important;"></textarea>
                    </div>

                    <div class="form-group">
                         <input type="file" name="photo" class="form-control input-lg" placeholder="Screen Shot photo" style="font-size: 14px !important;"  accept="image/*" >
                    </div>
                    <div class="input-group">
                        <button class="btn btn-primary" type="submit">submit</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                    </div>
                </form>
            </div>
            <!-- //Conatc Form Section End -->
        </div>
    </div>
    <br>
    <br>

    
@stop

{{-- page level scripts --}}
@section('footer_scripts')

@stop
