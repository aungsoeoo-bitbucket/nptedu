@extends('layouts/default')

{{-- Page title --}}
@section('title')
Professors | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/shopping.css') }}">
    <link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
@stop

{{-- breadcrumb --}}
@section('top')
   <div class="breadcum"></div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <!--Content Section Start -->
        <!-- Best Deal Section Start -->
        <div class="row">
            <h3 id="title" class="text-primary" style="text-align: center;">Rector</h3>
            <br>
            <div class="col-sm-4" >
            </div>

            <div class="col-sm-4">
                <div class=" thumbnail text-center">
                        @if($professors[0]->photo)
                            <img src="{!! secure_asset('uploads/professor/'.$professors[0]->photo) !!}" alt="img"
                                 class="img-responsive"/ style="width: 150px;">
                        @elseif($professors[0]->gender === "male")
                            <img src="{{ secure_asset('assets/images/authors/avatar3.png') }}" alt="..."
                                 class="img-responsive"/ style="width: 150px;">
                        @elseif($professors[0]->gender === "female")
                            <img src="{{ secure_asset('assets/images/authors/avatar5.png') }}" alt="..."
                                 class="img-responsive"/ style="width: 150px;">
                        @else
                            <img src="{{ secure_asset('assets/images/authors/no_avatar.jpg') }}" alt="..."
                                 class="img-responsive"/ style="width: 150px;">
                        @endif
                    <br/>
                    <h5 class="text-primary">{{ $professors[0]->name }}</h5>
                   {{ $professors[0]->position }}<br>
                    {!! $professors[0]->bio !!} <br>  
                    
                    <hr>
                    <div class="btn-block text-white">
                        <a href="#" class="divider">
                                <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="#">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="#">
                            <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                        </a>                  
                    </div>
 
                </div>
            </div>
            <div class="col-sm-4">
            </div>
        </div>
        <hr>
        <!-- //Best Deal Section End -->
        <!-- New Launches Section Start -->
        <div class="row">
            <h3 id="title"  class="text-primary" style="text-align: center;">Our Professors </h3>
            <br>
            @foreach($professors as $professor)
            <div class="col-sm-6 col-md-3 " >
                <div class=" thumbnail text-center">
                        @if($professor->photo)
                            <img src="{!! secure_asset('uploads/professor/'.$professor->photo) !!}" alt="img"
                                 class="img-responsive"/ style="width: 150px;">
                        @elseif($professor->gender === "male")
                            <img src="{{ secure_asset('assets/images/authors/avatar3.png') }}" alt="..."
                                 class="img-responsive"/ style="width: 150px;">
                        @elseif($professor->gender === "female")
                            <img src="{{ secure_asset('assets/images/authors/avatar5.png') }}" alt="..."
                                 class="img-responsive"/ style="width: 150px;">
                        @else
                            <img src="{{ secure_asset('assets/images/authors/no_avatar.jpg') }}" alt="..."
                                 class="img-responsive"/ style="width: 150px;">
                        @endif
                    <br/>
                    <h5 class="text-primary">{{ $professor->name }}</h5>
                   {{ $professor->position }}<br>
                    {!! $professor->bio !!} <br>  
                    <hr>
                    <div class="btn-block text-white">
                        <a href="#" class="divider">
                                <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="#">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                        </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="#">
                            <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                        </a>                  
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <nav align="center">
             {{-- {!! $professors->render() !!} --}}
        </nav>
        <!-- //New Launches Section End -->
       
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ secure_asset('assets/vendors/wow/js/wow.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            new WOW().init();
        });
    </script>
@stop
