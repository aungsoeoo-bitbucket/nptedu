@extends('layouts/default')

{{-- Page title --}}
@section('title')
About us | Nay Pyi Taw EDU
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/aboutus.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/animate/animate.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/devicon/devicon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/devicon/devicon-colors.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
    </div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <!-- Slider Section Start -->
        <div class="row">
            <!-- Left Heading Section Start -->
            <div class="col-md-7 col-sm-12 ">
                <h2><label class="text-primary">Message From MBA Programme</label></h2>
                <p>
                    Meikhtila University of Economics was established in2001 in order to adequately respond to rapid changes taking place in the global situation and at the same time contribute to the development of our nation through the pursuit of creative educational and research activities.
                </p>
                <p>Meiktila University of Economic is offering MBA Degree at three campuses: Meiktila University of Economics, University of Mandalay and Graduate School of Administration and Development (GSAD) in Nay Pyi Taw. The MBA programme was launched in 2004 at Meiktila University of Economics and in 2011 at University of Mandalay. In 2018, it was extended at Graduate School of Administration and Development (GSAD) in Nay Pyi Taw.</p>
                <p>
                   The programme practices creative learning for you to improve your cognitive ability in applying business and managerial concepts. It relievers the knowledge, skills and capability required to gain a competitive advantage in the fastest growing economies. Moreover, it provides interpersonal skill for developing social network and aims to cultivate and participate in strong MBA society. Our Programme offers various courses, which are continuously improved to meet all of the requirements for developing and leading in respective organizations operated in Myanmar and international environment.
                </p>
                <p>
                    MBA programme provides incomparable opportunities for individuals who truly want to develop the skills, capabilities and sensibilities necessary to become true leaders in their respective fields, and organizations. We assert that the programme can be a facilitator of your lifelong learning process. Within our MBA programme, students receive a hands-on learning experience through core courses, small group seminars, group presentation, an integrated research project, and a wide range of electives. MBA students also enjoy the chance to take part in internships, short trip and long trip excursions. <br>
                    To achieve all this, our faculty are united in their commitment to providing an education tailored to the needs of each individual student and to creating an environment in which students can actively encourage and engage with one another.
                </p>
            </div>
            <!-- //Left Heaing Section End -->
            <!-- Slider Start -->
            <div class="col-md-5 col-sm-12 slider" >
                <br>
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item"><img src="{{ asset('assets/images/cover1.jpg') }}" alt="slider-image">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/cover2.jpg') }}" alt="slider-image">
                    </div>
                   {{--  <div class="item"><img src="{{ asset('assets/images/slide3.png') }}" alt="slider-image">
                    </div> --}}
                </div>
            </div>
            <!-- //Slider End -->
        </div>
        <!-- //Slider Section End -->
        <!-- Services Section Start -->
        <div class="row">
            <div class="text-center">
                <h3 class="border-success"><span class="heading_border bg-success" >MBA Programme</span></h3>
            </div>
            <!-- left Section Start -->
            <div class="col-md-12">
                <!-- Responsive Section Start -->
                <div class="col-sm-4 col-md-">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <div class="info">
                            <h3 class="success text-center">Vision</h3>
                            <p>To create the socially responsible business leaders that can effectively utilize in development of national economy.</p>
                             <p>&nbsp;</p>
                             <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
                <!-- //Responsive Section End -->
                <!-- Easy to Use Section Start -->
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-icon box-icon1">
                            <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#418bca" data-hc="#418bca"></i>
                        </div>
                        <div class="info">
                            <h3 class="primary text-center">Mission</h3>
                            <p>To imporove cognitive ability in applying business and managerial concepts.</p>
                             <p>&nbsp;</p>
                             <p>&nbsp;</p>
                             <p>&nbsp;</p>
                        </div>
                    </div>
                </div>
                <!-- //Easy to use Section End -->
                 <!-- Easy to Use Section Start -->
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-icon box-icon2">
                            <i class="livicon icon" data-name="gears" data-size="55" data-loop="true" data-c="#f89a14" data-hc="#f89a14"></i>
                        </div>
                        <div class="info">
                            <h3 class="primary text-center">Objective</h3>
                             <ol>
                                <li style="text-align: left;"> To build up the required competence, skills and techniques needed in leading business organizations.</li>
                                <li style="text-align: left;">To provide the education services that can assure the quality of our graduates to the modern economic environment.</li>
                                <li style="text-align: left;">To fulfill the interests of stakeholders and society.</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- //Easy to use Section End -->
            </div>
            <!-- Left Section End -->
        </div>
        <!-- // Services Section End -->
    
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('assets/vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/wow/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/aboutus.js') }}"></script>
    <!--page level js ends-->
@stop
