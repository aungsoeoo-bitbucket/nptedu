@extends('layouts/default')

{{-- Page title --}}
@section('title')
MBA | Thesis Title
@stop

{{-- page level styles --}}
@section('header_styles')
    {{-- <link rel="stylesheet" type="text/css" href="{{ secure_asset('assets/css/frontend/shopping.css') }}"> --}}
    <link href="{{ secure_asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick-theme.css') }}">
    
    <style type="text/css" media="screen">
        .search_div{
            margin: 10px;
        }

        * {
          box-sizing: border-box;
        }

        .slider {
            width: 90%;
            margin: 30px auto;
        }

        .slick-slide {
          margin: 0px 20px;
        }

        .slick-slide img {
          width: 100%;
        }

        .slick-prev:before,
        .slick-next:before {
          color: black;
        }


        .slick-slide {
          transition: all ease-in-out .3s;
          opacity: .2;
        }

        .slick-active {
          opacity: 1;
        }

        .slick-current {
          opacity: 1;
        }
        h4.text-primary, h5.text-primary{
            text-align: center;
        }

    </style>
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum"></div><br>
@stop
<?php 
    $keyword=(isset($_GET['keyword'])?$_GET['keyword']:'');
?>

{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <div class="row">
           {!! Form::open(array('url' => URL::to('/thesis-title'), 'method' => 'get', 'class' => 'form-horizontal', 'files'=> true)) !!}

                <div class="col-md-2 search_div">
                     <div class="form-group">
                            <input type="text" name="keyword" value="{{ old('keyword',$keyword) }}" id="keyword" class="form-control" placeholder="Enter Roll Number" >
                    </div>
                </div>

                <div class="col-md-2 search_div">
                    <button type="submit" class="btn btn-primary">
                        Search
                    </button>
                </div>
            {!! Form::close() !!}
        </div>
        
        <div class="row">
            @foreach($thesisgroups as $th)
                <div class="col-md-12">
                    <h4 class="text-primary">{{ $th->group_name }}( {{   date('d.m.Y', strtotime($th->attend_date))}} )</h4> 

                    <?php 
                        $name_arr       = explode(',', $th->username_en);  
                        $pic_arr        = explode(',', $th->pic);
                        $roll_no_arr    = explode(',', $th->roll_no); 
                        $email_arr      = explode(',', $th->email);
                        $ph_arr         = explode(',', $th->phone);

                    ?>
                    
                    <div class="row">
                        @foreach($pic_arr as $key=>$val)
                            <div class="col-md-3" align="center">
                                    @if($val!='')
                                        <img  src="{!! url('/').'/uploads/users/'.$val !!}" alt="profile pic" width="60%" style="padding: 10px;">
                                    @else
                                        <img  src="{{ asset('assets/images/authors/no_avatar.png') }}"
                                             alt="..." width="60%" style="padding: 10px;" />
                                    @endif

                                    <h5 class="text-primary mt-4 mb-2">{{ $roll_no_arr[$key]}}</h5>
                                    <h5 class="text-primary mt-4 mb-2">
                                        {{ $name_arr[$key] }}
                                    </h5>
                                    <h5 class="text-primary mt-4 mb-2">{{ $email_arr[$key] }}</h5>
                                    <h5 class="text-primary mt-4 mb-2">{{ $ph_arr[$key] }}</h5> 
                            </div>
                        @endforeach
                    </div>
                   
                </div>
            @endforeach
        </div>
        <ul class="pager">
            {!! $thesisgroups->appends(request()->query()) !!}
        </ul>
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')

@stop
