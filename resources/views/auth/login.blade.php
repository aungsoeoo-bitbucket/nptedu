@extends('layouts/default')

{{-- Page title --}}
@section('title')
Login | Welcome to naypyitawedu.com
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/login.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
    </div>
@stop


{{-- Page content --}}
@section('content')
    <div class="container" style="margin:20px;">
        <!--Content Section Start -->
        <div class="row">
            <div class="box animation flipInX">
                <div class="box1">
                    <img src="{{ asset('assets/images/logo.png') }}" alt="logo" class="img-responsive mar">
                    <hr>
                    <h3 class="text-primary">Log In</h3>
                        <!-- Notifications -->
                    <div id="notific">
                        @include('admin.layouts.notification')
                        </div>
                        <form action="{{ url('login')}}" class="omb_loginForm"  autocomplete="off" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group {{ $errors->first('email', 'has-error') }}">
                                <label class="sr-only">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Email"
                                       value="{!! old('email') !!}">
                            </div>
                            <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                            <div class="form-group {{ $errors->first('password', 'has-error') }}">
                                <label class="sr-only">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                            <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember Password
                                </label>

                            </div>
                            <input type="submit" class="btn btn-block btn-primary" value="Log In">
                            Don't have an account? <a href="{{ route('register') }}"><strong> Sign Up</strong></a>
                        </form>
                    </div>
                    <div class="bg-light animation flipInX">
                        <a href="{{ route('forgot-password') }}" id="forgot_pwd_title">Forgot Password?</a>
                    </div>

                </div>
            </div>
        </div>
        <!-- //Content Section End -->
    </div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->

    <!--page level js ends-->
@stop
