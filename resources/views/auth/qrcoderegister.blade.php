@extends('layouts/default')

{{-- Page title --}}
@section('title')
Register | Welcome to naypyitawedu.com
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->

    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css')}}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css')}}" rel="stylesheet">
    <link type="text/css" media="screen"  rel="stylesheet"  href="{{ asset('assets/css/croppie.css')}}">

     <script type="text/javascript" src="{{  asset('assets/js/frontend/jquery.min.js') }}"></script>
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
    </div>
@stop


{{-- Page content --}}
@section('content')
    <div class="container" style="margin:20px;">
        <!--Content Section Start -->
        <div class="row">
            <div class="wrapper ">
                <div id="notific">
                @include('admin.layouts.notification')
                </div>
                <section class="content">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        MBA First Year Registration Form
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <!-- Notifications -->
                                    <div id="notific">
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                    </div>
                                    <!--main content-->
                                    <form id="commentForm" action="{{ url('register/student')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                       
                                        <!-- CSRF Token -->
                                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        

                                        <div id="rootwizard">
                                            <ul>
                                                <li><a href="#tab1" data-toggle="tab">Security</a></li>
                                                <li><a href="#tab2" data-toggle="tab">Profile</a></li>
                                                <li><a href="#tab3" data-toggle="tab">Contact</a></li>
                                            </ul>
                                            <div class="tab-content">

                                                <div class="tab-pane" id="tab1">
                                                    <h2 class="hidden">&nbsp;</h2>
                                                    <div class="form-group ">
                                                        <label for="username" class="col-sm-2 control-label">Name (MM) *</label>
                                                        <div class="col-sm-10">
                                                            <input id="username" name="username" type="text"
                                                                   placeholder="Eg: ေမာင္မင္းမင္း" class="form-control required"
                                                                   value="{{ old('username')}}" style="font-family: Zawgyi, 'Open Sans', Arial, sans-serif !important;" />

                                                            
                                                        </div>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label for="username_en" class="col-sm-2 control-label">  Name (Eng) *</label>
                                                        <div class="col-sm-10">
                                                            <input id="username_en" name="username_en" type="text"
                                                                   placeholder="Eg: Mg Min Min" class="form-control required"
                                                                   value="{{ old('username_en') }}"/>

                                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-group ">
                                                        <label for="email" class="col-sm-2 control-label">Email *</label>
                                                        <div class="col-sm-10">
                                                            <input id="email" name="email" placeholder="E-mail" type="text"
                                                                   class="form-control required email" value="{{ old('email')}}"/>
                                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-group ">
                                                        <label for="password" class="col-sm-2 control-label">Password *</label>
                                                        <div class="col-sm-10">
                                                            <input id="password" name="password" type="password" placeholder="Password"
                                                                   class="form-control required" value=""/>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-group ">
                                                        <label for="password_confirm" class="col-sm-2 control-label">Confirm Password *</label>
                                                        <div class="col-sm-10">
                                                            <input id="password_confirm" name="password_confirm" type="password"
                                                                   placeholder="Confirm Password " class="form-control required"/>
                                                            
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="tab2" disabled="disabled">
                                                    <h2 class="hidden">&nbsp;</h2> <div class="form-group  ">
                                                        <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
                                                        <div class="col-sm-10">
                                                            <input id="dob" name="dob" type="text" class="form-control"
                                                                   data-date-format="YYYY-MM-DD"
                                                                   placeholder="yyyy-mm-dd" value="{{ old('dob')}}" />
                                                        </div>
                                                        <span class="help-block"></span>
                                                    </div>

                                                    <div class="form-group ">
                                                        <label for="email" class="col-sm-2 control-label">Gender *</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control" title="Select Gender..." name="gender">
                                                                <option value="">Select Gender</option>
                                                                <option value="male" {{ (old('gender')=='male')?'selected':''}}
                                                                         >Male
                                                                </option>
                                                                <option value="female" {{( old('gender')=='female')?'selected':''}} >
                                                                    Female
                                                                </option>

                                                            </select>
                                                        </div>
                                                        <span class="help-block"></span>
                                                    </div>

                                                   
                                                    <div class="form-group ">
                                                        <label for="designation" class="col-sm-2 control-label">Designation </label>
                                                        <div class="col-sm-10">
                                                            <input id="designation" name="designation" type="text"
                                                                   placeholder="ရာထူး" style="font-family: Zawgyi, 'Open Sans', Arial, sans-serif !important;" class="form-control" value="{{ old('designation') }}" />
                                                            
                                                        </div>
                                                    </div>

                                                      <div class="form-group ">
                                                        <label for="department" class="col-sm-2 control-label">Department </label>
                                                        <div class="col-sm-10">
                                                            <input id="department" name="department" type="text"
                                                                   placeholder="ဌာန" style="font-family: Zawgyi, 'Open Sans', Arial, sans-serif !important;" class="form-control" value="{{ old('department') }}"/>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-group ">
                                                        <label for="ministry_company" class="col-sm-2 control-label">Ministry/Company </label>
                                                        <div class="col-sm-10">
                                                            <input id="ministry_company" name="ministry_company" type="text"
                                                                   placeholder="ဝန္ၾကီးဌာန/ကုမၸဏီ" style="font-family: Zawgyi, 'Open Sans', Arial, sans-serif !important;" class="form-control" value="{{ old('ministry_company') }}"/>
                                                            
                                                        </div>
                                                    </div>




                                                    <div class="form-group ">
                                                        <label for="pic" class="col-sm-2 control-label">Profile picture</label>
                                                        <div class="col-sm-10">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <img src="" id="image_preview" alt="image preview" style="max-width: 200px; max-height: 200px;">
                                                                {{-- <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div> --}}
                                                                <div>
                                                                    <span class="btn btn-default btn-file">
                                                                        <span class="fileinput-new">Select image</span>
                                                                        <span class="fileinput-exists">Change</span>
                                                                        <input id="pic"  type="file" class="form-control upload"/>
                                                                    </span>
                                                                    <a href="#" class="btn btn-danger fileinput-exists"
                                                                       id="remove_preview">Remove</a>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="pic_file" value="" id="hidden_photo">

                                                            <span class="help-block"></span>
                                                            <span class="text-danger">{{ $errors->first('pic_file') }}</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="tab3" disabled="disabled">
                                                    

                                                     <div class="form-group">
                                                        <label for="address" class="col-sm-2 control-label">Address *</label>
                                                        <div class="col-sm-10">
                                                            <textarea id="address" style="height: 60px !important; font-family: Zawgyi, 'Open Sans', Arial, sans-serif !important;" class="form-control" placeholder="ေနရပ္လိပ္စာ"  name="address" id="" cols="30" rows="10">{{ old('address') }}</textarea>
                                                        </div>
                                                        <span class="help-block"></span>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="phone" class="col-sm-2 control-label">Phone *</label>
                                                        <div class="col-sm-10">
                                                            <input id="phone" style="font-family: Zawgyi, 'Open Sans', Arial, sans-serif !important;" name="phone" type="text" class="form-control" placeholder="ဖုန္းနံပါတ္" 
                                                                   value="{{ old('phone') }}"/>
                                                        </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                
                                                <ul class="pager wizard">
                                                    <li class="previous"><a href="#">Previous</a></li>
                                                    <li class="next"><a href="#">Next</a></li>
                                                    <li class="next finish" style="display:none;"><a href="javascript:;">Submit</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <!--row end-->
                </section>
               
                <!-- right-side -->
            </div>
        </div>
        <!-- //Content Section End -->
    </div>

     @include('modal.image_crop')
   
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script src="{{ asset('config/baseurl.js')}}" type="text/javascript"></script>
    <!-- end of global js -->
<!-- begin page level js -->
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js')}}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js')}}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js')}}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/croppie.js')}}"></script>
        <!-- end page level js -->
     <!-- crop image modal -->
    
@stop

